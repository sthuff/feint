## Synopsis

FireEye ITSO Notification Tool (FEINT)

The FEINT queries information from the FireEye CMS and prepares it for submission to Virginia Tech's Service-Now Incident system, then updates FireEye to acknowledge the alert with the Service-Now Incident number and the user's PID who submitted the incident.

## Motivation

This application was written to help improve efficiency between receiving an FireEye alert and being able to provide relevant information to the Virginia Tech Call Center in order to more quickly respond to security incidents.

## Installation

See Dockerfile(s)
