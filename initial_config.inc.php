<?php 
/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	initial_config.inc.php
	
	Included by index.php, update_malwaredb.php, url_tool.php
	
	Sets initial configuration variables and PHP settings.
	
	Sets the access level for a user based on their GroupMembershipUUGID
	obtained from the mod_auth_cas apache module.

****************************************************************************/

	// SERVER TIMEZONE REQUIRED FOR DATE FUNCTIONS
	date_default_timezone_set('UTC');

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// If we're not on a localhost/dev instance...
	if ($_SERVER["SERVER_NAME"] != "localhost") {

		//print "DEBUG: GroupMembershipUUGID is: ".$_SERVER["HTTP_CAS_GROUPMEMBERSHIPUUGID"];
		
		/*
		Notes: 
		mod_auth_cas in Apache should reject anyone that is not a member of the following GroupMembershipUUGIDs: 
		
		require cas-attribute groupMembershipUugid:iso.org.analysts
        require cas-attribute groupMembershipUugid:iso.org.mgt
        require cas-attribute groupMembershipUugid:itee.ic
        require cas-attribute groupMembershipUugid:cns.fn.vtoc.call-center
        require cas-attribute groupMembershipUugid:es.dms.confluence.itee.informationcenter
		*/

if ($_SERVER["HTTP_CAS_UUPID"] == "testuser" ) {
			$casUser = $_SERVER["HTTP_CAS_UUPID"];
			$userPermissions = "dbreadonly";
}
else {
		
		if (strpos($_SERVER["HTTP_CAS_GROUPMEMBERSHIPUUGID"], "iso.org.analysts") || strpos($_SERVER["HTTP_CAS_GROUPMEMBERSHIPUUGID"], "iso.org.mgt")) {
			$casUser = $_SERVER["HTTP_CAS_UUPID"];
			$userPermissions = "dbreadwrite";
		} 
		else if (strpos($_SERVER["HTTP_CAS_GROUPMEMBERSHIPUUGID"], "itee.ic") || strpos($_SERVER["HTTP_CAS_GROUPMEMBERSHIPUUGID"], "cns.fn.vtoc.call-center") || strpos($_SERVER["HTTP_CAS_GROUPMEMBERSHIPUUGID"], "es.dms.confluence.itee.informationcenter")) {
			$casUser = $_SERVER["HTTP_CAS_UUPID"];
			$userPermissions = "dbreadonly";
		}
		else {
			// This technically shouldn't ever happen since the default action for unauthenticated connections
			// is to redirect to the CAS/Login page... but just in case...
			print "<br/>You are not authorized to access this page.  Do you need to log in again?";
			exit;
		}
}

	}
	else {
		// force the localhost dev instance to dbreadonly since we don't have CAS functionality
		$userPermissions = "dbreadonly";
	}
?>