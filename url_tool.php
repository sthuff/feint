<?php

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	url_tool.php
	
	This is a standalone version of the LAA URL Tool.  Instead of defaults
	being loaded from a FireEye Alert, the current time is used.

****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>ITSO LAA URL Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("url_tool_javascripts.inc.php");
	
	// GMT Times
	
	$gmtDateTimeAddTime = new DateTime();
	$timestampGMTAddTime = 			$gmtDateTimeAddTime->format('Y-m-d H:i:s O');
	$timestampGMTAddTimeFormat = 	$gmtDateTimeAddTime->format('Y-m-d\TH:i:s.uP');

	$gmtDateTimeSubTime = new DateTime();
	$timestampGMTSubTime = 			$gmtDateTimeSubTime->format('Y-m-d H:i:s O');
	$timestampGMTSubTimeFormat = 	$gmtDateTimeSubTime->format('Y-m-d\TH:i:s.uP');

	// EST Times 
	
	$estDateTimeAddTime = new DateTime();
	$estDateTimeAddTime->setTimeZone(new DateTimeZone('America/New_York'));
	$timestampESTAddTime = 			$estDateTimeAddTime->format('Y-m-d H:i:s O');
	$timestampESTAddTimeFormat = 	$estDateTimeAddTime->format('Y-m-d\TH:i:s.uP');

	$estDateTimeSubTime = new DateTime();
	$estDateTimeSubTime->setTimeZone(new DateTimeZone('America/New_York'));
	$timestampESTSubTime = 			$estDateTimeSubTime->format('Y-m-d H:i:s O');
	$timestampESTSubTimeFormat = 	$estDateTimeSubTime->format('Y-m-d\TH:i:s.uP');	
?>
</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
		<div class="container">

			<p>Use this form to generate a LAA URL at the specified date/time.<br/><br/></p>

			<p>The default LAA Dashboard is <strong>ITSO-IP-Lookupv2</strong> but can be changed using the drop-down list below. <br/></p>

			<form id="laaForm">
				<table class="incidentFormTable">
				<tr>
					<td width="900px" colspan="2">
					LAA Dashboard: <br/>
					<select id="urlDash">
						<option value="laaURL_ITSO_IPLOOKUPv2" selected="selected">ITSO-IP-Lookup v2</option>
						<option value="laaURL_ITSO_WIRED_IPLOOKUP">ITSO-Wired-IP-Lookup</option>
						<option value="laaURL_ITSO_WIRELESS_IPLOOKUP">ITSO-Wireless-IP-Lookup</option>	
					</select> 
					</td>
				</tr>
				<tr>
					<td width="900px" colspan="2">
						<p>Query: <br/><input type="text" id="custQueryText" name="custQueryText" value="" size="45" /></p>
					</td>
				</tr>
				<tr>
					<td>
						<p>Search From: <br/>
						<input type="text" name="dateTimeFromLAA" id="dateTimeFromLAA" value="<?php print $timestampESTSubTime; ?>" size="30" />
						<input type="hidden" name="dateTimeFromLAAFormat" id="dateTimeFromLAAFormat" value="<?php print $timestampGMTSubTimeFormat; ?>" />
						</p>
					</td>
					<td>
						<p>Search To: <br/>
						<input type="text" name="dateTimeToLAA" id="dateTimeToLAA" value="<?php print $timestampESTAddTime; ?>" size="30" />
						<input type="hidden" name="dateTimeToLAAFormat" id="dateTimeToLAAFormat" value="<?php print $timestampGMTAddTimeFormat; ?>" />
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="2">LAA URL for <strong><span id="laaURLDashboardName">ITSO-IP-Lookup v2</span></strong>: <br/>
						<textarea id="laaURL" name="laaURL" rows="10" cols="95"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" id="copyLAAText" value="Copy URL to Clipboard" />
					</td>
				</tr>
				</table>
			</form>			

			
		</div> 			<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>