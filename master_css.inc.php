<?php 

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	master_css.inc.php
	
	Included by index.php, update_malwaredb.php, url_tool.php
	
	Includes the master stylesheet and sets the favicon for the site.	

****************************************************************************/
?>
	<!-- Master CSS -->
		<link rel="stylesheet" type="text/css" media="all" href="./css/master.css" />
		<link rel="shortcut icon" href="./favicon.ico" />
<?php 

// EOF

?>