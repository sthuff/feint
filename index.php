<?php
/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	index.php

	Main functionality of the FEINT tool is handled by index.php.  Two 
	different modes of index.php are triggered based on whether a 
	$_GET["alertID"] varaible exists.   
	
	If $_GET["alertID"] does NOT exist, a simple form asking for the
	FireEye alert ID is presented.  
	
	If $_GET["alertID"] DOES exist, an API query is made to the FireEye CMS
	and the data returned is formatted into a web form for review.  The name
	of the malware item associated with the alert is used to query the ITSO
	Malware Database.  If a match is found, the Malware Descrition and 
	Malware Severity fields are filled with data.  
	
	If an ITSO staff member is logged in, they have the option to author 
	or update the Malware Description and Malware Severity values.  If a
	non-ITSO staff member is logged in (Call Center), the Severity and 
	Description fields are read-only.  
	
	Data pulled from FireEye can be changed as needed.  For example, if 
	the communication port information is backward (as happens periodically), 
	it can be fixed before submission to Service-Now.  
	
	The Detected Issue field (Malware name) is a searchable field that
	offers predicted answers as the user types.  This can be useful if 
	FireEye returns a very generic type of Malware.  In many cases of 
	generic Malware names, the ITSO Malware Database contains more-detailed 
	items that can be selected from a predicted list by adding a space to 
	the Detected Issue field and choosing the appropriate result.
	
	Additional investigation tools are also available as dialogs that 
	will appear over the main web form but won't disrupt the data it 
	contains.  
	
	The "Generate LAA URL" tool will take information from 
	the current FireEye alert and craft a URL that can be pasted into 
	a web browser and initiate a query to the Log Archiving Analysis (LAA) 
	tool at Virginia Tech using parameters and date ranges relevant to 
	the FireEye alert currently queried by the FEINT.  
	
	More information on the Generate LAA Tool can be found in the 
	laa_dialog.inc.php file.
	
	The "Generate Argus Command" tool will take information from the 
	current FireEye alert and craft an Argus command that can be pasted
	onto the command line of the Argus server.
	

****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>FEINT - FireEye ITSO Notification Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("javascripts.inc.php");
?>

</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
		<div class="container">

<?php
	if (!isset($_GET["feAlertID"])) {
		
		if (isset($_GET["feAlertsDuration"])) {
			if ($_GET["feAlertsDuration"] == "1_hour") { $fe1_hourSelected = "selected=\"selected\""; $feAlertsDuration = "1_hour"; } else { $fe1_hourSelected = ""; }
			if ($_GET["feAlertsDuration"] == "2_hours") { $fe2_hoursSelected = "selected=\"selected\""; $feAlertsDuration = "2_hours";  } else { $fe2_hoursSelected = ""; }
			if ($_GET["feAlertsDuration"] == "6_hours") { $fe6_hoursSelected = "selected=\"selected\""; $feAlertsDuration = "6_hours";  } else { $fe6_hoursSelected = ""; }
			if ($_GET["feAlertsDuration"] == "12_hours") { $fe12_hoursSelected = "selected=\"selected\""; $feAlertsDuration = "12_hours";  } else { $fe12_hoursSelected = ""; }
			if ($_GET["feAlertsDuration"] == "24_hours") { $fe24_hoursSelected = "selected=\"selected\""; $feAlertsDuration = "24_hours";  } else { $fe24_hoursSelected = ""; }
			if ($_GET["feAlertsDuration"] == "48_hours") { $fe48_hoursSelected = "selected=\"selected\""; $feAlertsDuration = "48_hours";  } else { $fe48_hoursSelected = ""; }
		}
		else {
			$fe1_hourSelected = "";
			$fe2_hoursSelected = "";
			$fe6_hoursSelected = "";
			$fe12_hoursSelected = "selected='selected'";
			$fe24_hoursSelected = "";
			$fe48_hoursSelected = "";
			$feAlertsDuration = "12_hours"; 
		}
?>
	<!-- form start -->  

		<p>
			<br/>
<?php
	// force DEV mode if we're on a dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/feintdev")) { 
		print "<strong>NOTICE: This is the development version of FEINT.  Incidents will default to the DEV instance of <br/>Service-Now and FireEye Acknowledgements will be skipped.</strong><br/><br/>";
		$mode = "DEV"; 
	}
?>
			This tool imports data from FireEye and will create an incident in Service-Now to the NOC. <br/>
			Manually enter a FireEye Alert ID in the field below:
			<br/>
			<br/>
		</p>

		<div id="formFrame">
			<form action="index.php" method="get">
			<table width="900px" border="0" class="incidentForm">
			<tr>
				<td width="150px">FireEye Alert ID:</td>
				<td width="300px">
					<input type="text" id="feAlertID" name="feAlertID" value="" size="30" />	
				</td>
				<td width="450px">
					<input type="submit" id="feAlertIDSubmit" value="Look Up Alert" style="width:125px;"/>
				</td>
			</tr>
			</table>	
			</form>
			<br/>
			<br/>
			<p>Alternatively, you can click an alert linked in the following list: </p>
			<br/>
			<form>
				<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
				<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />
				<table width="900px" border="0" class="incidentForm">
				<tr>
					<td width="450px">Listing FireEye Alerts in the last:</td>
					<td colspan="3">
						<select id="feAlertsDuration" name="feAlertsDuration" style="width:125px;">
							<option value="1_hour" <?php print $fe1_hourSelected; ?>>1 Hour</option>
							<option value="2_hours" <?php print $fe2_hoursSelected; ?>>2 Hours</option>
							<option value="6_hours" <?php print $fe6_hoursSelected; ?>>6 Hours</option>
							<option value="12_hours" <?php print $fe12_hoursSelected; ?>>12 Hours</option>
							<option value="24_hours" <?php print $fe24_hoursSelected; ?>>24 Hours</option>
							<option value="48_hours" <?php print $fe48_hoursSelected; ?>>48 Hours</option>
						</select>
					</td>
				</tr>
				</table>
			</form>
			<br/>
			<strong>Results shown in <span class="green">green</span> have Source IPs with alerts that have been acknowledged in the last 24 hours.</strong>
			<br/>
			<br/>
			<div id="alertsListLoading"><img src="./images/loading16x16.gif" /> Loading alerts, please wait...</div>
			<div id="alertsListContainer"></div>
		</div>
<?php

	} else {   // FE ALERT ID IS SET

		include("./assets/cms_info.inc.php");
		include_once("./assets/cms_funcs.inc.php");
		
		// DEBUG: checking info access
		#print "cmsUser is: ".$cmsUser."<br/>";
		$apiToken = cmsAuthorize($cmsUser,$cmsPass);
		unset($cmsUser);
		unset($cmsPass);

		// DEBUG: a list of alerts within a duration
		//$apiAlertsList = cmsGetAlertsWithinTimeframe($apiToken, "concise", "1_hour");
		//print "DEBUG Alerts List in 1 hour: <br/>";
		//var_dump($apiAlertsList);
		//print "<br/>";
		
		// FireEye severity values are crit, majr, minr
		//verbosity options are: "concise", "normal", "extended"

		// Trim and sanitize $alertID to an int
		$alertID = trim($_GET["feAlertID"]);
		$alertID = filter_var($alertID, FILTER_SANITIZE_NUMBER_INT);
		
		if (is_nan($alertID)) { 
			print "Alert ID was invalid or not a number."; 
			exit;
		}
		
		$apiAlertsJSON = cmsGetAlertWithID($apiToken, "extended", $alertID);
		
		#print "<br/>DEBUG: JSON Response is: <br/>";
		#var_dump($apiAlertsJSON);

		// Decode the JSON object into a stdClass
		$apiAlerts = json_decode($apiAlertsJSON);
		
		//print "<br/>DEBUG: CLASS Response is: <br/>";
		//var_dump($apiAlerts);
		//print "<br/>";
			
		if (isset($apiAlerts->httpStatus)) {
			print "<br/><strong>".$apiAlerts->httpStatus."</strong> - Unable to connect to the Cadbury CMS.  Most likely, the API token has expired.  Please refresh this page to renew the API token. <br/><br/>";
			print "<strong>Error:</strong> ".$apiAlerts->errorCode.", HTTP Status: ".$apiAlerts->httpStatus." - ".$apiAlerts->description."<br/><br/>";
			print "Halting execution since the CMS could not be queried.<br/><br/>";
			print "<a href=\"javascript:window.location.reload();\">Refresh page</a><br/><br/>";
			exit;
		}

		// Trim and Sanitize values from FireEye API (just in case)
		if ($apiAlerts->alert->{"@attributes"}->severity == "crit") { $feSeverityCritSelected = "selected=\"selected\""; } else { $feSeverityCritSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->severity == "majr") { $feSeverityMajrSelected = "selected=\"selected\""; } else { $feSeverityMajrSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->severity == "minr") { $feSeverityMinrSelected = "selected=\"selected\""; } else { $feSeverityMinrSelected = ""; }		

		//////////////////////////////////////////////////////////////
		//
		// BEGIN TIMESTAMP MANIPULATION AND FORMATTING
		//
		//////////////////////////////////////////////////////////////

		$feTimestampServer = trim($apiAlerts->alert->occurred);
		$feTimestampServer = filter_var($feTimestampServer, FILTER_SANITIZE_STRING);
		
		$feTimestampObj = new DateTime($feTimestampServer);
		
		$estTZ = new DateTimeZone('America/New_York');	
		$offset = $estTZ->getOffset($feTimestampObj);

		$feTimestampObj->add(DateInterval::createFromDateString((string)$offset.'seconds'));
		$feTimestampObj->setTimeZone(new DateTimeZone('America/New_York'));		
		$feTimestamp = $feTimestampObj->format('Y-m-d H:i:s T');
		
		$feTimestampObj->setTimeZone(new DateTimeZone('UTC'));
		$feTimestampUTC = $feTimestampObj->format('Y-m-d H:i:s T');

		//print "<br/>DEBUG feTimestamp: ".$feTimestamp."<br/>";
		//print "<br/>DEBUG UTC: ".$feTimestampUTC."<br/>";

		// Get date +/- "some time" interval for later use with the Argus and LAA URL Generators...
		// LAA URL uses time format: 2017-02-26T03:40:39.890Z
		
		// GMT Times
		
		$gmtDateTimeAddTime = new DateTime($feTimestampUTC);		
		$gmtDateTimeAddTime->add(DateInterval::createFromDateString((string)'5 minutes'));
		$gmtDateTimeAddTime->setTimeZone(new DateTimeZone('GMT'));
		$feTimestampGMTAddTime = $gmtDateTimeAddTime->format('Y-m-d H:i:s O');
		$feTimestampGMTAddTimeFormat = $gmtDateTimeAddTime->format('Y-m-d\TH:i:s.uP');
					
		$gmtDateTimeSubTime = new DateTime($feTimestampUTC);
		$gmtDateTimeSubTime->sub(DateInterval::createFromDateString((string)'2 hours'));
		$gmtDateTimeSubTime->setTimeZone(new DateTimeZone('GMT'));
		$feTimestampGMTSubTime = $gmtDateTimeSubTime->format('Y-m-d H:i:s O');
		$feTimestampGMTSubTimeFormat = $gmtDateTimeSubTime->format('Y-m-d\TH:i:s.uP');

		// EST Times
		
		$estDateTimeAddTime = new DateTime($feTimestampUTC);		
		$estDateTimeAddTime->add(DateInterval::createFromDateString((string)'5 minutes'));
		$estDateTimeAddTime->setTimeZone(new DateTimeZone('America/New_York'));
		$feTimestampESTAddTime = $estDateTimeAddTime->format('Y-m-d H:i:s O');
		$feTimestampESTAddTimeFormat = $estDateTimeAddTime->format('Y-m-d\TH:i:s.uP');

		$estDateTimeSubTime = new DateTime($feTimestampUTC);
		$estDateTimeSubTime->sub(DateInterval::createFromDateString((string)'2 hours'));
		$estDateTimeSubTime->setTimeZone(new DateTimeZone('America/New_York'));
		$feTimestampESTSubTime = $estDateTimeSubTime->format('Y-m-d H:i:s O');
		$feTimestampESTSubTimeFormat = $estDateTimeSubTime->format('Y-m-d\TH:i:s.uP');

		
		//print "<br/>DEBUG: feTimestamp: ".$feTimestamp."<br/>";
		//print "<br/>DEBUG: feTimestampESTAddTime: ".$feTimestampESTAddTime." | Format: ".$feTimestampESTAddTimeFormat." | DST = ".date('I')."<br/>";	
		//print "<br/>DEBUG: feTimestampESTSubTime: ".$feTimestampESTSubTime." | Format: ".$feTimestampESTSubTimeFormat." | DST = ".date('I')."<br/>";	
		//print "<br/>";
		//print "<br/>DEBUG: feTimestampGMTAddTime: ".$feTimestampGMTAddTime." | Format: ".$feTimestampGMTAddTimeFormat." | DST = ".date('I')."<br/>";
		//print "<br/>DEBUG: feTimestampGMTSubTime: ".$feTimestampGMTSubTime." | Format: ".$feTimestampGMTSubTimeFormat." | DST = ".date('I')."<br/>";

		//print "<br/>DEBUG Times: <br/>";
		//print "GMT + Time: ".$feTimestempGMTAddTime."<br/>";
		//print "GMT - Time: ".$feTimestempGMTSubTime."<br/>";

		//////////////////////////////////////////////////////////////
		//
		// END TIMESTAMP MANIPULATION AND FORMATTING
		//
		//////////////////////////////////////////////////////////////
		
		$feApplianceID = trim($apiAlerts->alert->{"@attributes"}->{"appliance-id"});
		$feApplianceID = filter_var($feApplianceID, FILTER_SANITIZE_STRING);

		$feID = trim($apiAlerts->alert->{"@attributes"}->id);
		$feID = filter_var($feID, FILTER_SANITIZE_STRING);

		$feRootInfection = trim($apiAlerts->alert->{"@attributes"}->{"root-infection"});
		$feRootInfection = filter_var($feRootInfection, FILTER_SANITIZE_STRING);

		$feName = trim($apiAlerts->alert->{"@attributes"}->name);
		$feName = filter_var($feName, FILTER_SANITIZE_STRING);

		$feVictimHost = gethostbyaddr($apiAlerts->alert->src->ip);
		
		$feSourceIP = trim($apiAlerts->alert->src->ip);
		$feSourceIP = filter_var($feSourceIP, FILTER_SANITIZE_STRING);
		
		$feSourcePort = trim($apiAlerts->alert->src->port);
		$feSourcePort = filter_var($feSourcePort, FILTER_SANITIZE_STRING);

		$feSourceMAC = trim($apiAlerts->alert->src->mac);
		$feSourceMAC = filter_var($feSourceMAC, FILTER_SANITIZE_STRING);

		//print "<br/>DEBUG: Alert Explanation Malware Detected is: <br/>";
		//var_dump($apiAlerts->{"alert"}->{"explanation"}->{"malware-detected"});
		//print "<br/>";
		
		if (isset($apiAlerts->{"alert"}->{"explanation"}->{"malware-detected"}->{"malware"}->{"@attributes"}->{"name"})) {
			$feMalwareName = trim($apiAlerts->{"alert"}->{"explanation"}->{"malware-detected"}->{"malware"}->{"@attributes"}->{"name"});			
		} else {
			$feMalwareName = trim($apiAlerts->{"alert"}->{"explanation"}->{"malware-detected"}->{"malware"}[0]->{"@attributes"}->{"name"});
		}		
		$feMalwareName = filter_var($feMalwareName, FILTER_SANITIZE_STRING);
		
		//$feDestinationIP = trim($apiAlerts->alert->dst->ip);
		//$feDestinationIP = filter_var($feDestinationIP, FILTER_SANITIZE_STRING);
		
		//$feDestinationPort = trim($apiAlerts->alert->dst->port);
		//$feDestinationPort = filter_var($feDestinationPort, FILTER_SANITIZE_STRING);

		if (isset($apiAlerts->alert->dst->ip)) {
			$feDestinationIP = trim($apiAlerts->alert->dst->ip);
			$feDestinationIP = filter_var($feDestinationIP, FILTER_SANITIZE_STRING);
		}
		else { $feDestinationIP = "NONE"; }

		if (isset($apiAlerts->alert->dst->port)) {
			$feDestinationPort = trim($apiAlerts->alert->dst->port);
			$feDestinationPort = filter_var($feDestinationPort, FILTER_SANITIZE_STRING);
		}
		else { $feDestinationPort = "NONE"; }

		$feDestinationMAC = trim($apiAlerts->alert->dst->mac);
		$feDestinationMAC = filter_var($feDestinationMAC, FILTER_SANITIZE_STRING);

		if ($apiAlerts->alert->{"@attributes"}->sensor == "") { $feSensorGENERALSelected = "selected=\"selected\""; } else { $feSensorGENERALSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "ALXFE") { $feSensorALXFESelected = "selected=\"selected\""; } else { $feSensorALXFESelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "BABYRUTH") { $feSensorBABYRUTHSelected = "selected=\"selected\""; } else { $feSensorBABYRUTHSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "BLFE") { $feSensorBLFESelected = "selected=\"selected\""; } else { $feSensorBLFESelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "BOUNTY") { $feSensorBOUNTYGSelected = "selected=\"selected\""; } else { $feSensorBOUNTYSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "FASTBREAK") { $feSensorFASTBREAKSelected = "selected=\"selected\""; } else { $feSensorFASTBREAKSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "FCFE") { $feSensorFCFESelected = "selected=\"selected\""; } else { $feSensorFCFESelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->sensor == "MRBIG") { $feSensorMRBIGSelected = "selected=\"selected\""; } else { $feSensorMRBIGSelected = ""; }
		
		$feSensorSrcTxt = trim($apiAlerts->alert->{"@attributes"}->sensor);
		$feSensorSrcTxt = filter_var($feSensorSrcTxt, FILTER_SANITIZE_STRING);
		
		$feAdditionalInfo = "Additional Information from FireEye for Alert: ".$feID."\n\n";
		$feAdditionalInfo .= "Name: ".$feMalwareName."\n";
		$feAdditionalInfo .= "Type: ".$feName."\n";
		$feAdditionalInfo .= "Sensor: FireEye-".$apiAlerts->alert->{"@attributes"}->sensor."\n";
		$feAdditionalInfo .= "\n";
		$feAdditionalInfo .= "Details: \n";
		$feAdditionalInfo .= "\n";
		$feAdditionalInfo .= "Ocurred: ".$feTimestamp."\n";
		if (isset($apiAlerts->alert->explanation->{"malware-detected"}->malware->md5sum) && is_string($apiAlerts->alert->explanation->{"malware-detected"}->malware->md5sum)) {
			$feAdditionalInfo .= "md5sum/URL: ".$apiAlerts->alert->explanation->{"malware-detected"}->malware->md5sum."\n";
		}
		$feAdditionalInfo .= "\n";
		$feAdditionalInfo .= "Source IP: ".$feSourceIP."\n";
		$feAdditionalInfo .= "Source Port: ".$feSourcePort."\n";
		//$feAdditionalInfo .= "Source MAC: ".$feSourceMAC."\n";
		$feAdditionalInfo .= "\n";	
		$feAdditionalInfo .= "Destination IP: ".$feDestinationIP."\n";
		$feAdditionalInfo .= "Destination Port: ".$feDestinationPort."\n";
		//$feAdditionalInfo .= "Destination MAC: ".$feDestinationMAC."\n";
		$feAdditionalInfo .= "\n";	
		
		$feAdditionalInfo = filter_var($feAdditionalInfo, FILTER_SANITIZE_STRING);
		
		
		/* MALWARE INFO DB LOOKUP */ 

		include_once("./assets/classes.inc.php");
		include("./assets/db_info.inc.php");
		$dbName = "feint";
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);

		try {
			$statement = $connection->prepare('SELECT id, name, severity, description FROM malware WHERE name = :name LIMIT 1');
			$statement->execute(array('name' => $feMalwareName));

			if ($statement->rowCount() > 0) {
				foreach(($statement->fetchAll(PDO::FETCH_CLASS,'Malware')) as $mw) {
					$mwID = $mw->getId();
					$mwSev = $mw->getSev(); 
					$mwName = $mw->getName(); 
					$mwDesc = $mw->getDesc(); 
				}
			} else {
				$mwID = "NEW";
				$mwSev = "high";
				$mwName = $feMalwareName;
				$mwDesc = "";
			}
			
			if ($userPermissions == "dbreadonly") {	
				$mwDescTemp = "There is no description entry for ".$feMalwareName." in the ITSO Malware Database.  \n\nThis Incident will first be routed to the ITSO so that a description can be added.";
		 	} else {	
				$mwDescTemp = "There is no description entry for ".$feMalwareName." in the ITSO Malware Database.  \n\nPlease update this text and click Update Malware Description before submitting.";
			}
			
			$incidentCommentsTemp = "This is an optional field where analysts can include PUBLIC incident-specific information that will appear just above the malware description and is intended to be seen by the end-user.";
			$incidentNotesTemp = "This is an optional field where analysts can include PRIVATE incident-specific work notes that will appear at the top of the Additional Information section that is only visible to analysts.";
		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
			
		if ($mwSev == "critical") { $mwSevCriticalSelected = "selected=\"selected\""; } else { $mwSevCriticalSelected = ""; }
		if ($mwSev == "high") { $mwSevHighSelected = "selected=\"selected\""; } else { $mwSevHighSelected = ""; }
		if ($mwSev == "medium") { $mwSevMediumSelected = "selected=\"selected\""; } else { $mwSevMediumSelected = ""; }
		if ($mwSev == "low") { $mwSevLowSelected = "selected=\"selected\""; } else { $mwSevLowSelected = ""; }

		// Check for Acknowledgements

		include("./assets/db_info.inc.php");
		$dbName = "feint";
		
		$showWarning = 0;

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP FROM logs WHERE logSrcIP = :logSrcIP AND logType = "ackAlertSuccess" ORDER BY logDate ASC');
			$statement->execute(array('logSrcIP' => $feSourceIP));

			if ($statement->rowCount() > 0) {
				
				$lcount = 0;
				
				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($rows as $log) { 
					$ackList[$lcount]["logID"] = $log["logID"];
					$ackList[$lcount]["logUser"] = $log["logUser"];
					$ackList[$lcount]["logDate"] = $log["logDate"];
					$ackList[$lcount]["logType"] = $log["logType"];
					$ackList[$lcount]["logDataID"] = $log["logDataID"];
					$ackList[$lcount]["logInfo"] = $log["logInfo"];
					$ackList[$lcount]["logSrcIP"] = $log["logSrcIP"];
					
					// DEBUG
					//print "Ack list: |".$log["logSrcIP"]."|, FESourceIP: |".$feSourceIP."|<br/>";
					
					$dateOfAck = new DateTime($ackList[$lcount]["logDate"]);
					$yesterday = new DateTime('NOW', new DateTimeZone('America/New_York'));
					$yesterday->sub(new DateInterval('PT24H'));
					
					// DEBUG
					//print "Date of Ack: ".$dateOfAck->format('Y-m-d H:i:s')." ( ".$log["logDate"]." ), Yesterday: ".$yesterday->format('Y-m-d H:i:s').", SW: ".$showWarning."<br/>";
					//if (strtotime($dateOfAck->format('Y-m-d H:i:s')) > strtotime($yesterday->format('Y-m-d H:i:s'))) { print "<br/>show warning<br/>"; }
					
					// if the alert was acknowledged since yesterday, show a warning...
					if (strtotime($dateOfAck->format('Y-m-d H:i:s')) > strtotime($yesterday->format('Y-m-d H:i:s')) && $showWarning == 0) { $showWarning = 1; }
					
					$lcount++;
				} // end foreach
								
			} // endif ackalerts exist
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;

?>			
		<!-- form start -->  

		<div id="formFrame">
			<p>
			<br/>
<?php
	// force DEV mode if we're on a dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/feintdev")) { 
		print "<strong>NOTICE: This is the development version of FEINT.  Incidents will default to the DEV instance of <br/>Service-Now and FireEye Acknowledgements will be skipped.</strong><br/><br/>";
		$mode = "DEV"; 
	}
?>
			Imported data from FireEye is shown below.  Submitting the form will create an Incident in Service-Now to the <br/>Call Center. <br/>
			
			<br/>
			<br/>
			</p>
			
			<div id="incidentShortDesc">
				<table style="width: 100%">
				<tr>
					<td>
					<h3>[SOC] / <?php print ucfirst($mwSev); ?> / <?php print $feSourceIP; ?> / <?php print $feMalwareName; ?></h3> 
<?php
				if ($showWarning == 1) {
?>
				<br/><span class='red'>Warning!</span> <?php print $feSourceIP; ?> has had alert(s) acknowledged in the last 24 hours. <br/>
<?php
				}
?>
						<div id="openINCWarning" style="display:none;"></div>
					</td>
					<td style="text-align: right; padding-right: 15px;">
<?php
		if ($userPermissions == "dbreadonly") {
?>
						Alert: <?php print $alertID; ?>
<?php
		} else {
?>						
						Alert: <a href="https://fireeye.iso.vt.edu/event_stream/events_for_bot?ev_id=<?php print $alertID; ?>"><?php print $alertID; ?></a>
<?php
		}
?>
					</td>
				</tr>
				</table>
			</div>
			
		<form>
			<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
			<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />
			<input type="hidden" id="incidentShort" name="incidentShort" value="[SOC] / <?php print ucfirst($mwSev); ?> / <?php print $feSourceIP; ?> / <?php print $feMalwareName; ?>" />
			<input type="hidden" id="feSourceIP" name="feSourceIP" value="<?php print $feSourceIP; ?>" />
			<input type="hidden" id="feAdditionalInfo" name="feAdditionalInfo" value="<?php print $feAdditionalInfo; ?>" />
			<input type="hidden" id="alertID" name="alertID" value="<?php print $alertID; ?>" />
			<input type="hidden" id="apiToken" name="apiToken" value="<?php print $apiToken; ?>" />
			<input type="hidden" id="feName" name="feName" value="<?php print $feName; ?>" />
			<input type="hidden" id="incidentID" name="incidentID" value="" />
<?php 
		if ($mwID == "NEW") {		
?>
			<input type="hidden" id="mwID" name="mwID" value="NEW" />
			<input type="hidden" id="mwFunc" name="mwFunc" value="add" />
			
<?php
		} else {
?>
			<input type="hidden" id="mwID" name="mwID" value="<?php print $mwID; ?>" />
			<input type="hidden" id="mwFunc" name="mwFunc" value="edit" />
<?php
		}
?>
			
		<table border="0" class="incidentFormTable">
		<tr>
			<td width="160px">Sensor Source: <br/></td>
			<td width="240px">
				<select id="feSensorSrc" name="feSensorSrc">
					<option value="fireeye" <?php print $feSensorGENERALSelected; ?>>FireEye-General</option>
					<option value="ALXFE" <?php print $feSensorALXFESelected; ?>>FireEye-ALXFE</option>
					<option value="BABYRUTH" <?php print $feSensorBABYRUTHSelected; ?>>FireEye-BABYRUTH</option>
					<option value="BLFE" <?php print $feSensorBLFESelected; ?>>FireEye-BLFE</option>
					<option value="BOUNTY" <?php print $feSensorBOUNTYSelected; ?>>FireEye-BOUNTY</option>
					<option value="FASTBREAK" <?php print $feSensorFASTBREAKSelected; ?>>FireEye-FASTBREAK</option>
					<option value="FCFE" <?php print $feSensorFCFESelected; ?>>FireEye-FCFE</option>
					<option value="MRBIG" <?php print $feSensorMRBIGSelected; ?>>FireEye-MRBIG</option>				
				</select>
			</td>			
			<td width="100px">Timestamp:</td>
			<td width="200px">
				<input type="text" id="feTimestamp" name="feTimestamp" value="<?php print $feTimestamp; ?>" size="27" /><br/>
				<?php //print $feTimestampServer; ?>
			</td>
		</tr>
		<tr>
			<td>Hostname:</td>
			<td colspan="2">
				<input type="text" id="feVictimHost" name="feVictimHost" value="<?php print $feVictimHost; ?>" size="45" />	
			</td>
			<td>
				<input type="button" id="showAckHistoryButton" value="<?php print $feSourceIP; ?> INC/ACK History" style="width: 217px;" />
			</td>
		</tr>
		<tr>
			<td>Communication Ports:</td>
			<td colspan="2">
				<p><input type="text" id="malwareComms" name="malwareComms" value="IP <?php print $feSourceIP.".".$feSourcePort." > ".$feDestinationIP.".".$feDestinationPort ?>" size="45" /></p>
<?php 
/*
				<p><input type="text" id="feSourcePort" name="feSourcePort" value="<?php print $feSourcePort; ?>" size="25" /></p>
				<p><input type="text" id="feDestinationIP" name="feDestinationIP" value="<?php print $feDestinationIP; ?>" size="25" /></p>
				<p><input type="text" id="feDestinationPort" name="feDestinationPort" value="<?php print $feDestinationPort; ?>" size="25" /></p>
*/
?>
			</td>
			<td>
				<input type="button" id="genArgusButton" value="Generate Argus Command" style="width: 217px;" />
			</td>
		</tr>
		<tr>
			<td>Detected Issue:</td>
			<td colspan="2">
				<input type="text" id="malwareName" name="malwareName" value="<?php print $feMalwareName; ?>" size="45" />
			</td>
			<td>
				<input type="button" id="genLAAButton" value="Generate CLS URL" style="width: 217px;" />
			</td>
		</tr>
		<tr>
			<td>Severity:</td>
			<td>
<?php 
				if ($userPermissions == "dbreadonly") {			
?>
				<?php print ucfirst($mwSev); ?>
				<input type="hidden" id="malwareSeverity" name="malwareSeverity" value="<?php print $mwSev; ?>" />
<?php
			 	} else {
?>
				<select id="malwareSeverity" name="malwareSeverity">
					<option value="critical" <?php print $mwSevCriticalSelected; ?>>Critical</option>
					<option value="high" <?php print $mwSevHighSelected; ?>>High</option>
					<option value="medium" <?php print $mwSevMediumSelected; ?>>Medium</option>
					<option value="low" <?php print $mwSevLowSelected; ?>>Low</option>
				</select>
<?php 
				 }
?>
			</td>
			<td colspan="2">
				
			<div id="feedback"></div>

<?php
/*
				<input type="button" id="updateMalware" value="Update Malware Description" style="width: 217px" />
*/
?>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				Incident-Specific Public Comments (Optional):
				<textarea id="incidentComments" name="incidentComments" rows="2" cols="105"></textarea>
				<input type="hidden" id="incidentCommentsTemp" value="<?php print $incidentCommentsTemp; ?>" /> <br/>
				
				Incident-Specific Private Work Notes (Optional):
				<textarea id="incidentNotes" name="incidentNotes" rows="2" cols="105"></textarea>
				<input type="hidden" id="incidentNotesTemp" value="<?php print $incidentNotesTemp; ?>" />  <br/>
				
				Malware Description:
				<textarea id="malwareDesc" name="malwareDesc" rows="8" cols="105"><?php print $mwDesc; ?></textarea>
				<input type="hidden" id="malwareDescTemp" value="<?php print $mwDescTemp; ?>" /> 
				<input type="hidden" id="malwareDescOrig" value="<?php print $mwDesc; ?>" /> 
			</td>
		</tr>
		<tr>
			<td>Caller PID:</td>
			<td>
				<input type="text" id="callerPID" name="callerPID" value="<?php print $casUser; ?>" size="25" /><br/>
				<?php /* Department: <div id="departmentName"></div> */ ?>
				<input type="hidden" id="departmentSysID" value="" />
				<input type="hidden" id="assignGroupSysID" value="" />
				<input type="hidden" id="callerPIDSysID" value="" />
			</td>
			
			<td colspan="2">Assignment Group: 
				<div class="right">
				<select id="assignmentGroup" name="assignmentGroup">
					<option value="callcenter">Call Center</option>
					<option value="itso">ITSO</option>
					<option value="nis">NIS</option>
					<option value="bamsit">BAMS IT</option>
					<option value="calsit">CALS IT</option>
					<option value="gsit">GS IT</option>
					<option value="libdeskav">Library Desktop/AV</option>
					<option value="vetmedit">VetMed IT</option>
				</select>
				</div>
			</td>
		</tr>
		<tr>
			<td>Contact Email(s):</td>
			<td colspan="2">
				<input type="text" id="contactEmail" name="contactEmail" value="<?php print $casUser; ?>@vt.edu" size="40" />
			</td>
			<td>
				(Comma-separated emails)
			</td>
		</tr>
		<tr>
			<td colspan="3">Submit public portions of the Incident as:
<?php 
				if ($userPermissions == "dbreadonly") {			
?>
				<span class="commNoteRadio"><label><input type="radio" id="snComment" name="snCommOrNote" value="comment" checked="checked"> Comments</label></span>
				<span class="commNoteRadio"><label><input type="radio" id="snWorknote" name="snCommOrNote" value="worknote"> Work Notes</label></span>
<?php
			 	} else {
?>
				<label><input type="radio" id="snComment" name="snCommOrNote" value="comments"> Comments</label>
				<label><input type="radio" id="snWorknote" name="snCommOrNote" value="worknotes" checked="checked"> Work Notes</label>
<?php
				}
?>
			</td>
			<td>
			<input type="button" id="submitIncident" value="Submit Incident" style="width: 217px;" /> 
			</td>
		</tr>
		</table>
	</form>
	<br/>
	<br/>
		</div>
			
		<div id="responseFrame" style="display:none;">
			<div id="response"><img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...</div>
			<br/>
			<br/>
			<a href="./index.php">Look up another FireEye Alert</a><br/>
		</div>

		<div id="showAckHistory" style="display:none;">
			<div id="ackHistoryLoading"><img src="./images/loading16x16.gif" /> Loading Acknowledgement/Incident History, please wait...</div>
			<div id="ackHistoryContainer"></div>
<?php 
		// rewritten to load the ack_history dialog async'd.  See /assets/ackhistory_funcs.php
		//include_once("ack_history.inc.php");		
?>
		</div>

		<div id="generateArgus" style="display:none;">
<?php 
		include_once("argus_dialog.inc.php");		
?>
		</div>
			
		<div id="generateLAA" style="display:none;">
<?php 
		include_once("laa_dialog.inc.php");		
?>
		</div>
			
		<div id="autoAssignmentGroup" style="display:none;">
			<input type="hidden" id="autoAssignmentFunction" value="enabled" />
			<div id="autoAssignmentGroupContent"></div>
			<div id="autoAssignmentGroupButtons">
				<br/>
				Click Confirm to set the Assignment Group for this Incident according to the SOP. <br/>
				<br/>
				If you want to disable this automation, click Cancel. <br/>
				<br/>
				<br/>
				<br/>
				<div class="right">
					<button id="autoAssignCancel" value="cancel">Cancel</button>
					<button id="autoAssignConfirm" value="confirm">Confirm</button>
				</div>
			</div>
		</div>
		
<?php 
	}  // END FE ALERT ID SET
?>
		</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>