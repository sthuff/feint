<?php 

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	meta_data.inc.php
	
	Included by index.php, update_malwaredb.php, url_tool.php
	
	Includes the meta data tags for the site.

****************************************************************************/

?>
	<!-- Meta Tags -->
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex, nofollow" />
		<meta name="keywords" content="FEINT FireEye ITSO Notification Tool">
		<meta name="description" content="FEINT FireEye ITSO Notification Tool">
<?php 

// EOF

?>