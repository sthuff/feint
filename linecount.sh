#!/bin/bash

LINECT=0; 
TOTAL=0;

filenames=`find ./*.php -type f -printf "%f\n"`

for eachfile in $filenames 
do
	while read -r LINE; do (( LINECT++ )); done < $eachfile; 
	echo $LINECT
	TOTAL=$((TOTAL+LINECT))
done
echo Total: $TOTAL
