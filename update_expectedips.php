<?php

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	update_expectedips.php
	
	
****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>FEINT - ITSO Expected IPs Update Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	//include_once("javascripts.inc.php");
?>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
	<link type="text/css" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<link rel="stylesheet" type="text/css" href="./DataTables/datatables.min.css"/>
	<script type="text/javascript" src="./DataTables/datatables.min.js"></script>

	<script>
		$(document).ready(function(){
			$('#malwareTable').DataTable({
				order: [0, 'asc'],
				"paging": true,
				"lengthMenu": [[25, 50, 100, 200, -1], [25, 50, 100, 200, "All"]],
				"deferRender": true
			});
			
<?php
		// UPDATE EXPECTED IPs
?>
			$("#updateExpectedIPs").click(updateExpectedIPs);
		});	// END DOCREADY
		
<?php
// Performs an update, insert or delete against the malware database with
// the mode based on the expFunc parameter that is POSTed to 
// malwaredb_funcs.php.  Additional details can be found in that file. 
?>
function updateExpectedIPs() {

	$("#feedback").html("<b>Processing action... please wait...</b>");

	$.post('./assets/expectedips_funcs.php', {
						expID: $("#expID").val(), expFunc: $("#expFunc").val(), 
						expectedIP: $("#expectedIP").val(), 
						expDesc: $("#expDesc").val(), 						
						casUser: $("#casUser").val()
	}, function(data) {

					$('#feedback').html(data);

	}).fail(function() {
		$('#feedback').html("Expected IPs DB operation failed.");
	});

	return false;	
}

	</script>

</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
		<div class="container">

<?php
	// NO FUNCTION - DISPLAY LIST
	// no function has been passed so display the list of malware entries
	if (!isset($_GET["expFunc"])) {
?>
	<!-- form start -->  

		<p style="width:800px">
			<br/>
			This tool will allow you to add or update the Expected Malware IPs database used in the <br/>
			FEINT - FireEye ITSO Notification Tool. <br/>
			<br/>
			Click on an IP or click Edit to update an entry.  Click Delete to remove the item from the database.<br/>
			<br/>
			<br/>
		</p>
<?php
		/* MALWARE INFO DB LOOKUP */ 		
		include_once("./assets/classes.inc.php");

		include("./assets/db_info.inc.php");
		$dbName = "feint";
		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT expID, expectedIP, expDesc, expDate FROM expectedips ORDER BY expectedIP ASC');
			$statement->execute();
?>
		<div id="formFrame" style="width:800px;">
			<form>

						<input type="hidden" id="expFunc" name="expFunc" value="edit" />
						<input type="hidden" id="expID" name="expID" value="" />
						<input type="hidden" id="expectedIP" name="expectedIP" value="" />
						<input type="hidden" id="expDesc" name="expDesc" value="" />
						<input type="hidden" id="casUser" name="casUser" value="<?php print $_SERVER["HTTP_CAS_UUPID"]; ?>" />

		<a href="./update_expectedips.php?expFunc=add">Add New Entry</a>
		<br/>
		<br/>

		<table id="malwareTable" class="display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>Expected IP</th>
					<th>Hostname</th>
					<th>Description</th>
					<th>Function</th>
				</tr>
				</thead>
				<tfoot>
					<th>Expected IP</th>
					<th>Hostname</th>
					<th>Description</th>
					<th>Function</th>
				</tfoot>
			<tbody>
<?php
			if ($statement->rowCount() > 0) {
				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($rows as $expIPItem) {
?>
				<tr>
					<td>
						<a href="update_expectedips.php?expID=<?php print $expIPItem["expID"]; ?>&expFunc=edit"><?php print $expIPItem["expectedIP"]; ?></a>
					</td>
					<td>
						<?php print gethostbyaddr($expIPItem["expectedIP"]); ?>
					</td>
					<td>
						<?php print $expIPItem["expDesc"]; ?>
					</td>
					<td class="deleteCell" style="display:block;">
						<a style="float:left;" href="update_expectedips.php?expID=<?php print $expIPItem["expID"]; ?>&expFunc=edit">Edit</a> 
						<a style="float:right;" href="update_expectedips.php?expID=<?php print $expIPItem["expID"]; ?>&expFunc=delete">Delete</a>
					</td>
				</tr>
<?php
				} // end foreach
			} else	{
				$expIPItem["expectedIP"] = "No Expected IPs entries found or funciton parameter invalid.  This is unlikely.  Perhaps an error?";
			}
			
		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
?>
			</table>

			<br/>
			<br/>			
			<a href="./update_expectedips.php?expFunc=add">Add New Entry</a>
			<br/>
			<br/>

			</form>
			<br/>
			<br/>			
		</div>
<?php

	}
	else {   // FUNCTION IS SET THROUGH GET VAR

		// ADD MODE
		if (isset($_GET["expFunc"]) && $_GET["expFunc"] == "add") {
			
			$expFunc = trim($_GET["expFunc"]);
			$expFunc = filter_var($expFunc, FILTER_SANITIZE_STRING);
?>
			<!-- form start -->  

			<div id="formFrame">

				<p style="width: 700px">
				Completing the form below will add an Expected IP item in the ITSO Database, which is used by the FEINT- FireEye ITSO Notification Tool.
				</p>

				<form>
					<input type="hidden" id="expFunc" name="expFunc" value="<?php print $expFunc; ?>" />
					<input type="hidden" id="expID" name="expID" value="NEW" />
					<input type="hidden" id="casUser" name="casUser" value="<?php print $_SERVER["HTTP_CAS_UUPID"]; ?>" />


				<table border="0" class="incidentFormTable">
				<tr>
					<td width="100px">Expected IP:</td>
					<td>
						<input type="text" id="expectedIP" name="expectedIP" value="" size="45" />
					</td>
				</tr>
				<tr>
					<td width="100px">Description:</td>
					<td>
						<input type="text" id="expDesc" name="expDesc" value="" size="45" />
					</td>
				</tr>
				<tr>
					<td>
					<input type="button" id="updateExpectedIPs" value="Add Expected IP Item" /> 
					</td>
					<td><div id="feedback" style="padding-left: 200px;"></div></td>
				</tr>
				</table>
				</form>
			</div>
<?php			
		} // END ADD MODE
		else {
			// EDIT AND DELETE REQUIRE AN ID...
		
			if (isset($_GET["expID"])) {
				// Trim whitespace and sanitize expID to an int
				$expID = trim($_GET["expID"]);
				$expID = filter_var($expID, FILTER_SANITIZE_NUMBER_INT);
			} else {
				print "<br/>ERROR: An ID value is required for this function.<br/>"; 
				exit;
			}

			// /* MALWARE INFO DB LOOKUP *

			include_once("./assets/classes.inc.php");

			include("./assets/db_info.inc.php");
			$dbName = "feint";
			try {
				$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
				unset($dbUser);
				unset($dbPass);

				$statement = $connection->prepare('SELECT expID, expectedIP, expDesc, expDate FROM expectedips WHERE expID = :expID LIMIT 1');
				$statement->execute(array('expID' => $expID));

				if ($statement->rowCount() > 0) {
					$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
				
					foreach ($rows as $expIPItem) {
						$expID = $expIPItem["expID"]; 
						$expectedIP = $expIPItem["expectedIP"]; 
						$expDesc = $expIPItem["expDesc"]; 
						$expDate = $expIPItem["expDate"]; 
					}
				} else {
					$expectedIP = "";
					$expDesc = "";
					$expDate = "";
				}
			}
			catch(PDOException $e) { print "Error: ".$e->getMessage(); }

			// EDIT MODE
			if (isset($_GET["expFunc"]) && $_GET["expFunc"] == "edit") {

				$expFunc = trim($_GET["expFunc"]);
				$expFunc = filter_var($expFunc, FILTER_SANITIZE_STRING);
?>		
			<!-- form start -->  

			<div id="formFrame">

				<p style="width: 700px">
				Modifying the information below will update the item in the ITSO Expected IPs Database, which is used by the FEINT- FireEye ITSO Notification Tool.
				</p>

				<form>
					<input type="hidden" id="expFunc" name="expFunc" value="<?php print $expFunc; ?>" />
					<input type="hidden" id="expID" name="expID" value="<?php print $expID; ?>" />
					<input type="hidden" id="casUser" name="casUser" value="<?php print $_SERVER["HTTP_CAS_UUPID"]; ?>" />


				<table border="0" class="incidentFormTable">
				<tr>
					<td width="100px">Expected IP:</td>
					<td>
						<input type="text" id="expectedIP" name="expectedIP" value="<?php print $expectedIP; ?>" size="45" /> <br/>
						Resolves to: <?php print gethostbyaddr($expectedIP); ?> <br/>
					</td>
				</tr>
				<tr>
					<td width="100px">Description:</td>
					<td>
						<input type="text" id="expDesc" name="expDesc" value="<?php print $expDesc; ?>" size="45" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="button" id="updateExpectedIPs" value="Update Expected IP Item" /> <br/>
						<a href="./update_expectedips.php?expID=<?php print $expID; ?>&expFunc=delete">Delete Item</a> <br/>
					</td>
					<td><div id="feedback" style="padding-left: 200px;"></div></td>
				</tr>
				</table>
				</form>
			</div>
<?php 
			} // END EDIT MODE
			elseif (isset($_GET["expFunc"]) && $_GET["expFunc"] == "delete") {
			// DELETE MODE

				$expFunc = trim($_GET["expFunc"]);
				$expFunc = filter_var($expFunc, FILTER_SANITIZE_STRING);

?>
			<p>This function will DELETE the following entry in the ITSO Expected IPs Database.  Please use with caution.</p>
			<!-- form start -->  

			<div id="formFrame">
				<form>
					<input type="hidden" id="expFunc" name="expFunc" value="<?php print $expFunc; ?>" />
					<input type="hidden" id="expID" name="expID" value="<?php print $expID; ?>" />
					<input type="hidden" id="expectedIP" name="expectedIP" value="<?php print $expectedIP; ?>" />
					<input type="hidden" id="expDesc" name="expDesc" value="<?php print $expDesc; ?>" />
					<input type="hidden" id="casUser" name="casUser" value="<?php print $_SERVER["HTTP_CAS_UUPID"]; ?>" />

				<table border="0" class="incidentFormTable">
				<tr>
					<td width="100px">Expected IP:</td>
					<td>
						<?php print $expectedIP; ?> <br/>					
					</td>
				</tr>
				<tr>
					<td width="100px">Hostname:</td>
					<td>
						<?php print gethostbyaddr($expectedIP); ?> <br/>
					</td>
				</tr>
				<tr>
					<td width="100px">Description:</td>
					<td>
						<?php print $expDesc; ?>
					</td>
				</tr>
				<tr>
					<td>
					<input type="button" id="updateExpectedIPs" value="Delete Expected IP Item" /> 
					</td>
					<td><div id="feedback" style="padding-left: 200px;"></div></td>
				</tr>
				</table>
				</form>
			</div>
<?php
			} // END DELETE MODE
			else {
				// BAD FUNCTION MODE OR INVALID ID
				print "ERROR: Invalid ID or function parameter is invalid. <br/>";
			} // END CATCH-ALL-ELSE

		} // END ADD MODE ELSE	

	}  // END FUNC SET
	
?>
		</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>