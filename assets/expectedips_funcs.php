<?php
/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	expectedips_funcs.php
	
	Some basic error-checking to avoid duplicate items is performed before
	adding new items to the database.
	
****************************************************************************/

	// Trim and sanitize posted variables...
    $expID = trim($_POST["expID"]);
	$expID = filter_var($expID, FILTER_SANITIZE_NUMBER_INT);

    $expFunc = trim($_POST["expFunc"]);
	$expFunc = filter_var($expFunc, FILTER_SANITIZE_STRING);

    $expectedIP = trim($_POST["expectedIP"]);
	$expectedIP = filter_var($expectedIP, FILTER_SANITIZE_STRING);

	$expDesc = trim($_POST["expDesc"]);
	$expDesc = filter_var($expDesc, FILTER_SANITIZE_STRING);

    $expDate = trim($_POST["expDate"]);
	$expDate = filter_var($expDate, FILTER_SANITIZE_STRING);

    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);


	/* MALWARE INFO DB LOOKUP */ 

	include_once("./classes.inc.php");

	include("./db_info.inc.php");
	$dbName = "feint";

	if ($expFunc == "add") {
		
		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT expID, expectedIP, expDesc, expDate FROM expectedips WHERE expectedIP = :expectedIP LIMIT 1');
			$statement->execute(array('expectdIP' => $expectedIP));

			if ($statement->rowCount() > 0) {
				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

				foreach ($rows as $expIPItem) {
					$expID = $expIPItem["expID"]; 
					$expectedIP = $expIPItem["expectedIP"]; 
					$expDesc = $expIPItem["expDesc"]; 
					$expDate = $expIPItem["expDate"]; 
				}
				
?>
				<strong>Error:</strong> Couldn't add record because it already exists. <br/>
				<a href="update_expectedips.php?expFunc=add">Add another record</a><br/>
				<a href="update_expectedips.php?expFunc=edit&expID=<?php print $expID; ?>">Go to existing record</a><br/>
<?php
			} else {
			
				$statement = $connection->prepare("INSERT INTO expectedips (expectedIP, expDesc) VALUES (:expectedIP, :expDesc)");
				$statement->execute(array("expectedIP" => $expectedIP, "expDesc" => $expDesc));
				$expID = $connection->lastInsertId();

				print "Record for ".$expectedIP." added successfully.<br/>";
				print "<a href=\"update_expectedips.php?expFunc=add\">Add another record</a><br/>";
				
				// Log the action

				try {
					$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
					$statement->execute(array("logUser" => $casUser, "logType" => "addExpectedIP", "logDataID" => $expID, "logInfo" => $expectedIP));
				}
				catch(PDOException $e) { print "Error: ".$e->getMessage(); }

			}
		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
		
	} elseif ($expFunc == "edit") {

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare("UPDATE expectedips SET expectedIP = :expectedIP, expDesc = :expDesc WHERE expID = :expID");
			$statement->bindValue(":expectedIP", $expectedIP, PDO::PARAM_STR); 
			$statement->bindValue(":expDesc", $expDesc, PDO::PARAM_STR); 
			$statement->bindValue(":expID", $expID, PDO::PARAM_INT); 
			$update = $statement->execute();

			print "Record for ".$expectedIP." updated successfully.<br/>";
			
			// Log the action

			try {
				$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
				$statement->execute(array("logUser" => $casUser, "logType" => "editExpectedIP", "logDataID" => $expID, "logInfo" => $expectedIP));
			}
			catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
		
	} elseif ($expFunc == "delete") {

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare("DELETE FROM expectedips WHERE expID = :expID");
			$statement->bindValue(":expID", $expID, PDO::PARAM_INT); 
			$statement->execute();
			
			print "Record for ".$expectedIP." deleted successfully.<br/>";
			
			try {
				$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
				$statement->execute(array("logUser" => $casUser, "logType" => "deleteExpectedIP", "logDataID" => $expID, "logInfo" => $expectedIP));
			}
			catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}

	} else {
		print "<br/>Error: Function not recognized. <br/>";
		exit;
	}

	// clear the connection
	$connection = null;
?>