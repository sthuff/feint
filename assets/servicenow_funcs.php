<?php

	$mode = "LIVE";
	$modeURL = "";

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// force DEV mode if we're on a dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/feintdev")) { $mode = "DEV"; $modeURL = "dev"; }

function snAPIQuery($url, $user, $pass, $jsonData, $type) {
		
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERPWD, $user.":".$pass);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	//curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Accept: application/json'));
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	
	if ($type == "insert") { 
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	}
	elseif ($type == "update") { 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	} 
	elseif ($type == "get") {
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
	}	

	$snResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "<br/>Unable to connect to the Service-Now API.";
		print "<br/>Error info: ".curl_error($ch);
		exit("<br/>Halting execution since Service-Now could not be reached.");
	}

	curl_close($ch);

	//print "<br/>DEBUG: RAW Response is: <br/>";
	//var_dump($snResponse);

	//$snInciXML = simplexml_load_string($snInciResponse);
	//$snInciJSON = json_encode($snInciXML);
	
	//print "<br/>DEBUG: JSON Response is: <br/>";
	//var_dump($snInciJSON);
	
	//return $snInciJSON;
	return $snResponse;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (isset($_POST["formAction"]) && $_POST["formAction"] == "verifyPID") {

	// Trim and sanitize posted variables...
	$callerPID = trim($_POST["callerPID"]);
	$callerPID = filter_var($callerPID, FILTER_SANITIZE_STRING);
	
    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);
	
	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
	
	/////////////////////////////////////////////////////////////
	//
	//	Get User Info
	//
	/////////////////////////////////////////////////////////////

		include("sn_info.inc.php");

		$snCASUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$casUser; 

		if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

		$callerNotFound = 0;

		$snGetCASUser = snAPIQuery($snCASUserURL, $snUser, $snPass, $casUser, "get");

		//print "<br/>DEBUG: snGetCASUser is: ".$snGetCASUser."<br/>";


		if ($snGetCASUser == "{\"result\":[]}") { 
			print "Somehow YOU were not found in Service-Now... This should never happen.  Halting.";
			exit;
		}

		$snCASUserJSONResponse = json_decode($snGetCASUser);

		$snCASUserID = $snCASUserJSONResponse->result[0]->sys_id;
		$snCASUserPhone = $snCASUserJSONResponse->result[0]->phone;

		// GET ITSO USER value
		//print "DEBUG: ITSO User is: ".$casUser."<br/>";

		$snCallerURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$callerPID; 

		if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

		$snGetCaller = snAPIQuery($snCallerURL, $snUser, $snPass, $callerPID, "get");

		//print "<br/>DEBUG: snGetCaller is: ".$snGetCaller."<br/>";

		// if we get an empty result...
		if ($snGetCaller == "{\"result\":[]}") { 

			//print "<br/>Caller value was not found.  Setting the Caller value to ".$casUser."<br/>"; 
			$callerNotFound = 1;
			$snGetCaller = $snGetCASUser;
		}

		$snGetCallerJSONResponse = json_decode($snGetCaller);

		//print "DEBUG: <br/>";
		//var_dump($snGetCallerJSONResponse);
		//print "<br/>";
	
		// u_department_number string
		$snCallerID = $snGetCallerJSONResponse->result[0]->sys_id;
		$snCallerDeptID = $snGetCallerJSONResponse->result[0]->department->value;

		//print "<br/>DEBUG: SN CAS UserID is: ".$snCASUserID."<br/>";
		//print "<br/>DEBUG: SN Caller UserID is: ".$snCallerID."<br/>";
		//print "<br/>DEBUG: SN Caller DeptID is: ".$snCallerDeptID."<br/>";

		// Get DEPARTMENT value

		if ($mode == "DEV") { $snDeptURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/cmn_department?sys_id=".$snCallerDeptID; } 
		else { $snDeptURL = "https://vt4help.service-now.com/api/now/table/cmn_department?sys_id=".$snCallerDeptID; } 

		if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

		$snGetCallerDept = snAPIQuery($snDeptURL, $snUser, $snPass, $callerPID, "get");
	
		// if we get an empty result...
		if ($snGetCallerDept == "{\"result\":[]}") { 

			//print "<br/>Caller Department value was not found.  Setting the Caller Department value to 'UnknownDept'<br/>"; 
			$callerDeptNotFound = 1;
			$snGetCallerDept = "UnknownDept";
		}

		$snGetCallerDeptJSONResponse = json_decode($snGetCallerDept);
	
		//print "<br/>DEBUG: snGetCallerDept is: ";
		//print "<textarea>".$snGetCallerDept."</textarea>";
		//print "<br/>";

		// Store the SN folder name sys_id in case we have to perform a SN lookup later...
		$departmentSysID = trim($snGetCallerDeptJSONResponse->result[0]->sys_id);
		$departmentSysID = filter_var($departmentSysID, FILTER_SANITIZE_STRING);

		// Sanitize and remove characters that Nessus will choke on
	
		$departmentName = trim($snGetCallerDeptJSONResponse->result[0]->name);
		$departmentName = preg_replace("([^\w\s\d\-_\[\].])", '', $departmentName);
		$departmentName = preg_replace("([\.]{2,})", '', $departmentName);
		//$departmentName = str_replace(" ","_",$departmentName);
	
		//print "<br/>DEBUG: snGetCallerDept Name is: ".$snGetCallerDeptJSONResponse->result[0]->name.", Dept Name is:".$departmentName."<br/>";

		print $callerNotFound."|DATA|".$callerPID."|DATA|".$departmentName."|DATA|".$departmentSysID."|DATA|".$snCallerID."|DATA|".$snCASUserID;
}

if (isset($_POST["formAction"])) {
	if ($_POST["formAction"] == "submitIncident" || $_POST["formAction"] == "updateIncident") {

		// use LIVE or DEV API URLs?

		// Trim and sanitize posted variables...
		$alertID = trim($_POST["alertID"]);
		$alertID = filter_var($alertID, FILTER_SANITIZE_NUMBER_INT);

		$feSourceIP = trim($_POST["feSourceIP"]);
		$feSourceIP = filter_var($feSourceIP, FILTER_SANITIZE_STRING);

		$feSensorSrc = trim($_POST["feSensorSrc"]);
		$feSensorSrc = filter_var($feSensorSrc, FILTER_SANITIZE_STRING);

		$feTimestamp = trim($_POST["feTimestamp"]);
		$feTimestamp = filter_var($feTimestamp, FILTER_SANITIZE_STRING);

		$feVictimHost = trim($_POST["feVictimHost"]);
		$feVictimHost = filter_var($feVictimHost, FILTER_SANITIZE_STRING);

		$malwareComms = trim($_POST["malwareComms"]);
		$malwareComms = filter_var($malwareComms, FILTER_SANITIZE_STRING);

		$malwareName = trim($_POST["malwareName"]);
		$malwareName = filter_var($malwareName, FILTER_SANITIZE_STRING);

		$malwareSeverity = trim($_POST["malwareSeverity"]);
		$malwareSeverity = filter_var($malwareSeverity, FILTER_SANITIZE_STRING);

		$malwareDesc = trim($_POST["malwareDesc"]);
		$malwareDesc = filter_var($malwareDesc, FILTER_SANITIZE_STRING);

		$incidentComments = trim($_POST["incidentComments"]);
		$incidentComments = filter_var($incidentComments, FILTER_SANITIZE_STRING);

		$incidentNotes = trim($_POST["incidentNotes"]);
		$incidentNotes = filter_var($incidentNotes, FILTER_SANITIZE_STRING);

		$feAdditionalInfo = trim($_POST["feAdditionalInfo"]);
		$feAdditionalInfo = filter_var($feAdditionalInfo, FILTER_SANITIZE_STRING);

		$contactEmail = trim($_POST["contactEmail"]);
		$contactEmail = filter_var($contactEmail, FILTER_SANITIZE_STRING);

		$snCommOrNote = trim($_POST["snCommOrNote"]);
		$snCommOrNote = filter_var($snCommOrNote, FILTER_SANITIZE_STRING);

		$casUser = trim($_POST["casUser"]);
		$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);

		$userPermissions = trim($_POST["userPermissions"]);
		$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);

		$callerPID = trim($_POST["callerPID"]);
		$callerPID = filter_var($callerPID, FILTER_SANITIZE_STRING);

		$assignmentGroup = trim($_POST["assignmentGroup"]);
		$assignmentGroup = filter_var($assignmentGroup, FILTER_SANITIZE_STRING);

		if ($_POST["formAction"] == "updateIncident") {

			$incToUpdate = trim($_POST["incToUpdate"]);
			$incToUpdate = filter_var($incToUpdate, FILTER_SANITIZE_STRING);	
		}

		/*
		[SOC] / High / IP  / Trojan.Ransomware.Locky


		Severity: High
		IP Address: 
		Hostname: 
		Timestamp: 
		Sensor Source: FireEye
		Detected Issue: Trojan.Ransomware.Locky
		Communication IPs and Ports:
		*/

		/////////////////////////////////////////////////////////////
		//
		//	Get User Info
		//
		/////////////////////////////////////////////////////////////

			include("sn_info.inc.php");

			//if ($mode == "DEV") { $snCASUserURL = "https://vt4helpdev.service-now.com/api/now/table/sys_user?email=".$casUser."%40vt.edu"; } 
			//else { $snCASUserURL = "https://vt4help.service-now.com/api/now/table/sys_user?email=".$casUser."%40vt.edu"; } 

			if ($mode == "DEV") { $snCASUserURL = "https://vt4helpdev.service-now.com/api/now/table/sys_user?user_name=".$casUser; } 
			else { $snCASUserURL = "https://vt4help.service-now.com/api/now/table/sys_user?user_name=".$casUser; } 

			if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

			$callerNotFound = 0;

			$snGetCASUser = snAPIQuery($snCASUserURL, $snUser, $snPass, $casUser, "get");

			//print "<br/>DEBUG: snGetCASUser is: ".$snGetCASUser."<br/>";


			if ($snGetCASUser == "{\"result\":[]}") { 
				print "Somehow YOU were not found in Service-Now... This should never happen.  Halting.";
				exit;
			}

			$snCASUserJSONResponse = json_decode($snGetCASUser);

			$snCASUserID = $snCASUserJSONResponse->result[0]->sys_id;
			$snCASUserPhone = $snCASUserJSONResponse->result[0]->phone;

			// GET ITSO USER value
			//print "DEBUG: ITSO User is: ".$casUser."<br/>";

			//if ($mode == "DEV") { $snCallerURL = "https://vt4helpdev.service-now.com/api/now/table/sys_user?email=".$callerPID."%40vt.edu"; } 
			//else { $snCallerURL = "https://vt4help.service-now.com/api/now/table/sys_user?email=".$callerPID."%40vt.edu"; } 

			if ($mode == "DEV") { $snCallerURL = "https://vt4helpdev.service-now.com/api/now/table/sys_user?user_name=".$callerPID; } 
			else { $snCallerURL = "https://vt4help.service-now.com/api/now/table/sys_user?user_name=".$callerPID; } 

			if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

			$snGetCaller = snAPIQuery($snCallerURL, $snUser, $snPass, $callerPID, "get");

			//print "<br/>DEBUG: snGetCaller is: ".$snGetCaller."<br/>";

			// if we get an empty result...
			if ($snGetCaller == "{\"result\":[]}") { 

				print "<br/>Caller value was not found.  Setting the Caller value to ".$casUser."<br/>"; 
				$callerNotFound = 1;
				$snGetCaller = $snGetCASUser;
			}

			$snGetCallerJSONResponse = json_decode($snGetCaller);

			$snCallerID = $snGetCallerJSONResponse->result[0]->sys_id;
			$snCallerPhone = $snGetCallerJSONResponse->result[0]->phone;

			//print "<br/>DEBUG: SN CAS UserID is: ".$snCASUserID."<br/>";

			//print "<br/>DEBUG: SN Caller UserID is: ".$snCallerID."<br/>";

		/////////////////////////////////////////////////////////////
		//
		//	Insert Incident
		//
		/////////////////////////////////////////////////////////////

			include("sn_info.inc.php");

			$jsonDataIns["short_description"] = "[SOC] / ".ucfirst($malwareSeverity)." / ".$feSourceIP." / ".$malwareName;

			$jsonDataIns["u_vt_alt_phone"] = $snCallerPhone;
			$jsonDataIns["sys_created_by"] = $casUser;


			//print "DEBUG: Severity is: ".$malwareSeverity."<br/>";

			if ($malwareSeverity == "critical") {
				$jsonDataIns["impact"] = "3";
				$jsonDataIns["urgency"] = "1";
			} else {
				$jsonDataIns["impact"] = "3";
				$jsonDataIns["urgency"] = "2";		
			}


			//$jsonDataIns["opened_by"]["link"] = "https://vt4helpdev.service-now.com/api/now/table/sys_user/".$snUser;
			//$jsonDataIns["opened_by"]["value"] = $snUser;
			$jsonDataIns["opened_by"] = $snCASUserID;

			//$jsonDataIns["caller_id"]["link"] = "https://vt4helpdev.service-now.com/api/now/table/sys_user/".$snUser;
			//$jsonDataIns["caller_id"]["value"] = $snUser;
			$jsonDataIns["caller_id"] = $snCallerID;

		
			// BAMS IT == cdb565ec0f52a100ee5a0bcce1050ef5 SYS ID
			// BAMS IT == fae7b3830f436a00d3254b9ce1050ec9 ASSIGN GRUOP ID
			
			// CALS IT == a7a565ec0f52a100ee5a0bcce1050ea1 SYS ID
			// CALS IT == 62fd3c430f9aa600d3254b9ce1050e43 ASSIGN GROUP ID
			
			// GS IT == 50b565ec0f52a100ee5a0bcce1050ed8 SYS ID
			// GS IT == f4391b17dbf9cfc4e3a0f839af961906 ASSIGN GROUP ID
		
			// LIBDESKAV == 44b565ec0f52a100ee5a0bcce1050ec1 SYS ID
			// LIBDESKAV == 90eb64a1db414340e3a0f839af961958 ASSIGN GROUP ID
			
			// VETMED IT == d0b565ec0f52a100ee5a0bcce1050ed5 SYS ID
			// VETMED IT == c5abd3c8db5a8700d05152625b96192b ASSIGN GROUP ID
		
			if ($userPermissions == "dbreadonly" && $malwareDesc == "") {
				$jsonDataIns["assignment_group"] = "bd5215e70f362900ee5a0bcce1050e1d";	// ITSO Group
			} else { 
				if ($assignmentGroup == "nis") {
					$jsonDataIns["assignment_group"] = "d6f170146f88b10063db9f5e5d3ee423";	// NIS  d6f170146f88b10063db9f5e5d3ee423
				} else if ($assignmentGroup == "itso") {
					$jsonDataIns["assignment_group"] = "bd5215e70f362900ee5a0bcce1050e1d";	// ITSO Group	bd5215e70f362900ee5a0bcce1050e1d		
				} else if ($assignmentGroup == "bamsit") {
					$jsonDataIns["assignment_group"] = "fae7b3830f436a00d3254b9ce1050ec9";	// BAMS Group	fae7b3830f436a00d3254b9ce1050ec9		
				} else if ($assignmentGroup == "calsit") {
					$jsonDataIns["assignment_group"] = "62fd3c430f9aa600d3254b9ce1050e43";	// CALS IT Group	62fd3c430f9aa600d3254b9ce1050e43		
				} else if ($assignmentGroup == "gsit") {
					$jsonDataIns["assignment_group"] = "f4391b17dbf9cfc4e3a0f839af961906";	// GS IT Group	f4391b17dbf9cfc4e3a0f839af961906		
				} else if ($assignmentGroup == "libdeskav") {
					$jsonDataIns["assignment_group"] = "90eb64a1db414340e3a0f839af961958";	// LIBDESKAV Group	90eb64a1db414340e3a0f839af961958		
				} else if ($assignmentGroup == "vetmedit") {
					$jsonDataIns["assignment_group"] = "c5abd3c8db5a8700d05152625b96192b";	// VETMED IT Group	c5abd3c8db5a8700d05152625b96192b		
				} else {
						$jsonDataIns["assignment_group"] = "1865c3040f0a6100d3254b9ce1050e1f";	// Call Center Group	1865c3040f0a6100d3254b9ce1050e1f
				}		
			}

			//print "DEBUG: Assignment Group is: ".$assignmentGroup.", ".$jsonDataIns["assignment_group"]."<br/>";
		
			// if the ITSO is submitting a ticket that it will handle, assign the incident to the user submitting it
			if ($userPermissions == "dbreadwrite" && $assignmentGroup == "itso") {
				$jsonDataIns["assigned_to"] = $snCASUserID;
			}

			$jsonDataIns["contact_type"] = "email";

			$jsonDataIns["category"] = "Security";
			$jsonDataIns["subcategory"] = "Compromised Computer";

		/////////////////////////////////////////////////////////////
		//
		//	Verify Email Contacts
		//
		/////////////////////////////////////////////////////////////

			$jsonDataIns["u_vt_alt_email"] = $callerPID."@vt.edu";
			$missingEmailList = "";

			// in case the email contact list is empty...
			if ($contactEmail == "" || $contactEmail == $callerPID."@vt.edu") {
				//print "EMPTY contactEmail<br/>";

				$jsonDataIns["u_vt_alt_email"] = $callerPID."@vt.edu";
			}
			else {
				//print "contactEmail NOT EMPTY<br/>";
				$emailList = explode(",", $contactEmail);

				$jsonDataIns["u_vt_alt_email"] = "";

				foreach ($emailList as $email) {
					if ($email == "itso-soc-g@vt.edu") {
						$jsonDataIns["u_vt_alt_email"] .= "itso-soc-g@vt.edu,";
					}
					else {
						// check each  email (except for itso-soc-g@vt.edu)
						$contactPID = str_replace("@vt.edu", "", $email);
						//print "Contact PID: ".$contactPID."<br/>";

						$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$contactPID;  
						$snGetUser = snAPIQuery($snUserURL, $snUser, $snPass, $casUser, "get");

						//print "<br/>DEBUG: <br/>".$snGetUser."<br/>";

						// if contactPID isn't found...
						if ($snGetUser == "{\"result\":[]}") {
							$missingEmailList .= $contactPID."@vt.edu,";

						} else {
							// if contactPID is found in SN, add the email to the contactEmail list
							$jsonDataIns["u_vt_alt_email"] .= $contactPID."@vt.edu,";
						}
					}				
				}

				// remove last comma on contactEmail list
				$jsonDataIns["u_vt_alt_email"] = substr($jsonDataIns["u_vt_alt_email"], 0, -1);	

				if ($missingEmailList != "") {
					// remove last comma if $missingEmail isn't empty
					$missingEmailList = substr($missingEmailList, 0, -1);
				}
			}	

			//print "<br/>DEBUG contactEmail is: ".$jsonDataIns["u_vt_alt_email"]."<br/>";
			//print "<br/>DEBUG missingEmail is: ".$missingEmailList."<br/>";


			if ($snCommOrNote == "comments") {

				$jsonDataIns["comments"]  = "Severity: ".ucfirst($malwareSeverity)."\n";
				$jsonDataIns["comments"] .= "IP Address: ".$feSourceIP."\n";
				$jsonDataIns["comments"] .= "Hostname: ".$feVictimHost."\n";
				$jsonDataIns["comments"] .= "Timestamp: ".$feTimestamp."\n";
				$jsonDataIns["comments"] .= "Sensor Source: FireEye-".$feSensorSrc."\n";
				$jsonDataIns["comments"] .= "Detected Issue: ".$malwareName."\n";
				$jsonDataIns["comments"] .= "Communication IPs and Ports: ".$malwareComms."\n";
				if ($incidentComments != "") { $jsonDataIns["comments"] .= "\n".$incidentComments."\n"; }
				$jsonDataIns["comments"] .= "\n".$malwareDesc."\n";	

			} else {
				$jsonDataIns["work_notes"]  = "Severity: ".ucfirst($malwareSeverity)."\n";
				$jsonDataIns["work_notes"] .= "IP Address: ".$feSourceIP."\n";
				$jsonDataIns["work_notes"] .= "Hostname: ".$feVictimHost."\n";
				$jsonDataIns["work_notes"] .= "Timestamp: ".$feTimestamp."\n";
				$jsonDataIns["work_notes"] .= "Sensor Source: FireEye-".$feSensorSrc."\n";
				$jsonDataIns["work_notes"] .= "Detected Issue: ".$malwareName."\n";
				$jsonDataIns["work_notes"] .= "Communication IPs and Ports: ".$malwareComms."\n";
				if ($incidentComments != "") { $jsonDataIns["work_notes"] .= "\n".$incidentComments."\n"; }
				$jsonDataIns["work_notes"] .= "\n".$malwareDesc."\n";		
			}
			//print "<br/>DEBUG: RAW jsonDataIns is: <br/>";
			//print_r($jsonDataIns);

		// if we're updating an existing incident...
		if (isset($_POST["incToUpdate"])) {

			print "Updating incident...<br/>";

			$incToUpdate = trim($_POST["incToUpdate"]);
			$incToUpdate = filter_var($incToUpdate, FILTER_SANITIZE_STRING);	

			if ($mode == "DEV") { $snInciURLUpdate = "https://vt4helpdev.service-now.com/api/now/table/incident?number=".$incToUpdate; }
			else { $snInciURLUpdate = "https://vt4help.service-now.com/api/now/table/incident?number=".$incToUpdate; }

			if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

			$snGetIncident = snAPIQuery($snInciURLUpdate, $snUser, $snPass, $incToUpdate, "get");

			//print "<br/>DEBUG: snGetIncident is: ".$snGetIncident."<br/>";

			// if we get an empty result...
			if ($snGetIncident == "{\"result\":[]}") { 

				print "<br/>Incident was not found.<br/>"; 
				$incidentNotFound = 1;
				exit;
			}

			$snGetIncidentJSONResponse = json_decode($snGetIncident);

			$snIncidentSysID = $snGetIncidentJSONResponse->result[0]->sys_id;

			//print "<br/>DEBUG: SN INC SysID is: ".$snIncidentID."<br/>";

			if ($mode == "DEV") { $snInciURLUpdate = "https://vt4helpdev.service-now.com/api/now/table/incident/".$snIncidentSysID; }
			else { $snInciURLUpdate = "https://vt4help.service-now.com/api/now/table/incident/".$snIncidentSysID; }

			if ($incidentNotes != "") { $jsonDataUpd["work_notes"] = "Notes from ".$casUser.":\n\n".$incidentNotes."\n\n"; }

			if (isset($jsonDataUpd["work_notes"]) && $jsonDataUpd["work_notes"] != "") { $jsonDataUpd["work_notes"] .= $feAdditionalInfo; }
			else { $jsonDataUpd["work_notes"] = $feAdditionalInfo; }

			$jsonDataUpd["sys_updated_by"] = $casUser;
			if ($snCommOrNote == "comments") {

				$jsonDataUpd["comments"]  = "Severity: ".ucfirst($malwareSeverity)."\n";
				$jsonDataUpd["comments"] .= "IP Address: ".$feSourceIP."\n";
				$jsonDataUpd["comments"] .= "Hostname: ".$feVictimHost."\n";
				$jsonDataUpd["comments"] .= "Timestamp: ".$feTimestamp."\n";
				$jsonDataUpd["comments"] .= "Sensor Source: FireEye-".$feSensorSrc."\n";
				$jsonDataUpd["comments"] .= "Detected Issue: ".$malwareName."\n";
				$jsonDataUpd["comments"] .= "Communication IPs and Ports: ".$malwareComms."\n";
				if ($incidentComments != "") { $jsonDataUpd["comments"] .= "\n".$incidentComments."\n"; }
				$jsonDataUpd["comments"] .= "\n".$malwareDesc."\n";	

			} else {
				$jsonDataUpd["work_notes"]  = "Severity: ".ucfirst($malwareSeverity)."\n";
				$jsonDataUpd["work_notes"] .= "IP Address: ".$feSourceIP."\n";
				$jsonDataUpd["work_notes"] .= "Hostname: ".$feVictimHost."\n";
				$jsonDataUpd["work_notes"] .= "Timestamp: ".$feTimestamp."\n";
				$jsonDataUpd["work_notes"] .= "Sensor Source: FireEye-".$feSensorSrc."\n";
				$jsonDataUpd["work_notes"] .= "Detected Issue: ".$malwareName."\n";
				$jsonDataUpd["work_notes"] .= "Communication IPs and Ports: ".$malwareComms."\n";
				if ($incidentComments != "") { $jsonDataUpd["work_notes"] .= "\n".$incidentComments."\n"; }
				$jsonDataUpd["work_notes"] .= "\n".$malwareDesc."\n";		
			}

			//print "<br/>DEBUG: URL is: ".$snInciURLUpdate."<br/>";

			$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

			//print "<br/>DEBUG: ENC jsonDataUpdEnc is: <br/>";
			//print $jsonDataUpdEnc;

			$snIncidentUpdate = snAPIQuery($snInciURLUpdate, $snUser, $snPass, $jsonDataUpdEnc, "update");

			$snUpdateJSONResponse = json_decode($snIncidentUpdate);
			//print "<br/>DEBUG: Update response JSON is: <br/>".$snIncidentUpdate."<br/>";

			if (isset($snUpdateJSONResponse->result->number)) {
				$snIncidentUpdateNumber = $snIncidentNumberUpdate = $snUpdateJSONResponse->result->number;
				print "<br/>Incident ".$snIncidentUpdateNumber." updated with new FE Alert data.<br/>";

			}
			else {
				print "<br/>Error updating incident.<br/>";
			}


			unset($snUser);
			unset($snPass);

		}
		// we're inserting a new incident...
		else {

			//$timeNowEasternTZ = new DateTime('now', new DateTimeZone('America/New_York'));
			//$nowTimestamp = $timeNowEasternTZ->format('Y-m-d H:i:s');

			//print "<br/>DEBUG: NOW IS ".$nowTimestamp."<br/>";

			//$jsonDataIns["opened_at"] = $nowTimestamp;

			if ($mode == "DEV") { $snInciURLInsert = "https://vt4helpdev.service-now.com/api/now/table/incident?sysparm_action=insert"; }
			else { $snInciURLInsert = "https://vt4help.service-now.com/api/now/table/incident?sysparm_action=insert"; }

			$jsonDataInsEnc = json_encode($jsonDataIns, JSON_FORCE_OBJECT);

			//print "<br/>DEBUG: ENC jsonDataInsEnc is: <br/>";
			//print $jsonDataInsEnc;

			//$snInciURL = "https://vt4helpdev.service-now.com/api/now/table/incident?sysparm_action=insert";
			//$snInciURL = "https://vt4helpdev.service-now.com/incident.do?JSONv2&sysparm_action=insert"; // OLD URL

			//print "<br/>DEBUG: URL is: ".$snInciURLInsert."<br/>";

			$snIncidentInsert = snAPIQuery($snInciURLInsert, $snUser, $snPass, $jsonDataInsEnc, "insert");

			$snInsertJSONResponse = json_decode($snIncidentInsert);

			//print "DEBUG: snInsertJSONResponse is: <br/>";
			//var_dump($snInsertJSONResponse);
			//print "<br/>";

			if (isset($snInsertJSONResponse->result->sys_id)) { 
				$snIncidentSysID = $snInsertJSONResponse->result->sys_id;
				$snIncidentNumber = $snInsertJSONResponse->result->number;

				if ($mode == "DEV") { $modeURL = "dev"; } else { $modeURL = ""; }

				print $snIncidentNumber."|DATA|<br/>Incident <a href='https://vt4help".$modeURL.".service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=".$snIncidentNumber."'>".$snIncidentNumber."</a> created in ".$mode." instance of Service-Now.<br/>";

				if ($callerNotFound == 1) {
					print "Caller was NOT found in Service-Now, so the Caller value was set to <strong>".$casUser."</strong>.<br/>";
					print "Please <a href='https://vt4help".$modeURL.".service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=".$snIncidentNumber."'>open this Incident</a> in Service-Now for review.<br/>";
				}

				if ($missingEmailList != "") {
					$missingEmailListFormat = str_replace(",", ", ", $missingEmailList);

					print "The following Email Contact(s) were NOT found in Service-Now and were removed: <strong>".$missingEmailListFormat."</strong>.<br/>";
					print "Please <a href='https://vt4help".$modeURL.".service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=".$snIncidentNumber."'>open this Incident</a> in Service-Now for review.<br/>";				
				}

				// Log the action

				/* MALWARE INFO DB LOOKUP */ 

				include("./db_info.inc.php");
				$dbName = "feint";

				try {
					$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
					unset($dbUser);
					unset($dbPass);
					$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo, logIncMalware) VALUES (:logUser, :logType, :logDataID, :logInfo, :logIncMalware)");
					$statement->execute(array("logUser" => $casUser, "logType" => $logMode."createIncident", "logDataID" => $alertID, "logInfo" => $snIncidentNumber, "logIncMalware" => $malwareName));
				}
				catch(PDOException $e) { print "Error: ".$e->getMessage(); }

				// clear the connection
				$connection = null;
			}
			else {
				print "<br/>Error creating incident.<br/>";
			}

			//print "<br/>DEBUG: sys_id is: ";
			//print $snInsertJSONResponse->result->sys_id."<br/>";

		/////////////////////////////////////////////////////////////
		//
		//	Update Incident
		//
		/////////////////////////////////////////////////////////////

			// Insert a delay into the execution so that SN doesn't reject 2 quick work_notes updates as spam...
			sleep(2);

			if ($mode == "DEV") { $snInciURLUpdate = "https://vt4helpdev.service-now.com/api/now/table/incident/".$snIncidentSysID; }
			else { $snInciURLUpdate = "https://vt4help.service-now.com/api/now/table/incident/".$snIncidentSysID; }

			if ($incidentNotes != "") { $jsonDataUpd["work_notes"] = "Notes from Incident Creator:\n\n".$incidentNotes."\n\n"; }

			if (isset($jsonDataUpd["work_notes"]) && $jsonDataUpd["work_notes"] != "") { $jsonDataUpd["work_notes"] .= $feAdditionalInfo; }
			else { $jsonDataUpd["work_notes"] = $feAdditionalInfo; }

			$jsonDataIns["sys_updated_by"] = $casUser;


			//print "<br/>DEBUG: URL is: ".$snInciURLUpdate."<br/>";

			$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

			//print "<br/>DEBUG: ENC jsonDataUpdEnc is: <br/>";
			//print $jsonDataUpdEnc;

			$snIncidentUpdate = snAPIQuery($snInciURLUpdate, $snUser, $snPass, $jsonDataUpdEnc, "update");

			$snUpdateJSONResponse = json_decode($snIncidentUpdate);
			//print "<br/>DEBUG: Update response JSON is: <br/>".$snIncidentUpdate."<br/>";

			if (isset($snUpdateJSONResponse->result->number)) {
				$snIncidentUpdateNumber = $snIncidentNumberUpdate = $snUpdateJSONResponse->result->number;
				print "<br/>Incident ".$snIncidentUpdateNumber." updated with Additional Information.<br/>";

			}
			else {
				print "<br/>Error updating incident.<br/>";
			}

			unset($snUser);
			unset($snPass);

		} // end insert/update inc isset

	} // end malwareName isset
	
} // end formAction isset

// EOF

?>