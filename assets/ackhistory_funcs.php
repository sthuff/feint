<?php 

/****************************************************************************

	FEINT ACK History Functions
	
	This makes up the HTML form that appears in the Show ACK History
	jQuery Dialog box.

****************************************************************************/

if (isset($_POST["feSourceIP"])) {

	// Trim and sanitize posted variables...
    $feSourceIP = trim($_POST["feSourceIP"]);
	$feSourceIP = filter_var($feSourceIP, FILTER_SANITIZE_STRING);

	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
	
	// force DEV mode for the Incident URL if we're in the dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/feintdev")) { $modeHistURL = "dev"; } else { $modeHistURL = ""; }

		// Check for Acknowledgements

		include("db_info.inc.php");
		$dbName = "feint";
		
		$showWarning = 0;

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP FROM logs WHERE logSrcIP = :logSrcIP AND logType = "ackAlertSuccess" ORDER BY logDate ASC');
			$statement->execute(array('logSrcIP' => $feSourceIP));

			if ($statement->rowCount() > 0) {
				
				$lcount = 0;
				
				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($rows as $log) { 
					$ackList[$lcount]["logID"] = $log["logID"];
					$ackList[$lcount]["logUser"] = $log["logUser"];
					$ackList[$lcount]["logDate"] = $log["logDate"];
					$ackList[$lcount]["logType"] = $log["logType"];
					$ackList[$lcount]["logDataID"] = $log["logDataID"];
					$ackList[$lcount]["logInfo"] = $log["logInfo"];
					$ackList[$lcount]["logSrcIP"] = $log["logSrcIP"];
					
					$lcount++;
				} // end foreach
								
			} // endif ackalerts exist
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;
?>
		<p>The history below <em>only</em> shows alerts that were acknowledged through FEINT.  Direct Alert Acknowledgements using the FireEye Web Interface will not appear here.<br/><br/></p>
<?php
		if (isset($ackList) && count($ackList) > 0) {
			
			include_once("servicenow_funcs.php");
			include("sn_info.inc.php");

?>
				<p>
					<strong>Add FE Alert to Incident</strong>: Clicking the <em>Update INC#######</em> button will add the current FireEye Alert details to the specified Incident in Service-Now.  
					This is preferable to opening a new Incident if the IP/device and Caller are the same for multiple FireEye alerts.<br/><br/>
					
					Links in the FE Alert ID column will open the alert directly in FireEye.  Links in the SN Inicdent column will open the Incident directly in Service-Now.  
					You must be logged into and have access to each service for these links to function. <br/><br/>
				</p>

				<table id="ackHistoryTable" class="display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<td><strong>Date</strong></td>
					<td><strong>FE Alert ID</strong></td>
					<td><strong>SN Incident</strong></td>
					<td><strong>Submitted By</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Add Alert to INC</strong></td>
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td><strong>Date</strong></td>
					<td><strong>FE Alert ID</strong></td>
					<td><strong>SN Incident</strong></td>
					<td><strong>Submitted By</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Add Alert to INC</strong></td>
				</tr>
				</tfoot>
				<tbody>
<?php
			foreach ($ackList as $ack) {
?>
				<tr>
					<td>
						<p><?php print $ack["logDate"]; ?></p>
					</td>
					<td>
						<a href="https://fireeye.iso.vt.edu/event_stream/events_for_bot?ev_id=<?php print $ack["logDataID"]; ?>" target="_blank" style="color: #fe5b00;"><?php print $ack["logDataID"]; ?></a>
					</td>
					<td>
						<a href="https://vt4help<?php print $modeHistURL; ?>.service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=<?php print $ack["logInfo"]; ?>" target="_blank" style="color: #fe5b00;"><?php print $ack["logInfo"]; ?></a>
					</td>
					<td>
						<p><?php print $ack["logUser"]; ?></p>
					</td>
					<td>
<?php
						if ($mode == "DEV") { $snInciURLGet = "https://vt4helpdev.service-now.com/api/now/table/incident?number=".$ack["logInfo"]; }
						else { $snInciURLGet = "https://vt4help.service-now.com/api/now/table/incident?number=".$ack["logInfo"]; }

						$snGetIncident = snIncident($snInciURLGet, $snUser, $snPass, $ack["logInfo"], "get");

						//print "<br/>DEBUG: snGetIncident is: ".$snGetIncident."<br/>";

						// if we get an empty result...
						if ($snGetIncident == "{\"result\":[]}") { 

							print "<br/>Status Unknown<br/>"; 
							$incidentNotFound = 1;
							exit;
						}

						$snGetIncidentJSONResponse = json_decode($snGetIncident);

						$snIncidentSysID = $snGetIncidentJSONResponse->result[0]->sys_id;
						$snIncidentState = $snGetIncidentJSONResponse->result[0]->incident_state;
																
						//print "<br/>DEBUG: SN INC SysID is: ".$snIncidentSysID."<br/>";
						//print "<br/>DEBUG: SN INC STATE is: ".$snIncidentState."<br/>";
						
						//var_dump($snGetIncidentJSONResponse);
										
						if ($snIncidentState == "6" || $snIncidentState == "7") { print "Closed"; } else { print "Open"; }										
?>
					</td>
					<td>
<?php
						if ($snIncidentState == "6" || $snIncidentState == "7") {
?>
							N/A
<?php
						} else {
?>
							<button id="updateINC-<?php print $ack["logInfo"]; ?>" class="updateInc" value="<?php print $ack["logInfo"]; ?>">Update <?php print $ack["logInfo"]; ?></button> <br/>
<?php 
							if ($userPermissions == "dbreadonly") {			
?>
							<span class="commNoteRadio"><label><input type="radio" id="snCommentAck-<?php print $ack["logInfo"]; ?>" name="snCommOrNote-<?php print $ack["logInfo"]; ?>" value="comment" checked="checked"> 
								<span style="font-size: 12px;">Comments</span></label></span>
							<span class="commNoteRadio"><label><input type="radio" id="snWorknoteAck-<?php print $ack["logInfo"]; ?>" name="snCommOrNote-<?php print $ack["logInfo"]; ?>" value="worknote"> 
								<span style="font-size: 12px;">Work Notes</span></label></span>
<?php
						 	} else {
?>
							<label><input type="radio" id="snCommentAck-<?php print $ack["logInfo"]; ?>" name="snCommOrNote-<?php print $ack["logInfo"]; ?>" value="comments"> 
								<span style="font-size: 12px;">Comments</span></label>
							<label><input type="radio" id="snWorknoteAck-<?php print $ack["logInfo"]; ?>" name="snCommOrNote-<?php print $ack["logInfo"]; ?>" value="worknotes" checked="checked"> 
								<span style="font-size: 12px;">Work Notes</span></label>
<?php
							}
?>
						<script>
							$("#openINCWarning").html($("#openINCWarning").html()+"<br/><span class='red'>Warning!</span> <a href='https://vt4help<?php print $modeHistURL; ?>.service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=<?php print $ack["logInfo"]; ?>' target='_blank'><?php print $ack["logInfo"]; ?></a> refers to <?php print $feSourceIP; ?> and is <strong>unresolved</strong>! <a class='openACKHist'>Open History</a><br/>");
							$("#openINCWarning").show();
						</script>
<?php
						}
?>
					</td>
				</tr>
<?php
			} // end foreach

			unset($snUser);
			unset($snPass);
?>
				</tbody>
				</table>

			<p><br/>View All Alerts for <a href="https://fireeye.iso.vt.edu/event_stream/events_for_bot?src_ip=<?php print $feSourceIP; ?>" target="_blank" style="color: #fe5b00;"><?php print $feSourceIP; ?></a> in FireEye<br/></p>

<?php
		} else {
?>
		<p>No acknowledgement history found for <?php print $feSourceIP; ?><br/><br/></p>			
		<p>View All Alerts for <a href="https://fireeye.iso.vt.edu/event_stream/events_for_bot?src_ip=<?php print $feSourceIP; ?>" target="_blank" style="color: #fe5b00;"><?php print $feSourceIP; ?></a> in FireEye</p>
<?php
		}
} // end feSourceIP set
?>

<?php

// EOF

?>