<?php

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	cms_funcs_inc.php
	
	Included by index.php
	
	This script holds functions called from index.php and also accepts 
	POST data during the Service-Now incident submission process to perform
	an Acknowledgement of the Alert from FireEye once the Incident number
	is known.
	
	If a POST["alertID"] variable exists, the associated POST data is 
	formatted and prepared for submission to the FireEye API to faciliate an
	Alert Acknowledgement.
	
****************************************************************************/

$mode = "LIVE";

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

// force DEV mode if we're on a dev instance...
if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/feintdev")) { $mode = "DEV"; }

class curlWithHeaders {

	public $headers; 
	
	public function execCURL($opts) {
        $this->headers = array();
        $opts[CURLOPT_HEADERFUNCTION] = array($this, '_processHeader');
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        return curl_exec($ch);
    }

    private function _processHeader($ch, $header) {
        $this->headers[] = $header;
        return strlen($header);
    }  
}

// Once we have an API Token from cmsAuthorize (below) and the alert ID
// from the FireEye web interface, we can perform an API query to pull 
// that data into FEINT.
function cmsGetAlertWithID($apiToken, $infoLevel, $alertID) {
	
	// DEBUG
	// print "Getting Alert with id: ".$alertID." and token ".$apiToken." at level ".$infoLevel."<br/>";
	
	$alertURL = "https://fireeye.iso.vt.edu/wsapis/v1.2.0/alerts/alert/".$alertID."?info_level=".$infoLevel;	

	// CURL is used to interact with the API.  The X-FeApi-Token header must be set to facilitate 
	// communication with the FE API.  This token is obtained from the cmsAuthorize function below.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $alertURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FeApi-Token: '.$apiToken,'Accept: application/xml; charset=utf-8'));
	
	$alertResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "Unable to connect to the Cadbury CMS.  Most likely, the API token expired.  Please refresh this page to renew the API token";
		print "Error info: ".curl_error($ch);
		exit("Halting execution since the CMS could not be reached.");
	}

	curl_close($ch);

	//print "<br/>DEBUG: RAW Response is: <br/>";
	//var_dump($alertResponse);
	//print "<br/>";
	
	// filter problematic chars	
	$alertResponse = utf8_for_xml($alertResponse);

	$alertXML = simplexml_load_string($alertResponse);
	//print "DEBUG AlertXML is: ".$alertXML."<br/>";
	//print_r($alertXML);
	$alertJSON = json_encode($alertXML);
	
	//print "<br/>DEBUG: JSON Response is: <br/>";
	//var_dump($alertJSON);
	
	return $alertJSON;
}

// Not currently used...
function cmsGetReportWithID($apiToken, $reportType, $alertID) {

	// DEBUG
	// print "Getting Alert with id: ".$alertID." and token ".$apiToken." at level ".$infoLevel."<br/>";
	
	$alertURL = "https://fireeye.iso.vt.edu/wsapis/v1.2.0/reports/report?report_type=".$reportType."&id=".$alertID;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $alertURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FeApi-Token: '.$apiToken,'Accept: application/pdf'));
	
	$alertResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "Unable to connect to the Cadbury CMS.  Most likely, the API token expired.  Please refresh this page to renew the API token";
		print "Error info: ".curl_error($ch);
		exit("Halting execution since the CMS could not be reached.");
	}

	curl_close($ch);

	return $alertResponse;
}

function cmsGetAlertsWithinTimeframe($apiToken, $infoLevel, $alertDuration) {
	
	#$alertURL = "https://cadbury.mgt.iso.vt.edu/wsapis/v1.1.0/alerts?info_level=".$infoLevel."&duration=".$alertDuration;	
	$alertURL = "https://fireeye.iso.vt.edu/wsapis/v1.2.0/alerts?duration=".$alertDuration;	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $alertURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FeApi-Token: '.$apiToken,'Accept: application/json; charset=utf-8'));
	
	$alertResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "Unable to connect to the Cadbury CMS.  Most likely, the API token expired.  Please refresh this page to renew the API token";
		print "Error info: ".curl_error($ch);
		exit("Halting execution since the CMS could not be reached.");
	}
	
	curl_close($ch);

	// ensure only a string is returned/parsed
	
	//$alertResponse = trim($alertResponse);
	//$alertResponse = filter_var($alertResponse, FILTER_SANITIZE_STRING);
	
	//print "<br/>DEBUG: RAW Response is: <br/>";
	//var_dump($alertResponse);
	//print "<br/>DEBUG: RAW Response END <br/>";

	// filter problematic chars	
	$alertResponse = utf8_for_xml($alertResponse);

	
/*
	libxml_use_internal_errors(true);
	$sxe = simplexml_load_string($alertResponse);
	
	if ($sxe === false) {
	    echo "<strong>Failed loading XML</strong><br/>\n";
		
	    foreach(libxml_get_errors() as $error) {
	        echo " - ".$error->message."<br/>\n";
	    }
	}
*/
	
	$alertXML = simplexml_load_string($alertResponse);
	$alertJSON = json_encode($alertXML);
	
	//libxml_clear_errors();
	
	//print "<br/>DEBUG: JSON Response is: <br/>";
	//var_dump($alertJSON);
	
	return $alertJSON;
}

function cmsConvertTimeToEST($myDateTime) {

		$estTimezone = new DateTimeZone('America/New_York');
		$offset = $estTimezone->getOffset($myDateTime);
		$myInterval=DateInterval::createFromDateString((string)$offset.'seconds');
		$myDateTime->add($myInterval);
		$myESTDateTime = $myDateTime->setTimeZone($estTimezone);	
	
		$timeEST = $myESTDateTime->format('Y-m-d H:i:s T');
	
		return $timeEST;
}

// Given the user/pass, we must authorize with the FireEye API so that we 
// can obtain a token that will be used to validate further communication 
// with the API.
function cmsAuthorize($user,$pass) {

	$authURL = "https://fireeye.iso.vt.edu/wsapis/v1.2.0/auth/login";
	
	$token = new curlWithHeaders();
	$authOpts = array(
		CURLOPT_URL				=> $authURL,	 	// URL
		CURLOPT_USERPWD			=> $user.":".$pass,	// credentials
		CURLOPT_RETURNTRANSFER	=> true,         	// return web page
		//CURLOPT_HEADER		=> false,	     	// don't return headers
		CURLOPT_FOLLOWLOCATION	=> true,         	// follow redirects
		//CURLOPT_ENCODING		=> "",           	// handle all encodings
		CURLOPT_CONNECTTIMEOUT	=> 120,          	// timeout on connect
		CURLOPT_TIMEOUT			=> 120,          	// timeout on response
		CURLOPT_MAXREDIRS		=> 10,           	// stop after 10 redirects
		CURLOPT_POST			=> 1,            	// data is post
		//CURLOPT_POSTFIELDS		=> $curl_data,  // post vars
		CURLINFO_HEADER_OUT		=> true,
		//CURLOPT_SSL_VERIFYHOS	=> 0,            	// don't verify ssl
		CURLOPT_SSL_VERIFYPEER	=> false,        	//
		CURLOPT_VERBOSE			=> 1                //   
	);

	$token->execCURL($authOpts);
	//print "User is: ".$user."<br/>";
	//print "URL is: ".$authURL."<br/>";
	//print "<br/>DEBUG: Headers are: <br/>";
	//print_r($token->headers); 
	//print "<br/>DEBUG: END HEADERS.<br/>";

	//print "<br/>DEBUG: FULL Response is are: <br/>";
	//print_r($token); 
	//print "<br/>DEBUG: END RESPONSE.<br/>";

	// Several headers are returned.  We need to find the X-FeApi-Token header,
	// extract it, and strip it down to just its value.
	foreach ($token->headers as $tokHeader) {
		
		if (strstr($tokHeader,"X-FeApi-Token: ")) { 
			$apiToken = str_replace("X-FeApi-Token: ", "", $tokHeader); 
		} 
	}
	
	// assuming we were successful, return the token
	if ($apiToken) { 
		return $apiToken; 
	}
	else { 
		// otherwise, provide error feedback
		print "Malformed header or token not returned by the API. Cannot continue. <br/>"; 
		exit;
	} 
}

/****************************************************************************

	After an Incident is created in Service-Now, an acknowledgement of the 
	FireEye alert is made so that it no longer populates the list of 
	outstanding alerts.  The acknowledgement created with this function 
	includes both the Service-Now Incident number and the VA Tech PID of the 
	user who submitted the Incident.
	
	The $jsonData parameter should be a JSON-encoded string.  
	
	Note that Acknowledgements can ONLY BE SUBMITTED ONCE.  Subsequent
	attempts will result in an error.

****************************************************************************/

function cmsAckAlertWithID($apiToken, $alertID, $casUser, $incidentID, $feSourceIP, $mode, $jsonData) {

	$ackURL = "https://fireeye.iso.vt.edu/wsapis/v1.2.0/alerts/alert/".$alertID;	

	$postData = $jsonData;

	//print "DEBUG in func: AlertID: ".$alertID.", feSourceIP: ".$feSourceIP."<br/>"; // json: ".$postData."<br/>";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $ackURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FeApi-Token: '.$apiToken,'Accept: application/json','Content-Type:application/json'));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_POST, true); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	
	$ackResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "Unable to connect to the Cadbury CMS.  Most likely, the API token expired.  Please refresh this page to renew the API token";
		print "Error info: ".curl_error($ch);
		exit("Halting execution since the CMS could not be reached.");
	}

	curl_close($ch);

	//print "<br/>DEBUG: RAW Response is: <br/>";
	//var_dump($ackResponse);
	//print "<br/>";
	
	$ackResponseObj = json_decode($ackResponse);
	
	//print "<br/>DEBUG: JSON Object Response is: <br/>";
	//var_dump($ackResponseObj);
	//print "<br/>";
	
	if ($mode == "DEVACK") { $instance = "DEV"; } else { $instance = ""; }
	
	if (isset($ackResponseObj->{"fireeyeapis"}->httpStatus)) {
			print "<br/><strong>".$ackResponseObj->{"fireeyeapis"}->httpStatus."</strong> FireEye Alert Acknowledgement: Failed - Alert already updated or not found.";
	
		// Log the action

		/* MALWARE INFO DB LOOKUP */ 

		include("./db_info.inc.php");
		$dbName = "feint";

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);
			$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo, logSrcIP) VALUES (:logUser, :logType, :logDataID, :logInfo, :logSrcIP)");
			$statement->execute(array("logUser" => $casUser, "logType" => "ackAlertFailed", "logDataID" => $alertID, "logInfo" => $instance.$incidentID, "logSrcIP" => $feSourceIP));
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;

	} 
	else {
		$jsonObj = json_decode($jsonData);
		print "<br/>FireEye Alert Acknowledged: ".$jsonObj->annotation;
		
		// Log the action

		/* MALWARE INFO DB LOOKUP */ 

		include("./db_info.inc.php");
		$dbName = "feint";

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);
			$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo, logSrcIP) VALUES (:logUser, :logType, :logDataID, :logInfo, :logSrcIP)");
			$statement->execute(array("logUser" => $casUser, "logType" => "ackAlertSuccess", "logDataID" => $alertID, "logInfo" => $incidentID, "logSrcIP" => $feSourceIP));
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;

	}
	
	return $ackResponseObj;
	
}

function utf8_for_xml($string) {
    return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}

//////////////////////////////////////////////////////////////////////////////////////////////

// If $_POST["alertID"] is set and we're not in DEV mode, we're submitting an alert acknowledgement...
if (isset($_POST["alertID"])) {

	// DEBUG force alert acknolwedgements enabled in DEV mode...
	//$mode = "DEVACK";
	
	if ($mode == "LIVE" || $mode == "DEVACK") {
	
		// Trim and sanitize posted variables...
		$ackalertID = trim($_POST["alertID"]);
		$ackalertID = filter_var($ackalertID, FILTER_SANITIZE_NUMBER_INT);

		$ackapiToken = trim($_POST["apiToken"]);
		$ackapiToken = filter_var($ackapiToken, FILTER_SANITIZE_STRING);

		$ackfeName = trim($_POST["feName"]);
		$ackfeName = filter_var($ackfeName, FILTER_SANITIZE_STRING);

		$ackmalwareName = trim($_POST["malwareName"]);
		$ackmalwareName = filter_var($ackmalwareName, FILTER_SANITIZE_STRING);

		$ackuserPermissions = trim($_POST["userPermissions"]);
		$ackuserPermissions = filter_var($ackuserPermissions, FILTER_SANITIZE_STRING);

		$ackcasUser = trim($_POST["casUser"]);
		$ackcasUser = filter_var($ackcasUser, FILTER_SANITIZE_STRING);

		$ackincidentID = trim($_POST["incidentID"]);
		$ackincidentID = filter_var($ackincidentID, FILTER_SANITIZE_STRING);
		
		$ackfeSourceIP = trim($_POST["feSourceIP"]);
		$ackfeSourceIP = filter_var($ackfeSourceIP, FILTER_SANITIZE_STRING);

		// The ackfeName is in the format:  "malware-object", however, the FireEye 
		// API expects it to be in the following format:  "Malware Object".  
		// The following performs that conversion...
		$ackfeName = str_replace("-"," ",$ackfeName);
		$ackfeName = ucwords($ackfeName);

		//print "DEBUG ackapiToken: ".$ackapiToken."<br/>";
		//print "DEBUG AckalertID: ".$ackalertID."<br/>";
		//print "DEBUG AckfeName: ".$ackfeName."<br/>";
		//print "DEBUG AckincidentID: ".$ackincidentID."<br/>";

		$jsonDataAck["annotation"] = $ackincidentID." submitted by ".$ackcasUser;
		$jsonDataAck["alertType"] = $ackfeName;

		// FireEye expects JSON data...
		$jsonDataAckEnc = json_encode($jsonDataAck, JSON_FORCE_OBJECT);

		//print "DEBUG JSON DATA ENC: ".$jsonDataAckEnc."<br/>";

		cmsAckAlertWithID($ackapiToken, $ackalertID, $ackcasUser, $ackincidentID, $ackfeSourceIP, $mode, $jsonDataAckEnc);
		
	} // END LIVE/DEVACK IF
	else {
		print "<br/>Skipping FireEye Alert Acknowledgement in DEV mode.";	
	}

}  // END ISSET / LIVE MODE
	
?>