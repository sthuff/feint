<?php
/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	alertslist_funcs.php
	
	Retreives list of alerts within the passed timeframe/duration
	
****************************************************************************/

	// SERVER TIMEZONE REQUIRED FOR DATE FUNCTIONS
	date_default_timezone_set('UTC');
	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

$mode = "live";

if ($mode == "DEV") {
	$_POST["feAlertsDuration"] = "2_hours";
	$_POST["casUser"] = "DEVsthuff";
	$_POST["userPermissions"] = "dbreadwrite";
	print "test: ".$_POST["feAlertsDuration"];
}

function checkAcks($feSourceIP) {
	
		// Check for Acknowledgements

		include("db_info.inc.php");
		$dbName = "feint";
		
		$showWarning = 0;

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP FROM logs WHERE logSrcIP = :logSrcIP AND logType = "ackAlertSuccess" ORDER BY logDate ASC');
			$statement->execute(array('logSrcIP' => $feSourceIP));

			if ($statement->rowCount() > 0) {
				
				$lcount = 0;
				
				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($rows as $log) { 
					$ackList[$lcount]["logID"] = $log["logID"];
					$ackList[$lcount]["logUser"] = $log["logUser"];
					$ackList[$lcount]["logDate"] = $log["logDate"];
					$ackList[$lcount]["logType"] = $log["logType"];
					$ackList[$lcount]["logDataID"] = $log["logDataID"];
					$ackList[$lcount]["logInfo"] = $log["logInfo"];
					$ackList[$lcount]["logSrcIP"] = $log["logSrcIP"];
					$ackList[$lcount]["ackWarning"] = 0;
					
					// DEBUG
					//print "Ack list: |".$log["logSrcIP"]."|, FESourceIP: |".$feSourceIP."|<br/>";
					
					$dateOfAck = new DateTime($ackList[$lcount]["logDate"]);
					$yesterday = new DateTime('NOW', new DateTimeZone('America/New_York'));
					$yesterday->sub(new DateInterval('PT24H'));
					
					// DEBUG
					//print "Date of Ack: ".$dateOfAck->format('Y-m-d H:i:s')." ( ".$log["logDate"]." ), Yesterday: ".$yesterday->format('Y-m-d H:i:s').", SW: ".$showWarning."<br/>";
					//if (strtotime($dateOfAck->format('Y-m-d H:i:s')) > strtotime($yesterday->format('Y-m-d H:i:s'))) { print "<br/>show warning<br/>"; }
					
					// if the alert was acknowledged since yesterday, show a warning...
					if (strtotime($dateOfAck->format('Y-m-d H:i:s')) > strtotime($yesterday->format('Y-m-d H:i:s'))) { $ackList[$lcount]["ackWarning"] = 1; }
					
					$lcount++;
				} // end foreach
								
			} // endif ackalerts exist
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;

		if (isset($ackList)) {
			return $ackList;
		}
}

function checkExpectedIPs($feSourceIP) {
			
		// Check for IPs that are Expected to produce malware alerts, 
		// like CS machines involved in malware research

		include("db_info.inc.php");
		$dbName = "feint";
		
		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT expID, expectedIP, expDesc FROM expectedips WHERE expectedIP = :expectedIP');
			$statement->execute(array('expectedIP' => $feSourceIP));

			if ($statement->rowCount() > 0) {
				
				$ecount = 0;
								
				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
				
				//print "DEBUG: ExpectedIPs ROWS is: <br/>";
				//print_r($rows);
				//print "DEBUG END <br/>";
				
				foreach ($rows as $expectedIPItem) { 
					$expIPList[$ecount]["expID"] = $expectedIPItem["expID"];
					$expIPList[$ecount]["expectedIP"] = $expectedIPItem["expectedIP"];
					$expIPList[$ecount]["expDesc"] = $expectedIPItem["expDesc"];
					$ecount++;
				} // end foreach
								
			} // endif expectedIP exists
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;

	
		if (isset($expIPList)) {
			
			//print "DEBUG: Expected ID is: ";
			//print_r($expIPList);
			//print "DEBUG: Expected IP List END <br/>";

			return $expIPList;
		}
}

function makeExpectedIPsTable($alertsObj, $userPermissions, $apiToken) {
	
?>		
			<div id="alertsListFrame" style="width:1080px; border: 1px solid black; padding: 10px;">
				<div id="alertsListHeader" style="color: #fe5b00; cursor: pointer;">Collapse Expected IP List</div>
				<div id="alertsListContent">
				<br/><br/>
				<strong>Expected Malware Traffic IPs</strong><br/>
				<br/>

				<p>The following table displays IPs that are expected to generate malware traffic because they are used for malware research or similar activities.  In most cases, hosts that appear in this special list <strong><em>should not</em></strong> have Service-Now incidents submitted in response to the traffic they generate.<br/><br/><em>Acknowledging an alert will refresh the alerts listing</em>.<br/><br/></p>

				<table id="expectedListTable" class="display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>Timestamp</th>
					<th>Source IP<br/>(hover for info)</th>
					<th>Src<br/>Port<br/><br/></th>
					<th>Destination IP</th>
					<th>Dst<br/>Port</th>
					<th>Alert ID</th>
					<th>Quick<br/>Acknowledge</th>
					<th>Name/Type</th>
					<th>Malware Name</th>
				</tr>
				</thead>
				<tfoot>
				<tr>
					<th>Timestamp</th>
					<th>Source IP<br/>(hover for info)</th>
					<th>Src<br/>Port<br/><br/></th>
					<th>Destination IP</th>
					<th>Dst<br/>Port</th>
					<th>Alert ID</th>
					<th>Quick<br/>Acknowledge</th>
					<th>Name/Type</th>
					<th>Malware Name</th>
				</tr>
				</tfoot>
				<tbody>
<?php
			$expectedIPsExist = FALSE;
	
			foreach ($alertsObj as $alertItem) {
				
				$ackList = checkAcks($alertItem["feSourceIP"]);

				if (isset($ackList)) {
					foreach ($ackList as $ack) {
						if ($ack["ackWarning"] == 1) { $highlightRow = " class='green'"; $alertItem["showAckWarning"] = 1; } else { $highlightRow = ""; }
					}
				} else { $highlightRow = ""; }

				$expIPList = checkExpectedIPs($alertItem["feSourceIP"]);
						
				if (isset($expIPList)) {
					
					$expectedIPsExist = TRUE;
					
					foreach ($expIPList as $expIPItem) {
						if ($expIPItem["expectedIP"] == $alertItem["feSourceIP"]) {
?>
				<tr>
					<td<?php print $highlightRow; ?>><?php print $alertItem["feTimestamp"]; ?></td>
					<td<?php print $highlightRow; ?> title="<?php print gethostbyaddr($expIPItem["expectedIP"]).", ".$expIPItem["expDesc"]; ?>"><?php print $alertItem["feSourceIP"]; ?></td>
					<td<?php print $highlightRow; ?>><?php print $alertItem["feSourcePort"]; ?></td>
					<td<?php print $highlightRow; ?>><?php print $alertItem["feDestinationIP"]; ?></td>
					<td<?php print $highlightRow; ?>><?php print $alertItem["feDestinationPort"]; ?></td>
					<td><a href="./index.php?feAlertID=<?php print $alertItem["feID"]; ?>"><?php print $alertItem["feID"]; ?></a></td>
					<td><button id="quickAck<?php print $alertItem["feID"]; ?>" class="quickAck" value="<?php print $alertItem["feID"]."||".$alertItem["feName"]."||".$apiToken."||".$alertItem["feMalwareName"]."||".$alertItem["feSourceIP"]; ?>">
						Acknowledge</button></td>
					<td<?php print $highlightRow; ?>><?php print $alertItem["feName"]; ?></td>
					<td<?php print $highlightRow; ?>><?php print $alertItem["feMalwareName"]; ?></td>
				</tr>			
<?php				
						} // end IF expectedIP = feSourceIP
					} // end foreach expIPList
				} // end expIPList exists
			} // end foreach
?>
				</tbody>
				</table>
				</div>
			</div>
<?php
			// if there are no expected IPs, collapse the table
			if ($expectedIPsExist == FALSE) {
?>
				<script>
					$("#alertsListContent").slideToggle(500, function () {
						//change text of header based on visibility of content div
						$("#alertsListHeader").text(function () {
							return $("#alertsListContent").is(":visible") ? "Collapse Expected IP List" : "Expand Expected IP List";
						});
					});
				</script>
<?php
			}
}


function makeAlertsTable($alertsObj, $userPermissions) {
	
?>		
			<div id="alertsListFrame" style="width:1100px;">
<?php
			if ($userPermissions == "dbreadwrite") {
?>
			<p>
				<br/><br/>
				FireEye alerts listed below are <strong><em>not expected</em></strong> to generate malware traffic.  Any alerts found to be credible should be reported through <br/>Service-Now using the ITSO SOP.
				<br/><br/>
			</p>
<?php
			}
?>
			
			<table id="alertListTable" class="display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>Timestamp</th>
				<th>Source IP</th>
				<th>Src<br/>Port<br/><br/></th>
				<th>Destination IP</th>
				<th>Dst<br/>Port</th>
				<th>Alert ID</th>
				<th>Name/Type</th>
				<th>Malware Name</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th>Timestamp</th>
				<th>Source IP</th>
				<th>Src<br/>Port<br/><br/></th>
				<th>Destination IP</th>
				<th>Dst<br/>Port</th>
				<th>Alert ID</th>
				<th>Name/Type</th>
				<th>Malware Name</th>
			</tr>
			</tfoot>
			<tbody>

<?php
			foreach ($alertsObj as $alertItem) {
				if ($userPermissions == "dbreadonly") {
					if (strstr($alertItem["feSourceIP"],"128.173.") || strstr($alertItem["feSourceIP"],"198.82.") || strstr($alertItem["feSourceIP"],"172.26.")) {
						
						$ackList = checkAcks($alertItem["feSourceIP"]);

						if (isset($ackList)) {
							foreach ($ackList as $ack) {
								if ($ack["ackWarning"] == 1 && $alertItem["showAckWarning"] == 0) { $highlightRow = " class='green'"; $alertItem["showAckWarning"] = 1; } else { $highlightRow = ""; }
							}
						} else { $highlightRow = ""; }
						
						$expIPList = checkExpectedIPs($alertItem["feSourceIP"]);
						
						if (!isset($expIPList)) {
?>
			<tr>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feTimestamp"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feSourceIP"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feSourcePort"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feDestinationIP"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feDestinationPort"]; ?></td>
				<td><a href="./index.php?feAlertID=<?php print $alertItem["feID"]; ?>"><?php print $alertItem["feID"]; ?></a></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feName"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feMalwareName"]; ?></td>
			</tr>			
<?php					
						}
					}
				}
				else { // non-dbreadonly doesn't filter results by IP

					$ackList = checkAcks($alertItem["feSourceIP"]);
						
					if (isset($ackList)) {
						foreach ($ackList as $ack) {
							if ($ack["ackWarning"] == 1 && $alertItem["showAckWarning"] == 0) { $highlightRow = " class='green'"; $alertItem["showAckWarning"] = 1; } else { $highlightRow = ""; }
						}
					} else { $highlightRow = ""; }

					$expIPList = checkExpectedIPs($alertItem["feSourceIP"]);
						
					if (!isset($expIPList)) {
?>
			<tr>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feTimestamp"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feSourceIP"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feSourcePort"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feDestinationIP"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feDestinationPort"]; ?></td>
				<td><a href="./index.php?feAlertID=<?php print $alertItem["feID"]; ?>"><?php print $alertItem["feID"]; ?></a></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feName"]; ?></td>
				<td<?php print $highlightRow; ?>><?php print $alertItem["feMalwareName"]; ?></td>
			</tr>			
<?php
					}
				}
			} // end foreach
?>
			</tbody>
			</table>
			</div>
<?php
}

function createAlertsObj($justAlertsList) {

	$a = 0;

	foreach ($justAlertsList as $feAlertItem) {

		// make sure we have results...
		if (isset($feAlertItem->occurred)) {


			//////////////////////////////////////////////////////////////
			//
			// BEGIN TIMESTAMP MANIPULATION AND FORMATTING
			//
			//////////////////////////////////////////////////////////////

			$alertsObj[$a]["feTimeStampServer"] = trim($feAlertItem->occurred);
			$alertsObj[$a]["feTimeStampServer"] = filter_var($alertsObj[$a]["feTimeStampServer"], FILTER_SANITIZE_STRING);

			$feTimestampObj = new DateTime($alertsObj[$a]["feTimeStampServer"]);
			$estTZ = new DateTimeZone('America/New_York');	
			$offset = $estTZ->getOffset($feTimestampObj);

			$feTimestampObj->add(DateInterval::createFromDateString((string)$offset.'seconds'));
			$feTimestampObj->setTimeZone(new DateTimeZone('America/New_York'));
			$alertsObj[$a]["feTimestamp"] = $feTimestampObj->format('Y-m-d H:i:s T');

			//////////////////////////////////////////////////////////////
			//
			// END TIMESTAMP MANIPULATION AND FORMATTING
			//
			//////////////////////////////////////////////////////////////

			$alertsObj[$a]["feApplianceID"] = trim($feAlertItem->{"@attributes"}->{"appliance-id"});
			$alertsObj[$a]["feApplianceID"] = filter_var($alertsObj[$a]["feApplianceID"], FILTER_SANITIZE_STRING);

			$alertsObj[$a]["feID"] = trim($feAlertItem->{"@attributes"}->id);
			$alertsObj[$a]["feID"] = filter_var($alertsObj[$a]["feID"], FILTER_SANITIZE_STRING);

			$alertsObj[$a]["feRootInfection"] = trim($feAlertItem->{"@attributes"}->{"root-infection"});
			$alertsObj[$a]["feRootInfection"] = filter_var($alertsObj[$a]["feRootInfection"], FILTER_SANITIZE_STRING);

			$alertsObj[$a]["feName"] = trim($feAlertItem->{"@attributes"}->name);
			$alertsObj[$a]["feName"] = filter_var($alertsObj[$a]["feName"], FILTER_SANITIZE_STRING);

			$alertsObj[$a]["feVictimHost"] = gethostbyaddr($feAlertItem->src->ip);

			$alertsObj[$a]["feSourceIP"] = trim($feAlertItem->src->ip);
			$alertsObj[$a]["feSourceIP"] = filter_var($alertsObj[$a]["feSourceIP"], FILTER_SANITIZE_STRING);

			$alertsObj[$a]["feSourcePort"] = trim($feAlertItem->src->port);
			$alertsObj[$a]["feSourcePort"] = filter_var($alertsObj[$a]["feSourcePort"], FILTER_SANITIZE_STRING);

			//print "<br/>DEBUG: Alert Explanation Malware Detected is: <br/>";
			//var_dump($feAlertItem->{"explanation"}->{"malware-detected"});
			//print "<br/>";

			if (isset($feAlertItem->{"explanation"}->{"malware-detected"}->{"malware"}->{"@attributes"}->{"name"})) {
				$alertsObj[$a]["feMalwareName"] = trim($feAlertItem->{"explanation"}->{"malware-detected"}->{"malware"}->{"@attributes"}->{"name"});
			} else {
				$alertsObj[$a]["feMalwareName"] = trim($feAlertItem->{"explanation"}->{"malware-detected"}->{"malware"}[0]->{"@attributes"}->{"name"});
			}		
			$alertsObj[$a]["feMalwareName"] = filter_var($alertsObj[$a]["feMalwareName"], FILTER_SANITIZE_STRING);

			$alertsObj[$a]["showAckWarning"] = 0;

			if (isset($feAlertItem->dst->ip)) {
				$alertsObj[$a]["feDestinationIP"] = trim($feAlertItem->dst->ip);
				$alertsObj[$a]["feDestinationIP"] = filter_var($alertsObj[$a]["feDestinationIP"], FILTER_SANITIZE_STRING);
			}
			else { $alertsObj[$a]["feDestinationIP"] = "NONE"; }

			if (isset($feAlertItem->dst->port)) {
				$alertsObj[$a]["feDestinationPort"] = trim($feAlertItem->dst->port);
				$alertsObj[$a]["feDestinationPort"] = filter_var($alertsObj[$a]["feDestinationPort"], FILTER_SANITIZE_STRING);
			}
			else { $alertsObj[$a]["feDestinationPort"] = "NONE"; }
			
			$a++;

		} // end if results exist
		else {
			print "Results were malformed or didn't exist.  Try refreshing or changing the time frame.";
			exit;
		}				

	} // end foreach

	return $alertsObj;
}

if (isset($_POST["feAlertsDuration"])) { // we have a duration and something to do...

	// Trim and sanitize posted variables...
	$feAlertsDuration = trim($_POST["feAlertsDuration"]);
	$feAlertsDuration = filter_var($feAlertsDuration, FILTER_SANITIZE_STRING);
	
    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);

	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
			
	include("./cms_info.inc.php");
	include_once("./cms_funcs.inc.php");

	// DEBUG: checking info access
	//print "cmsUser is: ".$cmsUser."<br/>";
	$apiToken = cmsAuthorize($cmsUser,$cmsPass);
	unset($cmsUser);
	unset($cmsPass);

	// A list of alerts within a duration
	$apiAlertsListJSON = cmsGetAlertsWithinTimeframe($apiToken, "concise", $feAlertsDuration);

	// Decode the JSON object into a stdClass
	$apiAlertsList = json_decode($apiAlertsListJSON);


//		print "DEBUG Alerts List in ".$feAlertsDuration.": <br/>";
//		print_r($apiAlertsList);
//		print "<br/>";

	if (isset($apiAlertsList->alert)) {
		// strips the non-alert content from the alertsList object
		$justAlerts = $apiAlertsList->alert;
	} else {
		print "Alert data not found for this time range."; 
		exit; 
	}
	
	$sizeAlerts = count($justAlerts);

	if ($sizeAlerts > 0) {
		//print "<br/>count is: ".count($justAlerts)."<br/>";
		
		// if only 1 alert...
		if ($sizeAlerts == 1) {
			$justAlertsList[0] = $justAlerts;
		} else { // more than 1 alert
			$justAlertsList = $justAlerts;
		} 
		
		$alertsObj = createAlertsObj($justAlertsList);

		// DEBUG
		//print "AlertsObj START<br/>";
		//print_r($alertsObj);
		//print "AlertsObj END<br/>";

		if ($userPermissions == "dbreadwrite") { 
			makeExpectedIPsTable($alertsObj, $userPermissions, $apiToken);
		}
		makeAlertsTable($alertsObj, $userPermissions);
		
	} // alerts < 0 end
	else {
		// no alerts
		print "<br/>No alerts found for the specified duration.<br/>";
	}
} // no POST feAlertsDuration set
?>
