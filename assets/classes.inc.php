<?php 

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	classes.inc.php
	
	Included by index.php, update_malwaredb.php, url_tool.php
	
	The Malware class handles retreiving and setting properties of malware 
	objects that are queried from the ITSO Malware Database
	
****************************************************************************/

class Malware
{
    protected $id;
	protected $severity;
    protected $name;
	protected $description;

    public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; }

    public function getSev() { return $this->severity; }
    public function setSev($severity) { $this->severity = $severity; }

	public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; }

    public function getDesc() { return $this->description; }
    public function setDesc($description) { $this->description = $description; }
}

class Logs
{
    protected $id;
	protected $user;
    protected $datetime;
	protected $type;
	protected $dataID;
	protected $info;
	protected $srcIP;

    public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; }

    public function getUser() { return $this->user; }
    public function setUser($user) { $this->user = $user; }

	public function getDateTime() { return $this->datetime; }
    public function setDateTime($datetime) { $this->datetime = $datetime; }

    public function getType() { return $this->type; }
    public function setType($type) { $this->type = $type; }
	
	public function getDataID() { return $this->dataID; }
    public function setDataID($dataID) { $this->dataID = $date; }

	public function getInfo() { return $this->info; }
    public function setInfo($info) { $this->info = $info; }

	public function getSrcIP() { return $this->srcIP; }
    public function setSrcIP($info) { $this->srcIP = $srcIP; }
}


// class JSONObject is NOT CURRENTLY USED
class JSONObject {
    public function __construct($json = false) {
        if ($json) $this->set(json_decode($json, true));
    }

    public function set($data) {
        foreach ($data AS $key => $value) {
            if (is_array($value)) {
                $sub = new JSONObject;
                $sub->set($value);
                $value = $sub;
            }
            $this->{$key} = $value;
        }
    }
}

?>