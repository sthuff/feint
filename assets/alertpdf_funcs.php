<?php
	// NOT CURRENTLY USED...

	// Trim and sanitize posted variables...
    $alertID = trim($_POST["alertID"]);
	$alertID = filter_var($alertID, FILTER_SANITIZE_NUMBER_INT);

	$apiToken = trim($_POST["apiToken"]);
	$apiToken = filter_var($apiToken, FILTER_SANITIZE_STRING);

	$feName = trim($_POST["feName"]);
	$feName = filter_var($feName, FILTER_SANITIZE_STRING);

    $reportType = trim($_POST["reportType"]);
	$reportType = filter_var($reportType, FILTER_SANITIZE_STRING);



	//print "DEBUG AlertID: ".$alertID."<br/>";
	//print "reportType: ".$reportType."<br/>";

	$reportURL = "https://fireeye.iso.vt.edu/wsapis/v1.2.0/reports/report?report_type=alertDetailsReport&alert_id=".$alertID."&infection_type=".$feName."&infection_id=".$alertID;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $reportURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FeApi-Token: '.$apiToken,'Accept: application/pdf'));
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FeApi-Token: '.$apiToken));
	
	$reportResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "Unable to connect to the Cadbury CMS.  Most likely, the API token expired.  Please refresh this page to renew the API token";
		print "Error info: ".curl_error($ch);
		exit("Halting execution since the CMS could not be reached.");
	}

	curl_close($ch);

header("Content-type:application/pdf");
header("Content-Disposition:attachment;filename=fireeye_report_".$alertID.".pdf");
header('Content-Length: '.strlen($reportResponse));
print $reportResponse;
?>