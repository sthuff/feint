<?php 

/****************************************************************************

	FEINT Argus Form Include
	
	This makes up the HTML form that appears in the Generate Argus Command 
	jQuery Dialog box.

****************************************************************************/

?>
			<form id="argusForm">
				<table>
				<tr>
					<td>
						<input type="radio" name="argusIP" value="<?php print $feSourceIP; ?>" checked> Source IP: <?php print $feSourceIP; ?> <br/>
					</td>
					<td>
						<input type="radio" name="argusIP" value="<?php print $feDestinationIP; ?>"> Destination IP: <?php print $feDestinationIP; ?> <br/>
					</td>
				</tr>
				<tr>
					<td>Date From: 
						<input type="text" name="dateTimeFromArgus" id="dateTimeFromArgus" value="<?php print $feTimestampMinusHour; ?>" />
					</td>
					<td>Date To: 
						<input type="text" name="dateTimeToArgus" id="dateTimeToArgus" value="<?php print $feTimestampPlusHour; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="2">Argus Command: <br/>
						<textarea id="argusCmd" name="argusCmd" rows="4" cols="50"></textarea>
					</td>
				</tr>
				</table>
			</form>			
<?php

// EOF

?>