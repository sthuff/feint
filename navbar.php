<?php 

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	navbar.php
	
	Included by index.php, update_malwaredb.php, url_tool.php
	
	Provides a navigation menu to users based on their access level.
	For example, dbreadonly users do not get links to the Malware DB or
	the URL Tools.
	
****************************************************************************/

	if ($_SERVER["SERVER_NAME"] == "localhost") { $_SERVER["HTTP_CAS_UUPID"] = "localUser"; }

?>
					<p style="text-align:right; padding-bottom: 15px;"><?php print $_SERVER["HTTP_CAS_UUPID"]; ?> (<a href="logout.php">Logout</a>) 
						<br/>
					</p>
					<p style="text-align:right; font-size: 14px;">
<?php
					if ($userPermissions == "dbreadonly") { print "<br/><br/>"; }
?>
						<a href="./index.php"><span style="font-size: 14px;">Home</span></a> 
					<br/>
					<a style="font-size: 10px;" href="./url_tool.php"><span style="font-size: 14px;">CLS URL Tool</span></a>
<?php
		if ($userPermissions == "dbreadwrite") {
?>
					<br/>
					<a style="font-size: 10px;" href="./update_malwaredb.php"><span style="font-size: 14px;">Malware DB</span></a> 
					<br/>
					<a style="font-size: 10px;" href="./update_expectedips.php"><span style="font-size: 14px;">Expected IPs DB</span></a> 
					<br/>
					<a style="font-size: 10px;" href="./logs.php"><span style="font-size: 14px;">Logs</span></a>
<?php
		}			
?>
					</p>
<?php

// EOF

?>