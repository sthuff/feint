<?php 

/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	javascripts.inc.php
	
	Included by index.php, update_malwaredb.php, url_tool.php
	
	Includes Javascript and jQuery Includes. 
	
	The FEINT makes heavy use of jQuery and the jQueryUI throughout.  
	
	The "Generate LAA URL" and "Generate Argus Command" tools make use 
	of the copyme.js library to copy the URL textarea field to the 
	clipboard when the user clicks the Copy URL button.	

****************************************************************************/

?>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
	<link type="text/css" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<link type="text/css" rel="stylesheet" href="./css/jquery-ui-timepicker-addon.css">
	<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="./js/copyme.js"></script>

	<link rel="stylesheet" type="text/css" href="./DataTables/datatables.min.css"/>
	<script type="text/javascript" src="./DataTables/datatables.min.js"></script>
<?php
	// While the bulk of this file could be kept as a javascript file (.js), it is 
	// instead remains a PHP include so that most comments can be hidden from the 
	// the client's browser
?>
	<script>
	$(document).ready(function(){
	
	// OS CHECKS

	var OSName="unknown";
	if (navigator.appVersion.indexOf("Win")!=-1) { OSName = "win"; }
	if (navigator.appVersion.indexOf("Mac")!=-1) { OSName = "mac"; }
	if (navigator.appVersion.indexOf("X11")!=-1) { OSName = "unix"; }
	if (navigator.appVersion.indexOf("Linux")!=-1) { OSName = "linux"; }
		
	// BROWSER CHECKS

    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;

	var browserName = "unknown";
	if (isOpera) { browserName = "opera"; }
	if (isFirefox) { browserName = "firefox"; }
	if (isSafari) { browserName = "safari"; }
	if (isIE) { browserName = "ie"; }
	if (isEdge) { browserName = "edge"; }
	if (isChrome) { browserName = "chrome"; }
	if (isBlink) { browserName = "blink"; }
		
	// DEBUG
	//alert(OSName+", "+browserName);
		
	if (isFirefox && OSName == "win") {
		$('#feTimestamp').prop("size",31);
		$('#feVictimHost').prop("size",50);
		$('#malwareComms').prop("size",50);
		$('#malwareName').prop("size",50);
		$('#incidentComments').prop("cols",92);
		$('#incidentNotes').prop("cols",92);
		$('#malwareDesc').prop("cols",92);
	}
		
	if (isChrome && OSName == "linux") {
		$('#incidentShortDesc').width(743);
		$('#feTimestamp').prop("size",26);
		$('#feVictimHost').prop("size",41);
		$('#malwareComms').prop("size",41);
		$('#malwareName').prop("size",41);
		$('#incidentComments').prop("cols",92);
		$('#incidentNotes').prop("cols",92);
		$('#malwareDesc').prop("cols",92);
	}
		
	if (isFirefox && OSName == "unix") {
		$('#incidentShortDesc').width(743);
		$('#feTimestamp').prop("size",26);
		$('#feVictimHost').prop("size",41);
		$('#malwareComms').prop("size",41);
		$('#malwareName').prop("size",41);
		$('#incidentComments').prop("cols",105);
		$('#incidentNotes').prop("cols",105);
		$('#malwareDesc').prop("cols",105);
	}
		
	if (isIE) {
		$('#feTimestamp').prop("size",31);
		$('#feVictimHost').prop("size",50);
		$('#malwareComms').prop("size",50);7894473
		$('#malwareName').prop("size",50);
		$('#incidentComments').prop("cols",92);
		$('#incidentNotes').prop("cols",92);
		$('#malwareDesc').prop("cols",92);
	}

	if (isEdge) {
		$('#feTimestamp').prop("size",31);
		$('#feVictimHost').prop("size",50);
		$('#malwareComms').prop("size",50);
		$('#malwareName').prop("size",50);
		$('#incidentComments').prop("cols",100);
		$('#incidentNotes').prop("cols",100);
		$('#malwareDesc').prop("cols",100);
	}

<?php		
		// SET USER PERMISSIONS
?>			
		if ($('#userPermissions').val() == "dbreadonly") {
<?php
			// If the user is a non-ITSO staff member (dbreadonly access), hide the 
			// Update Malware button, set the malware description textarea field 
			// to readonly, and set the text inside it to a light gray to indicate
			// cannot be edited.
?>			
			$('#updateMalware').hide();
			$('#malwareDesc').prop('readonly', true);
			$('#malwareDesc').addClass("blurredDefaultText");
			$('#incidentNotes').addClass("blurredDefaultText");
			$('#incidentComments').addClass("blurredDefaultText");
		} else {
<?php
			// If the user is a ITSO staff member (dbreadwrite access), the
			// Update Malware button is visible and the malware description textarea field 
			// is only set to light gray if the description is empty.  In this case, 
			// a default blurb is presented requsting the field be completed. This
			// blurb is removed whenever the field gains keyboard or mouse focus.
?>
			$('#malwareDesc').blur(function () { inputBlur(this, $('#malwareDescTemp').val() ); });
			$('#malwareDesc').focus(function () { inputFocus(this, $('#malwareDescTemp').val() ); });
			if ($('#malwareDesc').val() == "") { $('#malwareDesc').val( $('#malwareDescTemp').val() ) };
			
			$('#incidentNotes').addClass("blurredDefaultText");
			if ($('#incidentNotes').val() == "") { $('#incidentNotes').val( $('#incidentNotesTemp').val() ) };
			$('#incidentNotes').focus(function () { inputFocus(this, $('#incidentNotesTemp').val() ); });
			$('#incidentNotes').blur(function () { inputBlur(this, $('#incidentNotesTemp').val() ); });
			
			$('#incidentComments').addClass("blurredDefaultText");
			if ($('#incidentComments').val() == "") { $('#incidentComments').val( $('#incidentCommentsTemp').val() ) };
			$('#incidentComments').focus(function () { inputFocus(this, $('#incidentCommentsTemp').val() ); });
			$('#incidentComments').blur(function () { inputBlur(this, $('#incidentCommentsTemp').val() ); });
			
			var feSourceIP = $("#feSourceIP").val();
			if (typeof feSourceIP !== 'undefined') {

				// if we're an ITSO staff member AND the source IP isn't 128.173, 198.82 or 172.26, default to NIS
				// as the assignment group...
				
				if (feSourceIP.indexOf("128.173.") == -1 && feSourceIP.indexOf("198.82.") == -1 && feSourceIP.indexOf("172.26.") == -1) {
					$("#assignmentGroup").val("nis");	
				} else {
					$("#assignmentGroup").val("itso");
				}
			}
		}		
<?php		
		// GET LIST OF ALERTS if no FE Alert selected...
?>		
		if ($('#feAlertsDuration').length > 0 ) {
			getAlertsList();        
    	} 
		else {
<?php		
		// GET Acknowledgement History if a FE Alert has been selected...
?>		
			getAckHistory();
		}
<?php		
		// SUBMIT TICKET TO SERVICE-NOW
?>
		$("#submitIncident").click(submitIncident);		
<?php
		// DOWNLOAD ALERT PDF (NOT USED)
?>
		//$("#downloadAlertPDF").click(downloadAlertPDF);
<?php
		// Reload the page with the selected duration if its pulldown is changed
?>
		$('#feAlertsDuration').on('change', function () {
			var duration = $(this).val(); // get selected value
			if (duration) { 
				getAlertsList();
			}
			return false;
		});
<?php
		// UPDATE MALWARE ITEM
?>
		$("#updateMalware").click(updateMalwareDB);
<?php
		// GENERATE ARGUS COMMAND
?>
		$("#genArgusButton").click(generateArgus);
<?php
		// GENERATE LAA URL
?>
		$("#genLAAButton").click(openLAADialog);
<?php
		// SHOW ACK HISTORY
?>
		$("#showAckHistoryButton").click(openACKHistDialog);
		
<?php
		// HANDLE LAA EVENTS
		
		// When any field that makes up the the "Generate LAA URL" tool is changed, 
		// the URL is re-generated to reflect the changes in real-time.
?>
		$("#urlDash").change(generateLAA);
		$('input:radio[name="urlIP"]').change(generateLAA);
		$("#dateTimeFromLAA").change(generateLAA);
		$("#dateTimeToLAA").change(generateLAA);
		$("#custQueryText").change(generateLAA);
<?php
		// NOTE - LAA expects timestamps in this format:
		// 2017-02-26T03:40:39.890Z

		// The timestamp formats that the LAA expects and the formats used by the jQueryUI date picker 
		// are not compatible.  Some conversion is done here.  We also perform some sanity checks
		// such that if an end date is picked that is earlier than a start date, the start date is forced 
		// to equal the end date.  Similarly, if an end date is chosen that is earlier than the start date, 
		// the the start date is forced to the end date to avoid errors.
?>		
		var startDateTextBox = $("#dateTimeFromLAA");
		var endDateTextBox = $("#dateTimeToLAA");
		startDateTextBox.datetimepicker({ dateFormat: "yy-mm-dd", timeInput: true, timeFormat: "HH:mm:ss z",
											onClose: function(dateText, inst) {
												if (endDateTextBox.val() != '') {
													var testStartDate = startDateTextBox.datetimepicker('getDate');
													var testEndDate = endDateTextBox.datetimepicker('getDate');
													if (testStartDate > testEndDate)
														endDateTextBox.datetimepicker('setDate', testStartDate);
												}
												else { endDateTextBox.val(dateText); }
											}
										 }); 
		
		endDateTextBox.datetimepicker({ dateFormat: "yy-mm-dd", timeInput: true, timeFormat: "HH:mm:ss z", 
											onClose: function(dateText, inst) {
												if (startDateTextBox.val() != '') {
													var testStartDate = startDateTextBox.datetimepicker('getDate');
													var testEndDate = endDateTextBox.datetimepicker('getDate');
													if (testStartDate > testEndDate)
														startDateTextBox.datetimepicker('setDate', testEndDate);
												}
												else { startDateTextBox.val(dateText); }
											}
										}); 

<?php
		// Copy LAA Text to clipboard using the copyme library
?>
		$('#copyLAAText').click(function(){ $('#laaURL').copyme(); });
		
<?php
		// AUTOCOMPLETE ON MALWARE NAME / DETECTED ISSUE FIELD
		// Invoke jQuery's autocomplete functionality.  Database queries are handled by autocomplete_malwarename.php.
		// See this file for further details.
?>
		$('#malwareName').autocomplete({ source:'autocomplete_malwarename.php', select: acSelected, focus: acSelected, minLength:2 });
		
<?php
		// SEARCH BUTTON ACTION IN MALWARE DB LIST TOOL
?>
		$('#malwareSearch').click(function(){
			window.location = 'update_malwaredb.php?mwID='+$("#mwID").val()+'&mwFunc=edit';
		}); 
				
		$('#incListTable').DataTable({
			"paging": false,
			order: [[1, 'dec'],[0,'dec']],
			rowGroup: { 
				startRender: function ( rows, group ) {
					return 'Malware: '+group +' ('+rows.count()+')';
				},
				dataSrc: 1
			}
		});
		
		if ($('#callerPID').length > 0 ) {
			verifyPID();
    	}

		$('#callerPID').on('blur', function() {
			verifyPID();
			return false;
		});
		
		if ($('#contactEmail').length > 0 ) {
			verifyEmail();
    	}

		$('#contactEmail').on('blur', function() {
			verifyEmail();
			return false;
		});
		
	});	// END DOCREADY

function getAlertsList() {

	$('#alertsListLoading').show();
	$('#alertsListContainer').hide();

	$.post('./assets/alertslist_funcs.php', {
						feAlertsDuration: $('#feAlertsDuration').val(), 
						casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {
<?php
		// alerts list returned
?>
		$('#alertsListLoading').hide();
		$('#alertsListContainer').html(data);
		$('#alertsListContainer').show();
		
		$('#alertListTable').DataTable({
			"paging": false,
			order: [[1, 'dec'],[0,'dec']],
			rowGroup: { 
				startRender: function ( rows, group ) {
					return 'Source: '+group +' ('+rows.count()+')';
				},
				dataSrc: 1
			}
		});
		
		$('#expectedListTable').DataTable({
			"paging": false,
			order: [[1, 'dec'],[0,'dec']],
			rowGroup: { 
				startRender: function ( rows, group ) {
					return 'Source: '+group +' ('+rows.count()+')';
				},
				dataSrc: 1
			}
		});

		$('.quickAck').click(function() {
			var quickAckData = $(this).val();
			quickAcknowledge(quickAckData);
		});
		
		$("#alertsListHeader").click(function () {
			$("#alertsListContent").slideToggle(500, function () {
				//change text of header based on visibility of content div
				$("#alertsListHeader").text(function () {
					return $("#alertsListContent").is(":visible") ? "Collapse Expected IP List" : "Expand Expected IP List";
				});
			});

		});
		
	}).fail(function() {
		$('#alertsListContainer').html("Error: Retreiving Alerts List failed.");
	});	
}
		
function getAckHistory() {

	$('#ackHistoryLoading').show();
	$('#ackHistoryContainer').hide();

	$.post('./assets/ackhistory_funcs.php', {
		feSourceIP: $('#feSourceIP').val(), userPermissions: $("#userPermissions").val()
	}, function(data) {
<?php
		// ackhistory returned
?>
		$('#ackHistoryLoading').hide();
		$('#ackHistoryContainer').html(data);
		
		$('#ackHistoryTable').DataTable({
			order: [0, 'dec'],
			"deferRender": true
		});
		
		$(".openACKHist").click(openACKHistDialog);

		$('.updateInc').click(function() {
			$("#showAckHistory").dialog('close');

			var incToUpdate = $(this).val();
			var snCommOrNoteRadio = "worknote";
			if ($("#snCommentAck-"+incToUpdate).is(":checked")) { snCommOrNoteRadio = "comments"; } else { snCommOrNoteRadio = "worknotes"; }
			
			if ($('#incidentNotes').val() == $('#incidentNotesTemp').val()) { $('#incidentNotes').val("") };
			if ($('#incidentComments').val() == $('#incidentCommentsTemp').val()) { $('#incidentComments').val("") };
			
			$.post('./assets/servicenow_funcs.php', { 
						feSourceIP: $('#feSourceIP').val(), feSensorSrc: $("#feSensorSrc").val(),
						feTimestamp: $("#feTimestamp").val(), feVictimHost: $("#feVictimHost").val(), 
						malwareComms: $("#malwareComms").val(), malwareName: $("#malwareName").val(), 
						malwareSeverity: $("#malwareSeverity").val(), incidentNotes: $("#incidentNotes").val(), 
						incidentComments: $("#incidentComments").val(), malwareDesc: $("#malwareDesc").val(),
						callerPID: $("#callerPID").val(), assignmentGroup: $("#assignmentGroup").val(),
						casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val(), alertID: $("#alertID").val(),
						feAdditionalInfo: $("#feAdditionalInfo").val(), snCommOrNote: snCommOrNoteRadio,
						incToUpdate: incToUpdate,
						formAction: "updateIncident"
			}, function(data) {
				var respData = data.split("|DATA|");
				$('#incidentID').val(respData[0]);
				$('#response').html(respData[1]);
				
				// DEBUG (show data before the split): 
				$('#response').html(data);
				
			}).fail(function() {
				$('#response').html("Posting to SN failed.");
			});
<?php
	// Hide the formFrame DIV that contains all of the index.php content...
?>
			$("#formFrame").hide();
			
<?php
	// and show all of the data collected as responses from various APIs...
?>
			$("#responseFrame").show();
	
			return false;
		});
		
		$('#ackHistoryContainer').show();
				
	}).fail(function() {
		$('#ackHistoryContainer').html("Error: Retreiving Acknowledgement History failed.");
	});	
}

function verifyPID() {
	
	$.post('./assets/servicenow_funcs.php', {
				callerPID: $("#callerPID").val(), 
				formAction: "verifyPID",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {

		var respData = data.split("|DATA|");
		
		// DEBUG
		//alert("resp: "+respData[0]+", "+respData[1]);
		
		if (respData[0] == 1) {
			$("#callerPID").val($("#casUser").val()); 
			$("#departmentName").html("Not Found"); 
			$("#departmentSysID").val(""); 
			$("#callerPIDSysID").val(""); 
			$("#casUserSysID").val(respData[4]);
			$('#contactEmail').val($("#casUser").val()+"@vt.edu");
			
			$('#feedback').html("The Caller PID value was not found.  Resetting PID to '<strong>"+$("#casUser").val()+"</strong>'");
			
			
		} else {
			$("#callerPID").val(respData[1]);
			$("#departmentName").html(respData[2]); 
			$("#departmentSysID").val(respData[3]); 
			$("#callerPIDSysID").val(respData[4]); 
			$("#casUserSysID").val(respData[5]);
			$('#contactEmail').val(respData[1]+"@vt.edu");	
		
			$('#feedback').html("Department for Caller set by Service-Now: <br/><strong>"+respData[2]+"</strong>"); //<br/>"+respData[3]);
			
			// BAMS IT == cdb565ec0f52a100ee5a0bcce1050ef5 SYS ID
			// BAMS IT == fae7b3830f436a00d3254b9ce1050ec9 ASSIGN GRUOP ID
			
			// CALS IT == a7a565ec0f52a100ee5a0bcce1050ea1 SYS ID
			// CALS IT == 62fd3c430f9aa600d3254b9ce1050e43 ASSIGN GROUP ID
			
			// GS IT == 50b565ec0f52a100ee5a0bcce1050ed8 SYS ID
			// GS IT == f4391b17dbf9cfc4e3a0f839af961906 ASSIGN GROUP ID
			
			// LIBDESKAV == 44b565ec0f52a100ee5a0bcce1050ec1 SYS ID
			// LIBDESKAV == 67229cca0f469b00b97f0bcce1050e40 ASSIGN GROUP ID // wrong
			// LIBDESKAV == 90eb64a1db414340e3a0f839af961958 ASSIGN GROUP ID
			
			// VETMED IT == d0b565ec0f52a100ee5a0bcce1050ed5 SYS ID
			// VETMED IT == c5abd3c8db5a8700d05152625b96192b ASSIGN GROUP ID
		
			if ($("#departmentSysID").val() == "cdb565ec0f52a100ee5a0bcce1050ef5") {
				showAutoAssignmentDialog("fae7b3830f436a00d3254b9ce1050ec9", respData[2], "bamsit");
				$('#feedback').html("Assignment Group and Contact Email <br/>auto-set according to ITSO SOP");
			} else if ($("#departmentSysID").val() == "a7a565ec0f52a100ee5a0bcce1050ea1") {
				showAutoAssignmentDialog("62fd3c430f9aa600d3254b9ce1050e43", respData[2], "calsit");
				$('#feedback').html("Assignment Group and Contact Email <br/>auto-set according to ITSO SOP");
			} else if ($("#departmentSysID").val() == "50b565ec0f52a100ee5a0bcce1050ed8") {
				showAutoAssignmentDialog("f4391b17dbf9cfc4e3a0f839af961906", respData[2], "gsit");
				$('#feedback').html("Assignment Group and Contact Email <br/>auto-set according to ITSO SOP");
			} else if ($("#departmentSysID").val() == "44b565ec0f52a100ee5a0bcce1050ec1") {
				showAutoAssignmentDialog("90eb64a1db414340e3a0f839af961958", respData[2], "libdeskav"); 
				$('#feedback').html("Assignment Group and Contact Email <br/>auto-set according to ITSO SOP");
			} else if ($("#departmentSysID").val() == "d0b565ec0f52a100ee5a0bcce1050ed5") {
				showAutoAssignmentDialog("c5abd3c8db5a8700d05152625b96192b", respData[2], "vetmedit");
				$('#feedback').html("Assignment Group and Contact Email <br/>auto-set according to ITSO SOP");
			}
		}

	}).fail(function() {
		//alert("failed");
		$('#feedback').html("API call to Service-Now failed.");
	});
	
}

function showAutoAssignmentDialog(assignGroupSysID, deptName, assignGroupOpt) {
	
	var assignGroupText = $("#assignmentGroup option[value='"+assignGroupOpt+"']").text();
	
	var autoAssignmentGroupHTML = "The caller for this Incident is from <strong>"+deptName+"</strong>.<br/><br/>"
								+"Per the ITSO Incident Reporting SOP, Incidents directed to this department should be assigned directly to "
								+"<strong>"+assignGroupText+"</strong> and use the following Contact Email(s): <br/><br/>";

	var autoAssignmentGroupEmails = "itso-soc-g@vt.edu";
	$("#autoAssignmentGroupContent").html(autoAssignmentGroupHTML+autoAssignmentGroupEmails);
	$("#autoAssignmentGroup").dialog({ title: "Auto-Assignment Group", modal: true, minWidth: 800, minHeight: 400, position: { my: "center", at: "center", of: window } });

	$("#autoAssignConfirm").click(function(){
		$('#contactEmail').val(autoAssignmentGroupEmails);
		$("#assignmentGroup").val(assignGroupOpt);
		$("#assignGroupSysID").val(assignGroupSysID);
		$("#autoAssignmentGroup").dialog('close');
	});

	$("#autoAssignCancel").click(function() {
		$("#autoAssignmentGroup").dialog('close');
	});
}

function verifyEmail() {
	
	$.post('./assets/servicenow_funcs.php', {
				callerPID: $("#callerPID").val(),
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val(),
				formAction: "verifyEmail"
	}, function(data) {
		
		if (data == 1) { 
			$("#callerPID").val($("#casUser").val()); 
			$('#feedback').html("Caller PID Not Found. Resetting to: "+$("#casUser").val());			
		} 

	}).fail(function() {
		$('#feedback').html("API call to Service-Now failed.");
	});
	
}

function quickAcknowledge(quickAckAlertData) {
	
				var qaData = quickAckAlertData.split("||");

				//alert("qaData is: "+qaData);
	
				$.post('./assets/cms_funcs.inc.php', {
									alertID: qaData[0], feName: qaData[1],
									apiToken: qaData[2], malwareName: qaData[3], 
									casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val(),
									incidentID: "QuickAck", feSourceIP: qaData[4]
				}, function(ackdata) {
					
					//alert(ackdata)
<?php
					// Reload the alerts list to show changes...
?>
					$('#response').html($('#response').html()+ackdata);
					getAlertsList();
					
				}).fail(function() {
					$('#response').html("Posting ACK to FE failed.");
				});	
}
		
function submitIncident() {

<?php
	// Some simple form validation...
?>
		
	if ($('#feSensorSrc').val() == "") { $("#feedback").html("Error: The Sensor Source value is empty... Error?"); return false }
	if ($('#feTimestamp').val() == "") { $("#feedback").html("Error: The Timestamp value should not be empty."); return false }
	if ($('#feVictimHost').val() == "") { $("#feedback").html("Error: The Hostname value is empty. Use 'Unkown' if it cannot be determined."); return false }
	if ($('#malwareComms').val() == "") { $("#feedback").html("Error: The Communication Ports value is empty. Use 'Unkown' if it cannot be determined."); return false }
	if ($('#malwareName').val() == "") { $("#feedback").html("Error: The Detected Issue/Malware Name value should not be empty."); return false }
	if ($('#malwareSeverity').val() == "") { $("#feedback").html("Error: The Malware Severity value is empty... Error?"); return false }	

	if ($('#malwareDesc').val() == $("#malwareDescTemp").val()) { $('#malwareDesc').val(""); }	
	if ($('#incidentNotes').val() == $('#incidentNotesTemp').val()) { $('#incidentNotes').val("") };
	if ($('#incidentComments').val() == $('#incidentCommentsTemp').val()) { $('#incidentComments').val("") };
	
	if ($('#malwareDesc').val() == "" && $("#userPermissions").val() == "dbreadwrite") { 
		if (confirm("Are you sure you want to submit this incident with a blank Malware Description?")) {
			// blank
		} else { 
			return; 
		}
	}
	
	if ($('#openINCWarning').is(":visible")) { 
		if (confirm($('#feSourceIP').val()+" has unresolved Incidents in SN. Are you sure you want to submit a new Incident?")) {
			// blank
		} else { 
			return; 
		}
	}

	//$("#response").html("<img src='./images/loading16x16.gif' /> <b>Loading SN response...</b>");

<?php
	// Update the Malware DB with any description changes...
?>
	if ($('#malwareDesc').val() != $("#malwareDescOrig").val()) {
		updateMalwareDB();
	}

<?php
	// Set the default between comments or work notes in Service-Now Incident as work notes but 
	// update the value based on whatever the user has selected...
?>
	var snCommOrNoteRadio = "worknote";
	if ($("#snComment").is(":checked")) { snCommOrNoteRadio = "comments"; } else { snCommOrNoteRadio = "worknotes"; }

<?php
	// servicenow_funcs.php handles all Service-Now API calls
?>
	$.post('./assets/servicenow_funcs.php', { 
						feSourceIP: $('#feSourceIP').val(), feSensorSrc: $("#feSensorSrc").val(),
						feTimestamp: $("#feTimestamp").val(), feVictimHost: $("#feVictimHost").val(), 
						malwareComms: $("#malwareComms").val(), malwareName: $("#malwareName").val(), 
						malwareSeverity: $("#malwareSeverity").val(), incidentNotes: $("#incidentNotes").val(), 
						incidentComments: $("#incidentComments").val(), malwareDesc: $("#malwareDesc").val(),
						callerPID: $("#callerPID").val(), contactEmail: $("#contactEmail").val(), assignmentGroup: $("#assignmentGroup").val(),
						casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val(), alertID: $("#alertID").val(),
						feAdditionalInfo: $("#feAdditionalInfo").val(), snCommOrNote: snCommOrNoteRadio,
						formAction: "submitIncident"
	}, function(data) {
<?php
				// if we have a successful response, we should get back a string with a delimiter, within it
				// that contains the Service-Now Incident number, the "|DATA|" delimiter and then some 
				// feedback pretaining to the response from Service-Now that will be presented to the user
?>
				var respData = data.split("|DATA|");
				$('#incidentID').val(respData[0]);
				$('#response').html(respData[1]);
				// DEBUG (show data before the split): 
				//$('#response').html(data);

				// DEBUG
				//alert("AlertID: "+$('#alertID').val()+", INC: "+$("#incidentID").val()+", API: "+$("#apiToken").val());
<?php				
				// If an incident was successfully created in Service-Now, we need to update the FireEye CMS 
				// and acknowledge the Alert with the Incident number and PID of the FEINT user
?>
				$.post('./assets/cms_funcs.inc.php', {
									alertID: $('#alertID').val(), feName: $("#feName").val(),
									apiToken: $("#apiToken").val(), malwareName: $("#malwareName").val(), 
									casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val(),
									incidentID: $("#incidentID").val(), feSourceIP: $("#feSourceIP").val()
				}, function(ackdata) {
<?php
					// FireEye should not return data on successful Alert Acknowledgement 
					// submission... however, we want to display this data in case there 
					// is an error
?>
					$('#response').html($('#response').html()+ackdata);
				}).fail(function() {
					$('#response').html("Posting ACK to FE failed.");
				});
				
	}).fail(function() {
		$('#response').html("Posting to SN failed.");
	});

<?php
	// Hide the formFrame DIV that contains all of the index.php content...
?>
	$("#formFrame").hide();
<?php
	// and show all of the data collected as responses from various APIs...
?>
	$("#responseFrame").show();
	
	return false;

}	// END submitIncident()
		
		
<?php
	// Convert a date string to ISO format
?>
function ISODateString(d) {
	//DEBUG
	//alert (d);
	function pad(n){
		return n<10 ? '0'+n : n
	}
	return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z'
}

<?php
	// When field gets focus... if its contents equals the default malware desc 
	// blurb with instructions on filling out the empty field, empty it so the 
	// user can do so...
?>
function inputFocus(i, defaultTxt) {
    if (i.value == defaultTxt) {
        i.value = "";
        $(i).removeClass("blurredDefaultText");
    }
}

<?php
	// if the field is empty and we're leaving it, put the default malware blurb
	// back in and gray it out...
?>
function inputBlur(i, defaultTxt) {
    if (i.value == "") {
        i.value = defaultTxt;
        $(i).addClass("blurredDefaultText");
    }
}

<?php		
	// Open the Generate Argus Command Dialog
?>
function generateArgus() {
	$( "#generateArgus" ).dialog({ title: "Generate Argus Command" });
}

<?php
	// Open the Generate LAA URL dialog and set initial parameters...
?>
function openLAADialog() {
	generateLAA();
	$( "#generateLAA" ).dialog({ title: "Generate Central Log Service (formerly LAA) URL", modal: true, minWidth: 1000, minHeight: 730, position: { my: "left top", at: "left top", of: window } });
}

<?php
	// Open the Show ACK History dialog and set initial parameters...
?>
function openACKHistDialog() {
	$( "#showAckHistory" ).dialog({ title: "SN Incident and FE Alert Acknowledgement History for "+$("#feSourceIP").val(), modal: true, minWidth: 1100, minHeight: 830, position: { my: "left top", at: "left top", of: window } });	
}


<?php
	// Grab all current alert values in various form fields and use them to 
	// generate a URL based on the selected Dashboard template and date/time 
	// ranges...
?>

function generateLAA() {
	
	// set defaults	
	var searchIP = $('#srcIP').val();
	
	if ($("#srcIP").is(":checked")) { searchIP = $('#srcIP').val(); }
	else if ($("#dstIP").is(":checked")) { searchIP = $('#dstIP').val(); }
	else if ($("#custQuery").is(":checked")) { searchIP = $('#custQueryText').val(); }

	// LAA Time example: 2017-02-26T03:40:39.890Z
	var fromLAA = $('#dateTimeFromLAA').val().split(' ');
	var toLAA = $('#dateTimeToLAA').val().split(' ');

	var fromDateString = fromLAA[0].replace(/-/g, "/")+" "+fromLAA[1];
	var toDateString = toLAA[0].replace(/-/g, "/")+" "+toLAA[1];
	
	var dateTimeFromLAAISO = ISODateString(new Date( fromDateString ));
	var dateTimeToLAAISO = ISODateString(new Date( toDateString ));
	
	// DEBUG
	//alert("LAA From Date is: "+fromLAA[0]+", LAA From Time is: "+fromLAA[1]+"\nFull LAA ISO From DateTime is "+dateTimeFromLAAISO+"\n");
	
	
		var laaURL_ITSO_IPLOOKUPv2 = "https://log.it.vt.edu/node/app/kibana#/dashboard/ab0e6030-65a7-11e8-8740-015b8c84429f?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_event,nat.border_domain,device.name),id:e74929d0-659e-11e8-8740-015b8c84429f,panelIndex:1,row:1,size_x:8,size_y:3,sort:!('@timestamp',desc),type:search),(col:9,id:e370e9f0-65a4-11e8-8740-015b8c84429f,panelIndex:2,row:1,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,src.ip,src.mac),id:ca578960-659f-11e8-8740-015b8c84429f,panelIndex:3,row:4,size_x:8,size_y:3,sort:!('@timestamp',desc),type:search),(col:9,id:f9d26fc0-65a4-11e8-8740-015b8c84429f,panelIndex:4,row:4,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.type,syslog.message),id:'942e86d0-65a0-11e8-8740-015b8c84429f',panelIndex:5,row:7,size_x:8,size_y:3,sort:!('@timestamp',desc),type:search),(col:9,id:'0df15890-65a5-11e8-8740-015b8c84429f',panelIndex:6,row:7,size_x:4,size_y:3,type:visualization),(col:1,columns:!('@timestamp',device.ip,wlan.auth_ip,src.ip,src.mac,aaa.client.name,username,aaa.action,aaa.radius-instance,aaa.eap-type,aaa.auth-status,message),id:d7e48d60-65a1-11e8-8740-015b8c84429f,panelIndex:7,row:10,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,device.interface,src.mac),id:'31c3c120-65a2-11e8-8740-015b8c84429f',panelIndex:8,row:13,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.interface,username,netrecon.building_name,netrecon.circuit_number,netrecon.outlet,netrecon.room,netrecon.building_id),id:a77e4de0-65a2-11e8-8740-015b8c84429f,panelIndex:9,row:17,size_x:6,size_y:3,sort:!('@timestamp',desc),type:search),(col:7,columns:!(netrecon.fact.type,netrecon.ipr_ip,netrecon.primary_contact,netrecon.secondary_contact,netrecon.ip_range_start,netrecon.ip_range_end,netrecon.block_size,netrecon.administrative_group),id:ff4a3200-65a2-11e8-8740-015b8c84429f,panelIndex:10,row:17,size_x:6,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,id:'8984d300-65a6-11e8-8740-015b8c84429f',panelIndex:11,row:16,size_x:12,size_y:1,type:visualization),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:'7c663db0-65a3-11e8-8740-015b8c84429f',panelIndex:12,row:20,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%22"+searchIP+"%22)),timeRestore:!f,title:itso-IP-Lookup-v2,uiState:(),viewMode:view)";
	
	
	//var laaURL_ITSO_IPLOOKUPv2 = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-IP-Lookup-v2?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:9,id:itso-NAT-IP-help-text,panelIndex:10,row:1,size_x:4,size_y:4,type:visualization),(col:9,id:itso-IP-to-MAC-help-text,panelIndex:11,row:5,size_x:4,size_y:3,type:visualization),(col:9,id:itso-DHCP-help-text,panelIndex:12,row:8,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_event,nat.border_domain,device.name),id:itso-BORDER-NAT,panelIndex:13,row:1,size_x:8,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,src.ip,src.mac),id:itso-IP_TO_MAC,panelIndex:14,row:5,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.time,dhcp.type,syslog.message),id:itso-DHCPD,panelIndex:15,row:8,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(%27@timestamp%27,device.ip,auth_ip,aaa.Radius-Client-Ip-Address,src.mac,username,aaa.action,aaa.Radius-Instance,aaa.EAP-Type,aaa.Authentication-Type,message),id:itso-AUTHN,panelIndex:16,row:11,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,device.interface,src.mac),id:itso-Mac_to_Port,panelIndex:17,row:14,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.interface,username,netrecon.building_name,netrecon.circuit_number,netrecon.outlet,netrecon.room,netrecon.building_id),id:itso-DEVICE_OUTLET,panelIndex:18,row:18,size_x:6,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:7,columns:!(netrecon.fact.type,netrecon.ipr_ip,netrecon.primary_contact,netrecon.secondary_contact,netrecon.ip_range_start,netrecon.ip_range_end,netrecon.block_size,netrecon.administrative_group),id:itso-IPR,panelIndex:19,row:18,size_x:6,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,id:itso-IPR-slash-Device-Outlet-Help-Text,panelIndex:20,row:17,size_x:12,size_y:1,type:visualization),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:itso-VPNPoolIP-and-radiusd,panelIndex:21,row:21,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),timeRestore:!f,title:%27itso-IP%20Lookup%20v2%27,uiState:(),viewMode:view)";
	
	
	var laaURL_ITSO_WIRED_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/4eb492b0-65ab-11e8-857d-99eb43df3d65?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(netrecon.fact.type,device.name,device.ip,src.ip,src.mac),id:ca578960-659f-11e8-8740-015b8c84429f,panelIndex:1,row:1,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,columns:!(netrecon.fact.type,netrecon.ipr_ip,netrecon.primary_contact,netrecon.secondary_contact,netrecon.ip_range_start,netrecon.ip_range_end,netrecon.block_size,netrecon.administrative_group),id:ff4a3200-65a2-11e8-8740-015b8c84429f,panelIndex:2,row:4,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,device.interface,src.mac),id:'31c3c120-65a2-11e8-8740-015b8c84429f',panelIndex:3,row:7,size_x:8,size_y:3,sort:!('@timestamp',desc),type:search),(col:9,id:'8984d300-65a6-11e8-8740-015b8c84429f',panelIndex:4,row:7,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,device.name,device.interface,username,netrecon.building_name,netrecon.circuit_number,netrecon.outlet,netrecon.room,netrecon.building_id),id:a77e4de0-65a2-11e8-8740-015b8c84429f,panelIndex:5,row:10,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.type,syslog.message),id:'942e86d0-65a0-11e8-8740-015b8c84429f',panelIndex:6,row:13,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%22"+searchIP+"%22)),timeRestore:!f,title:itso-Wired-IP-Lookup,uiState:(),viewMode:view)";
	
	
	var laaURL_ITSO_WIRELESS_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/3d914130-65ac-11e8-857d-99eb43df3d65?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_event,nat.border_domain,device.name),id:e74929d0-659e-11e8-8740-015b8c84429f,panelIndex:1,row:1,size_x:8,size_y:3,sort:!('@timestamp',desc),type:search),(col:9,id:e370e9f0-65a4-11e8-8740-015b8c84429f,panelIndex:2,row:1,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.type,syslog.message),id:'942e86d0-65a0-11e8-8740-015b8c84429f',panelIndex:3,row:4,size_x:8,size_y:3,sort:!('@timestamp',desc),type:search),(col:9,id:'0df15890-65a5-11e8-8740-015b8c84429f',panelIndex:4,row:4,size_x:4,size_y:3,type:visualization),(col:1,columns:!('@timestamp',device.ip,wlan.auth_ip,src.ip,src.mac,aaa.client.name,username,aaa.action,aaa.radius-instance,aaa.eap-type,aaa.auth-status,message),id:d7e48d60-65a1-11e8-8740-015b8c84429f,panelIndex:5,row:7,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:'7c663db0-65a3-11e8-8740-015b8c84429f',panelIndex:6,row:10,size_x:12,size_y:3,sort:!('@timestamp',desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%22"+searchIP+"%22)),timeRestore:!f,title:itso-Wireless-IP-Lookup,uiState:(),viewMode:view)";
	
	//var laaURL_ITSO_WIRELESS_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Wireless-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:9,id:itso-DHCP-help-text,panelIndex:12,row:5,size_x:4,size_y:3,type:visualization),(col:1,columns:!(%27@timestamp%27,device.ip,aaa.Radius-Client-Ip-Address,src.mac,username,aaa.action,aaa.Radius-Instance,aaa.EAP-Type,aaa.Authentication-Type,message),id:itso-AUTHN,panelIndex:15,row:8,size_x:12,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:itso-VPNPoolIP-and-radiusd,panelIndex:16,row:12,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_event,nat.border_domain,device.name),id:itso-BORDER-NAT,panelIndex:17,row:1,size_x:8,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:9,id:itso-NAT-IP-help-text,panelIndex:18,row:1,size_x:4,size_y:4,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.time,dhcp.type,syslog.message),id:itso-DHCPD,panelIndex:19,row:5,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),timeRestore:!f,title:%27itso-%20Wireless%20IP%20Lookup%27,uiState:(),viewMode:view)";
	
	var laaURL_ITSO_ARGUS_24HOUR = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Argus-24-Hour-View?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-24h,mode:quick,to:now))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,id:itso-Visualization-Argus-Edge-Line-Chart,panelIndex:1,row:1,size_x:12,size_y:4,type:visualization),(col:1,id:itso-Visualization-Argus-Core-Line-Chart,panelIndex:2,row:5,size_x:12,size_y:4,type:visualization),(col:1,id:itso-Visualization-Argus-Rlan-Line-Chart,panelIndex:3,row:9,size_x:12,size_y:4,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:%27*%27)),timeRestore:!t,title:%27itso-Argus%2024%20Hour%20View%27,uiState:(P-1:(spy:(mode:(fill:!f,name:!n)),vis:(legendOpen:!f)),P-2:(vis:(legendOpen:!f)),P-3:(vis:(legendOpen:!f))),viewMode:view)";

<?php
/*
	var laaURL_ITSO_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/ITSO-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,id:NIS-netrecon-fact-count-PIE,panelIndex:33,row:1,size_x:3,size_y:5,type:visualization),(col:4,id:NIS-netrecon-All-Script-Fact-Count,panelIndex:34,row:1,size_x:6,size_y:5,type:visualization),(col:10,id:NIS-netrecon-fact-count-TABLE,panelIndex:36,row:1,size_x:3,size_y:5,type:visualization),(col:7,columns:!(src_ip,dst_ip,nat_border_start_port,nat_border_stop_port,nat_border_domain,device_name,nat_border_event,nat_border_route_domain),id:NIS-netrecon-BORDER-NAT,panelIndex:37,row:8,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(received_from,src_ip,src_mac,username,aaa_Radius-Instance,aaa_Calling-Station-Id,aaa_action,aaa_EAP-Type,aaa_Authority,aaa_Authentication-Type,aaa_NAS-IP-Address,aaa_NAS-Identifier,aaa_Radius-Client-Ip-Address,aaa_Virtual-Server),id:NIS-netrecon-RADIUS,panelIndex:38,row:12,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,device_interface,src_mac),id:NIS-netrecon-MAC_TO_PORT,panelIndex:39,row:6,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:7,columns:!(netrecon_fact_type,src_ip,src_mac,device_name,device_ip),id:NIS-netrecon-IP_TO_MAC,panelIndex:40,row:6,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,dhcp_time,dhcp_type),id:NIS-netrecon-DHCPD,panelIndex:41,row:8,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,ssid,username,src_ip,src_mac,wlan_AAA_profile,wlan_event,wlan_ap_assoc_time,wlan_auth_ip,wlan_auth_method,wlan_auth_server,wlan_bssid,wlan_vlan,wlan_deauth_cause,wlan_deauth_message,wlan_message_type,wlan_role,ap_name,ap_ip,ap_mac,received_from),id:NIS-netrecon-All-Wireless,panelIndex:42,row:10,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_interface,username,netrecon_building_name,netrecon_circuit_number,netrecon_outlet,netrecon_room,netrecon_building_id),id:NIS-netrecon-DEVICE_OUTLET,panelIndex:43,row:14,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,netrecon_ipr_ip,netrecon_ip_range_start,netrecon_ip_range_end,netrecon_primary_contact,netrecon_secondary_contact,netrecon_block_size,netrecon_administrative_group),id:NIS-netrecon-IPR,panelIndex:44,row:18,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(aaa_alert,aaa_enforcement_profiles,aaa_errcode,aaa_login_status,aaa_roles,aaa_session_type,device_ip,device_time,log_time,message,request_timestamp,src_ip,src_mac,username,role_name),id:NIS-netrecon-AUTHN,panelIndex:46,row:16,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),title:%27ITSO-IP%20Lookup%27,uiState:())";

	var laaURL_ITSO_OLDWIRED_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Wired-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(netrecon_fact_type,device_name,device_interface,username,netrecon_building_name,netrecon_circuit_number,netrecon_outlet,netrecon_room,netrecon_building_id),id:itso-DEVICE_OUTLET,panelIndex:1,row:7,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,dhcp_time,dhcp_type,syslog_message),id:itso-DHCPD,panelIndex:2,row:10,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:9,columns:!(netrecon_primary_contact,netrecon_secondary_contact,netrecon_ipr_ip,netrecon_fact_type,netrecon_ip_range_start,netrecon_ip_range_end,netrecon_block_size,netrecon_administrative_group),id:itso-IPR,panelIndex:3,row:1,size_x:4,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,src_ip,src_mac),id:itso-IP_TO_MAC,panelIndex:4,row:1,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,device_interface,src_mac),id:itso-Mac_to_Port,panelIndex:5,row:4,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),title:%27itso-%20Wired%20IP%20Lookup%27,uiState:())";

	var laaURL_ITSO_OLDWIRELESS_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Wireless-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(device_name,src_ip,dst_ip,nat_border_start_port,nat_border_stop_port,nat_border_event,nat_border_domain,netrecon_fact_type,received_from,nat_border_route_domain),id:itso-BORDER-NAT,panelIndex:3,row:1,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,dhcp_time,dhcp_type,syslog_message),id:itso-DHCPD,panelIndex:4,row:4,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(device_ip,src_mac,username,message),id:itso-AUTHN,panelIndex:8,row:7,size_x:12,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:9,id:itso-NAT-IP-help-text,panelIndex:10,row:1,size_x:4,size_y:3,type:visualization),(col:9,id:itso-DHCP-help-text,panelIndex:12,row:4,size_x:4,size_y:3,type:visualization),(col:1,columns:!(dst_ip,src_ip,username,ras_msg,ras_roles),id:itso-VPNPoolIP-and-radiusd,panelIndex:13,row:11,size_x:12,size_y:4,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),title:%27itso-%20Wireless%20IP%20Lookup%27,uiState:())";

	var laaURL_NISNETRECON_ALLFACTS = "https://log.it.vt.edu/node/app/kibana#/dashboard/NIS-netrecon-all-facts?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,id:NIS-netrecon-fact-count-PIE,panelIndex:33,row:1,size_x:3,size_y:5,type:visualization),(col:4,id:NIS-netrecon-All-Script-Fact-Count,panelIndex:34,row:1,size_x:6,size_y:5,type:visualization),(col:10,id:NIS-netrecon-fact-count-TABLE,panelIndex:36,row:1,size_x:3,size_y:5,type:visualization),(col:7,columns:!(src_ip,dst_ip,nat_border_start_port,nat_border_stop_port,nat_border_domain,device_name,nat_border_event,nat_border_route_domain),id:NIS-netrecon-BORDER-NAT,panelIndex:37,row:8,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(received_from,src_ip,src_mac,username,aaa_Radius-Instance,aaa_Calling-Station-Id,aaa_action,aaa_EAP-Type,aaa_Authority,aaa_Authentication-Type,aaa_NAS-IP-Address,aaa_NAS-Identifier,aaa_Radius-Client-Ip-Address,aaa_Virtual-Server),id:NIS-netrecon-RADIUS,panelIndex:38,row:12,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,device_interface,src_mac),id:NIS-netrecon-MAC_TO_PORT,panelIndex:39,row:6,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:7,columns:!(netrecon_fact_type,src_ip,src_mac,device_name,device_ip),id:NIS-netrecon-IP_TO_MAC,panelIndex:40,row:6,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,dhcp_time,dhcp_type),id:NIS-netrecon-DHCPD,panelIndex:41,row:8,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,ssid,username,src_ip,src_mac,wlan_AAA_profile,wlan_event,wlan_ap_assoc_time,wlan_auth_ip,wlan_auth_method,wlan_auth_server,wlan_bssid,wlan_vlan,wlan_deauth_cause,wlan_deauth_message,wlan_message_type,wlan_role,ap_name,ap_ip,ap_mac,received_from),id:NIS-netrecon-All-Wireless,panelIndex:42,row:10,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,device_interface,username,netrecon_building_name,netrecon_circuit_number,netrecon_outlet,netrecon_room,netrecon_building_id),id:NIS-netrecon-DEVICE_OUTLET,panelIndex:43,row:14,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,netrecon_ipr_ip,netrecon_ip_range_start,netrecon_ip_range_end,netrecon_primary_contact,netrecon_secondary_contact,netrecon_block_size,netrecon_administrative_group),id:NIS-netrecon-IPR,panelIndex:44,row:18,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(aaa_alert,aaa_enforcement_profiles,aaa_errcode,aaa_login_status,aaa_roles,aaa_session_type,device_ip,device_time,log_time,message,request_timestamp,src_ip,src_mac,username,role_name),id:NIS-netrecon-AUTHN,panelIndex:46,row:16,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),title:%27NIS%20netrecon%20all%20facts%27,uiState:())";

	
	var laaURL_NISNETRECON_WIRELESS = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Wireless-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_domain,nat.border_event,device.name),id:itso-BORDER-NAT,panelIndex:3,row:1,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:9,id:itso-NAT-IP-help-text,panelIndex:10,row:1,size_x:4,size_y:3,type:visualization),(col:9,id:itso-DHCP-help-text,panelIndex:12,row:4,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.time,dhcp.type,syslog.message),id:itso-DHCPD,panelIndex:14,row:4,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(%27@timestamp%27,device.ip,aaa.Radius-Client-Ip-Address,src.mac,username,aaa.action,aaa.Radius-Instance,aaa.EAP-Type,aaa.Authentication-Type,message),id:itso-AUTHN,panelIndex:15,row:7,size_x:12,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:itso-VPNPoolIP-and-radiusd,panelIndex:16,row:11,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),timeRestore:!f,title:%27itso-%20Wireless%20IP%20Lookup%27,uiState:(),viewMode:view)";

	var laaURL_NISNETRECON_WIRELESS = "https://log.it.vt.edu/node/app/kibana#/dashboard/NIS-netrecon-wireless?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,id:NIS-netrecon-Wireless-Fact-Count-LINE,panelIndex:12,row:1,size_x:12,size_y:2,type:visualization),(col:1,columns:!(netrecon_fact_type,device_name,device_ip,ssid,username,wlan_AAA_profile,wlan_ap_assoc_time,wlan_auth_ip,wlan_auth_method,wlan_auth_server,wlan_bssid,wlan_deauth_cause,wlan_deauth_message,wlan_event,wlan_message_type,wlan_role,wlan_vlan,ap_ip,ap_mac,ap_name,src_ip,src_mac,received_from),id:NIS-netrecon-All-Wireless,panelIndex:13,row:3,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,dhcp_time,dhcp_type,syslog_message),id:NIS-netrecon-DHCPD,panelIndex:14,row:5,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,device_name,src_ip,dst_ip,nat_border_start_port,nat_border_stop_port,nat_border_domain,nat_border_event,received_from,nat_border_route_domain),id:NIS-netrecon-BORDER-NAT,panelIndex:15,row:7,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(received_from,src_ip,src_mac,username,aaa_Radius-Instance,aaa_Calling-Station-Id,aaa_action,aaa_EAP-Type,aaa_Authority,aaa_Authentication-Type,aaa_NAS-IP-Address,aaa_NAS-Identifier,aaa_Radius-Client-Ip-Address,aaa_Virtual-Server),id:NIS-netrecon-RADIUS,panelIndex:16,row:9,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),title:%27NIS%20netrecon%20wireless%27,uiState:())";

	var laa_URL_NISNETRECON_IPLOCATION = "https://log.it.vt.edu/node/app/kibana#/dashboard/NIS-netrecon-IP-Location?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,id:NIS-Netrecon-Search-for-IP-location-Instructions,panelIndex:36,row:1,size_x:5,size_y:4,type:visualization),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,device_name,device_ip),id:NIS-netrecon-IP_TO_MAC,panelIndex:37,row:7,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:7,columns:!(netrecon_fact_type,src_mac,device_name,device_interface,device_ip),id:NIS-netrecon-MAC_TO_PORT,panelIndex:38,row:7,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,src_ip,src_mac,dhcp_time,dhcp_type),id:NIS-netrecon-DHCPD,panelIndex:39,row:9,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:7,columns:!(src_ip,dst_ip,nat_border_start_port,nat_border_stop_port,nat_border_domain,nat_border_event,device_name,netrecon_fact_type,nat_border_route_domain),id:NIS-netrecon-BORDER-NAT,panelIndex:40,row:9,size_x:6,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:6,id:NIS-netrecon-IP-Location-TABLE,panelIndex:41,row:1,size_x:3,size_y:4,type:visualization),(col:9,id:NIS-netrecon-IP-Location-PIE,panelIndex:42,row:1,size_x:4,size_y:4,type:visualization),(col:1,id:NIS-netrecon-IP-Location-LINE,panelIndex:43,row:5,size_x:12,size_y:2,type:visualization),(col:1,columns:!(netrecon_fact_type,device_name,device_interface,username,netrecon_building_name,netrecon_circuit_number,netrecon_outlet,netrecon_room,netrecon_building_id),id:NIS-netrecon-DEVICE_OUTLET,panelIndex:44,row:11,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(%27@timestamp%27,asa_nat_domain,src_ip,dst_ip,asa_nat_action,asa_nat_src_nameif,asa_nat_dst_nameif,asa_nat_hrs_mins_secs,asa_nat_xlate_type),id:NIS-netrecon-ASA_NAT_XLATE,panelIndex:46,row:15,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon_fact_type,netrecon_ipr_ip,netrecon_primary_contact,netrecon_secondary_contact,netrecon_ip_range_start,netrecon_ip_range_end,netrecon_block_size,netrecon_administrative_group),id:NIS-netrecon-IPR,panelIndex:47,row:13,size_x:12,size_y:2,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),title:%27NIS%20netrecon%20IP%20Location%27,uiState:())";
*/
?>

	var laaURL = laaURL_ITSO_IPLOOKUPv2;

	if ($("#urlDash").val() == "laaURL_ITSO_IPLOOKUPv2") { laaURL = laaURL_ITSO_IPLOOKUPv2; }
	//else if ($("#urlDash").val() == "laaURL_ITSO_IPLOOKUP") { laaURL = laaURL_ITSO_IPLOOKUP; }
	else if ($("#urlDash").val() == "laaURL_ITSO_WIRED_IPLOOKUP") { laaURL = laaURL_ITSO_WIRED_IPLOOKUP; }
	else if ($("#urlDash").val() == "laaURL_ITSO_WIRELESS_IPLOOKUP") { laaURL = laaURL_ITSO_WIRELESS_IPLOOKUP; }	
	//else if ($("#urlDash").val() == "laaURL_NISNETRECON_ALLFACTS") { laaURL = laaURL_NISNETRECON_ALLFACTS; }
	//else if ($("#urlDash").val() == "laaURL_NISNETRECON_WIRELESS") { laaURL = laaURL_NISNETRECON_WIRELESS; }
	//else if ($("#urlDash").val() == "laa_URL_NISNETRECON_IPLOCATION") { laaURL = laa_URL_NISNETRECON_IPLOCATION; }

	$("#laaURLDashboardName").html( $("#urlDash option:selected").text() );
	
	// set the textarea to the crafted URL
	$('#laaURL').val(laaURL);
	
}

<?php
// Performs an update, insert or delete against the malware database with
// the mode based on the mwFunc parameter that is POSTed to 
// malwaredb_funcs.php.  Additional details can be found in that file. 
?>
function updateMalwareDB() {

	$("#feedback").html("<b>Processing action... please wait...</b>");

	$.post('./assets/malwaredb_funcs.php', {
						mwID: $("#mwID").val(), mwFunc: $("#mwFunc").val(), 
						malwareSeverity: $("#malwareSeverity").val(), 
						malwareName: $("#malwareName").val(), 
						malwareDesc: $("#malwareDesc").val(),
						casUser: $("#casUser").val()
	}, function(data) {

					$('#feedback').html(data);

	}).fail(function() {
		$('#feedback').html("Malware DB operation failed.");
	});

	return false;	
}

<?php
	// When we select an item in the auto-complete list, we need to parse 
	// the data and update the relevant fields
?>
function acSelected(event, ui) {
	var acString = ui.item.value;
	var acInfo = acString.split("||");
	$("#mwID").val(acInfo[0]);
	$("#malwareSeverity").val(acInfo[1]);
	$("#malwareName").val(acInfo[2]);
	$("#malwareDesc").val(acInfo[3]);
	
	// now that we've handled the data, don't re-modify the malwareName field
	return false;
}
		
<?php
/*
// NO LONGER USED...
// ConfirmDialog does not pause form execution... build-in javascript confirm()
// function used instead...
function ConfirmDialog(diagTitle, diagMessage) {
	$('<div></div>').appendTo('body').html('<div>'+diagMessage+'</div>').dialog({
		modal: true, title: diagTitle, zIndex: 10000, autoOpen: true,
		width: 'auto', resizable: false,
		buttons: {
			Yes: function () {
				// $(obj).removeAttr('onclick');                                
				// $(obj).parents('.Parent').remove();
				// $('body').append('<h1>Confirm Dialog Result: <i>Yes</i></h1>');
				$(this).dialog("close");
			},
			No: function () {
				//$('body').append('<h1>Confirm Dialog Result: <i>No</i></h1>');
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
}

// NO LONGER USED... Extended Query to FireEye API used instead...
function downloadAlertPDF() {
	
	//$("#feedback").html("<img src='./images/loading16x16.gif' /> <b>Downloading Alert PDF... this can take around 2 minutes...</b>");
	//$( "#alertPDFForm" ).submit();
	
	return false;
}
*/
?>

	</script>
<?php 

// EOF javascripts.inc.php

?>