<?php
/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	history_tool.php
	
	Shows history for malware items and Service-Now Incidents recorded in 
	FEINT
	
****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>FEINT - ITSO Malware Database Update Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("javascripts.inc.php");
?>
</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
		<div class="container">

<?php
	// NO FUNCTION - DISPLAY LIST
	// no function has been passed so display the list of malware entries
	if (!isset($_GET["mwFunc"])) {
?>
	<!-- form start -->  

		<p style="width: 700px">
			<br/>
			This tool will allow you to review the history of Incidents and malware items submitted through 
			FEINT - FireEye ITSO Notification Tool. <br/>
			<br/>
			<br/>
		</p>
<?php

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	/* MALWARE INFO DB LOOKUP */ 

	include_once("./assets/classes.inc.php");
	include("./assets/db_info.inc.php");
	$dbName = "feint";

		$dbName = "feint";
		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			unset($dbUser);
			unset($dbPass);

			$statement = $connection->prepare('SELECT id, name FROM malware ORDER BY name ASC');
			$statement->execute();
?>
		<div id="formFrame" style="width:900px">
			<form>
				
			<table width="900px" border="0" class="malwareListTable">
				<tr>
					<td id="malwareListHeader" colspan="2">
						<strong>Incidents List</strong>
					</td>
				</tr>
				
				
				<tr>
					<td>
						<input type="text" id="malwareName" name="malwareName" value="" size="45" />
						<input type="hidden" id="mwFunc" name="mwFunc" value="edit" />
						<input type="hidden" id="mwID" name="mwID" value="" />
						<input type="hidden" id="malwareDesc" name="malwareDesc" value="" />
						<input type="hidden" id="casUser" name="casUser" value="<?php print $_SERVER["HTTP_CAS_UUPID"]; ?>" />
					</td>
					<td class="deleteCell">
						<input type="button" id="malwareSearch" value="Search" />
					</td>
				</tr>
<?php
		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
?>
			</table>

			<table id="incListTable" class="display" cellspacing="0" width="100%">
<?php				
		include("./assets/db_info.inc.php");

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);
						
			$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP, logIncMalware FROM logs WHERE logType = "createIncident" ORDER BY logID ASC');
			$statement->execute();

			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
			
			if ($statement->rowCount() > 0) {
?>
				<thead>
				<tr>
					<th>Timestamp</th>
					<th>Malware Name</th>
					<th>Incident</th>
					<th>FE Alert ID</th>
				</tr>
				</thead>
				<tfoot>
				<tr>
					<th>Timestamp</th>
					<th>Malware Name</th>
					<th>Incident</th>
					<th>FE Alert ID</th>
				</tr>
				</tfoot>
				<tbody>
<?php
				foreach ($rows as $row) { 
					
					if ($row["logIncMalware"] == "") { $row["logIncMalware"] = "n/a, featured added 06/23/17"; }
?>
				<tr>
					<td><?php print $row["logDate"]; ?></td>
					<td><?php print $row["logIncMalware"]; ?></td>
					<td><a href="https://vt4help.service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=<?php print $row["logInfo"]; ?>" target="_blank" style="color: #fe5b00;"><?php print $row["logInfo"]; ?></a></td>
					<td><a href="https://fireeye.iso.vt.edu/event_stream/events_for_bot?ev_id=<?php print $row["logDataID"]; ?>" target="_blank" style="color: #fe5b00;"><?php print $row["logDataID"]; ?></a></td>
				</tr>
<?php
				}
			}
		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
		
	// clear the connection
	$connection = null;

?>
			</table>
			</form>
			<br/>
			<br/>			
		</div>
<?php

	}				
?>
		</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>