<?php
// This is incomplete and not used.

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>FireEye Reporting Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("javascripts.inc.php");
?>
</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="content">
					<h2>FireEye Reporting Tool</h2>
				</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->// This will technically force a log out without any confirmation and before 
// being redirected to the CAS logout page.  However, due to the way that 
// VT handles logouts, this is necessary as VT's CAS logout does not 
// provide the proper signal from its logout mechanism back to the 
// application, so as soon as the user lands at the CAS logout 
// confirmation, there's no way to log out from FEINT.

	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
		<div class="container">
			
<?php
	if (!isset($_GET["alertDuration"])) {
?>

			<p>Use this form to generate a FireEye Alert Report for the specified Duration.<br/><br/></p>

			<form id="feReportForm">
				<table class="incidentFormTable">
				<tr>
					<td width="300px">
					Search Duration: <br/>
					<select id="alertDuration" name="alertDuration">
						<option value="1_hour">1 Hour</option>
						<option value="2_hours">2 Hours</option>
						<option value="6_hours">6 Hours</option>
						<option value="12_hours">12 Hours</option>
						<option value="24_hours">24 Hours</option>
						<option value="48_hours">48 Hours</option>
						<option value="7_days">7 days</option>
						<option value="14_days">14 Days</option>
						<option value="30_days" selected="selected">30 Days</option>
					</select> 
					</td>
					<td width="600px" colspan="2">
						Alert Info Level: <br/> 
					<select id="alertInfoLevel" name="alertInfoLevel">
						<option value="concise" selected="selected">Concise</option>
						<option value="normal">Normal</option>
						<option value="extended">Extended</option>
					</select> 
					</td>
				</tr>
				<tr>
					<td width="900px" colspan="2">
						<p>Filter: <br/><input type="text" id="filterText" name="filterText" value="" size="45" /></p>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" id="submitReport" value="Generate Report" />
					</td>
				</tr>
				</table>
			</form>			

<?php
	} else {   // FE ALERT DURATION IS SET
		
		include("./assets/cms_info.inc.php");
		include_once("./assets/cms_funcs.inc.php");
		
		// DEBUG: checking info access
		//print "cmsUser is: ".$cmsUser."<br/>";
		$apiToken = cmsAuthorize($cmsUser,$cmsPass);
		unset($cmsUser);
		unset($cmsPass);

		// Trim and sanitize $alertID to an int
		$alertDuration = trim($_GET["alertDuration"]);
		$alertID = filter_var($alertDuration, FILTER_SANITIZE_STRING);
				
		$apiAlertsJSON = cmsGetAlertsWithinTimeframe($apiToken, "concise", $alertDuration);

		//print "<br/>DEBUG: JSON Response is: <br/>";
		var_dump($apiAlertsJSON);

		// Decode the JSON object into a stdClass
		$apiAlerts = json_decode($apiAlertsJSON);
		
		//print "<br/>DEBUG: CLASS Response is: <br/>";
		//var_dump($apiAlerts);
			
		if (isset($apiAlerts->httpStatus)) {
			print "<br/><strong>".$apiAlerts->httpStatus."</strong> - Unable to connect to the Cadbury CMS.  Most likely, the API token has expired.  Please refresh this page to renew the API token. <br/><br/>";
			print "<strong>Error:</strong> ".$apiAlerts->errorCode.", HTTP Status: ".$apiAlerts->httpStatus." - ".$apiAlerts->description."<br/><br/>";
			print "Halting execution since the CMS could not be queried.<br/><br/>";
			print "<a href=\"javascript:window.location.reload();\">Refresh page</a><br/><br/>";
			exit;
		}
/*
		// Trim and Sanitize values from FireEye API (just in case)
		if ($apiAlerts->alert->{"@attributes"}->severity == "crit") { $feSeverityCritSelected = "selected=\"selected\""; } else { $feSeverityCritSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->severity == "majr") { $feSeverityMajrSelected = "selected=\"selected\""; } else { $feSeverityMajrSelected = ""; }
		if ($apiAlerts->alert->{"@attributes"}->severity == "minr") { $feSeverityMinrSelected = "selected=\"selected\""; } else { $feSeverityMinrSelected = ""; }		
*/
	}  // FE ALERT DURATION END
?>

			
		</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>