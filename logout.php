<?php 
/****************************************************************************

	The FEINT queries information from the FireEye CMS and prepares it 
	for submission to Virginia Tech's Service-Now Incident system, then
	updates FireEye to acknowledge the alert with the Service-Now Incident 
	number and the user's PID who submitted the incident.

****************************************************************************/
/****************************************************************************

	logout.php
	
	This will technically force a log out without any confirmation and before 
	being redirected to the CAS logout page.  However, due to the way that 
	VT handles logouts, this is necessary as VT's CAS logout does not 
	provide the proper signal from its logout mechanism back to the 
	application, so as soon as the user lands at the CAS logout 
	confirmation, there's no way to log out from FEINT.

****************************************************************************/

unset($_SESSION['MOD_AUTH_CAS_S']);
setcookie('MOD_AUTH_CAS_S');

unset($_SESSION['MOD_AUTH_CAS']);
setcookie('MOD_AUTH_CAS');

unset($_SERVER["REMOTE_USER"]);

header('Location: https://login.vt.edu/profile/Logout');

?>