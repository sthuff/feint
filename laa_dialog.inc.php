<?php 

/****************************************************************************

	FEINT LAA Form Include
	
	This makes up the HTML form that appears in the Generate LAA URL 
	jQuery Dialog box.

****************************************************************************/

?>
			<p>Use this form to generate a LAA URL at the specified date/time.  By default, the Source IP of the FireEye alert is used and the date/time range is from 
				<strong>2 hours prior</strong> to the alert to <strong>5 minutes after</strong> the alert. <br/><br/></p>

			<p>The default LAA Dashboard is <strong>ITSO-IP-Lookup</strong> but can be changed using the drop-down list below. <br/></p>

			<form id="laaForm">
				<table class="incidentFormTable">
				<tr>
					<td width="900px" colspan="3">
					LAA Dashboard: <br/>
					<select id="urlDash">
						<option value="laaURL_ITSO_IPLOOKUPv2" selected="selected">ITSO-IP-Lookup v2</option>
						<option value="laaURL_ITSO_WIRED_IPLOOKUP">ITSO-Wired-IP-Lookup</option>
						<option value="laaURL_ITSO_WIRELESS_IPLOOKUP">ITSO-Wireless-IP-Lookup</option>	
<?php
/*
						<option value="laaURL_ITSO_IPLOOKUP">ITSO-IP-Lookup</option>
						<option value="laaURL_NISNETRECON_ALLFACTS">NIS netrecon all facts</option>
						<option value="laaURL_NISNETRECON_WIRELESS">NIS netrecon wireless</option>
						<option value="laa_URL_NISNETRECON_IPLOCATION">NIS netrecon IP location</option>
*/
?>
					</select> 
					</td>
				</tr>
				<tr>
					<td width="300px">
						<p><label><input type="radio" name="urlIP" id="srcIP" value="<?php print $feSourceIP; ?>" checked="checked"> Use Source IP: <br/><?php print $feSourceIP." port ".$feSourcePort; ?></label></p>
					</td>
					<td width="300px">
						<p><label><input type="radio" name="urlIP" id="dstIP" value="<?php print $feDestinationIP; ?>"> Use Destination IP: <br/><?php print $feDestinationIP." port ".$feDestinationPort; ?> </label></p>
					</td>
					<td width="300px">
						<p><label><input type="radio" name="urlIP" id="custQuery" value="custQuery"> Custom Query: <br/><input type="text" id="custQueryText" name="custQueryText" value="" /></label></p>
					</td>
				</tr>
				<tr>
					<td>
						<p>FireEye Alert Date: <br/><?php print $feTimestamp; ?>
					</td>
					<td>
						<p>Search From: <br/>
						<input type="text" name="dateTimeFromLAA" id="dateTimeFromLAA" value="<?php print $feTimestampESTSubTime; ?>" />
						<input type="hidden" name="dateTimeFromLAAFormat" id="dateTimeFromLAAFormat" value="<?php print $feTimestampESTSubTimeFormat; ?>" />
						</p>
					</td>
					<td>
						<p>Search To: <br/>
						<input type="text" name="dateTimeToLAA" id="dateTimeToLAA" value="<?php print $feTimestampESTAddTime; ?>" />
						<input type="hidden" name="dateTimeToLAAFormat" id="dateTimeToLAAFormat" value="<?php print $feTimestampESTAddTimeFormat; ?>" />
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">LAA URL for <strong><span id="laaURLDashboardName">ITSO-IP-Lookup</span></strong>: <br/>
						<textarea id="laaURL" name="laaURL" rows="10" cols="80"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<input type="button" id="copyLAAText" value="Copy URL to Clipboard" />
					</td>
				</tr>
				</table>
			</form>			
<?php

// EOF

?>