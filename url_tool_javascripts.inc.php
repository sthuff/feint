<?php 

/****************************************************************************

	ITSO Web URL Tool Javascript and jQuery Includes

****************************************************************************/

?>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
	<link type="text/css" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<link type="text/css" rel="stylesheet" href="./css/jquery-ui-timepicker-addon.css">
	<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="./js/copyme.js"></script>
	<script type="text/javascript" src="./js/moment.js"></script>



	<script>
	$(document).ready(function(){

		// HANDLE LAA EVENTS
		$("#urlDash").change(generateLAA);
		
		$("#dateTimeFromLAA").change(generateLAA);
		$("#dateTimeToLAA").change(generateLAA);

		$("#custQueryText").change(generateLAA);
		
		// LAA Time example: 2017-02-26T03:40:39.890Z
		var startDateTextBox = $("#dateTimeFromLAA");
		var endDateTextBox = $("#dateTimeToLAA");
		startDateTextBox.datetimepicker({ dateFormat: "yy-mm-dd", timeInput: true, timeFormat: "HH:mm:ss z",
											onClose: function(dateText, inst) {
												if (endDateTextBox.val() != '') {
													var testStartDate = startDateTextBox.datetimepicker('getDate');
													var testEndDate = endDateTextBox.datetimepicker('getDate');
													if (testStartDate > testEndDate)
														endDateTextBox.datetimepicker('setDate', testStartDate);
												}
												else { endDateTextBox.val(dateText); }
											}
										 }); 
		
		endDateTextBox.datetimepicker({ dateFormat: "yy-mm-dd", timeInput: true, timeFormat: "HH:mm:ss z", 
											onClose: function(dateText, inst) {
												if (startDateTextBox.val() != '') {
													var testStartDate = startDateTextBox.datetimepicker('getDate');
													var testEndDate = endDateTextBox.datetimepicker('getDate');
													if (testStartDate > testEndDate)
														startDateTextBox.datetimepicker('setDate', testEndDate);
												}
												else { startDateTextBox.val(dateText); }
											}
										}); 

		// Copy LAA Text to clipboard
		$('#copyLAAText').click(function(){ $('#laaURL').copyme(); });
		
		generateLAA();
				
	});	// END DOCREADY

function ISODateString(d) {
	//DEBUG
	//alert (d);
	function pad(n){
		return n<10 ? '0'+n : n
	}
	return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z'
}
	
function generateLAA() {
	
	// set defaults	
	var searchIP = $('#custQueryText').val();

	// LAA Time example: 2017-02-26T03:40:39.890Z
	var fromLAA = $('#dateTimeFromLAA').val().split(' ');
	var toLAA = $('#dateTimeToLAA').val().split(' ');

	var fromDateString = fromLAA[0].replace(/-/g, "/")+" "+fromLAA[1];
	var toDateString = toLAA[0].replace(/-/g, "/")+" "+toLAA[1];
	
	var dateTimeFromLAAISO = ISODateString(new Date( fromDateString ));
	var dateTimeToLAAISO = ISODateString(new Date( toDateString ));
	
	// DEBUG
	//alert("LAA From Date is: "+fromLAA[0]+", LAA From Time is: "+fromLAA[1]+"\nFull LAA ISO From DateTime is "+dateTimeFromLAAISO+"\n");
	
	var laaURL_ITSO_IPLOOKUPv2 = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-IP-Lookup-v2?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:9,id:itso-NAT-IP-help-text,panelIndex:10,row:1,size_x:4,size_y:4,type:visualization),(col:9,id:itso-IP-to-MAC-help-text,panelIndex:11,row:5,size_x:4,size_y:3,type:visualization),(col:9,id:itso-DHCP-help-text,panelIndex:12,row:8,size_x:4,size_y:3,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_event,nat.border_domain,device.name),id:itso-BORDER-NAT,panelIndex:13,row:1,size_x:8,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,src.ip,src.mac),id:itso-IP_TO_MAC,panelIndex:14,row:5,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.time,dhcp.type,syslog.message),id:itso-DHCPD,panelIndex:15,row:8,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(%27@timestamp%27,device.ip,auth_ip,aaa.Radius-Client-Ip-Address,src.mac,username,aaa.action,aaa.Radius-Instance,aaa.EAP-Type,aaa.Authentication-Type,message),id:itso-AUTHN,panelIndex:16,row:11,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,device.interface,src.mac),id:itso-Mac_to_Port,panelIndex:17,row:14,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.interface,username,netrecon.building_name,netrecon.circuit_number,netrecon.outlet,netrecon.room,netrecon.building_id),id:itso-DEVICE_OUTLET,panelIndex:18,row:18,size_x:6,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:7,columns:!(netrecon.fact.type,netrecon.ipr_ip,netrecon.primary_contact,netrecon.secondary_contact,netrecon.ip_range_start,netrecon.ip_range_end,netrecon.block_size,netrecon.administrative_group),id:itso-IPR,panelIndex:19,row:18,size_x:6,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,id:itso-IPR-slash-Device-Outlet-Help-Text,panelIndex:20,row:17,size_x:12,size_y:1,type:visualization),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:itso-VPNPoolIP-and-radiusd,panelIndex:21,row:21,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),timeRestore:!f,title:%27itso-IP%20Lookup%20v2%27,uiState:(),viewMode:view)";
	
	var laaURL_ITSO_WIRED_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Wired-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,columns:!(netrecon.fact.type,device.name,device.ip,src.ip,src.mac),id:itso-IP_TO_MAC,panelIndex:6,row:1,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,netrecon.ipr_ip,netrecon.primary_contact,netrecon.secondary_contact,netrecon.ip_range_start,netrecon.ip_range_end,netrecon.block_size,netrecon.administrative_group),id:itso-IPR,panelIndex:7,row:4,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.ip,device.interface,src.mac),id:itso-Mac_to_Port,panelIndex:8,row:7,size_x:9,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,device.name,device.interface,username,netrecon.building_name,netrecon.circuit_number,netrecon.outlet,netrecon.room,netrecon.building_id),id:itso-DEVICE_OUTLET,panelIndex:9,row:10,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.time,dhcp.type,syslog.message),id:itso-DHCPD,panelIndex:10,row:13,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:10,id:itso-IPR-slash-Device-Outlet-Help-Text,panelIndex:11,row:7,size_x:3,size_y:3,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),timeRestore:!f,title:%27itso-%20Wired%20IP%20Lookup%27,uiState:(),viewMode:view)";
		
	var laaURL_ITSO_WIRELESS_IPLOOKUP = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Wireless-IP-Lookup?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:%27"+dateTimeFromLAAISO+"%27,mode:absolute,to:%27"+dateTimeToLAAISO+"%27))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:9,id:itso-DHCP-help-text,panelIndex:12,row:5,size_x:4,size_y:3,type:visualization),(col:1,columns:!(%27@timestamp%27,device.ip,aaa.Radius-Client-Ip-Address,src.mac,username,aaa.action,aaa.Radius-Instance,aaa.EAP-Type,aaa.Authentication-Type,message),id:itso-AUTHN,panelIndex:15,row:8,size_x:12,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(dst.ip,src.ip,username,ras.roles,ras.msg),id:itso-VPNPoolIP-and-radiusd,panelIndex:16,row:12,size_x:12,size_y:3,sort:!(%27@timestamp%27,desc),type:search),(col:1,columns:!(netrecon.fact.type,src.ip,dst.ip,nat.border_start_port,nat.border_stop_port,nat.border_event,nat.border_domain,device.name),id:itso-BORDER-NAT,panelIndex:17,row:1,size_x:8,size_y:4,sort:!(%27@timestamp%27,desc),type:search),(col:9,id:itso-NAT-IP-help-text,panelIndex:18,row:1,size_x:4,size_y:4,type:visualization),(col:1,columns:!(netrecon.fact.type,src.ip,src.mac,dhcp.time,dhcp.type,syslog.message),id:itso-DHCPD,panelIndex:19,row:5,size_x:8,size_y:3,sort:!(%27@timestamp%27,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:%27%22"+searchIP+"%22%27)),timeRestore:!f,title:%27itso-%20Wireless%20IP%20Lookup%27,uiState:(),viewMode:view)";
	
	var laaURL_ITSO_ARGUS_24HOUR = "https://log.it.vt.edu/node/app/kibana#/dashboard/itso-Argus-24-Hour-View?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-24h,mode:quick,to:now))&_a=(filters:!(),options:(darkTheme:!t),panels:!((col:1,id:itso-Visualization-Argus-Edge-Line-Chart,panelIndex:1,row:1,size_x:12,size_y:4,type:visualization),(col:1,id:itso-Visualization-Argus-Core-Line-Chart,panelIndex:2,row:5,size_x:12,size_y:4,type:visualization),(col:1,id:itso-Visualization-Argus-Rlan-Line-Chart,panelIndex:3,row:9,size_x:12,size_y:4,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:%27*%27)),timeRestore:!t,title:%27itso-Argus%2024%20Hour%20View%27,uiState:(P-1:(spy:(mode:(fill:!f,name:!n)),vis:(legendOpen:!f)),P-2:(vis:(legendOpen:!f)),P-3:(vis:(legendOpen:!f))),viewMode:view)";
	
	var laaURL = laaURL_ITSO_IPLOOKUPv2;

	if ($("#urlDash").val() == "laaURL_ITSO_IPLOOKUPv2") { laaURL = laaURL_ITSO_IPLOOKUPv2; }
//	else if ($("#urlDash").val() == "laaURL_ITSO_IPLOOKUPv2") { laaURL = laaURL_ITSO_IPLOOKUPv2; }
	else if ($("#urlDash").val() == "laaURL_ITSO_WIRED_IPLOOKUP") { laaURL = laaURL_ITSO_WIRED_IPLOOKUP; }
	else if ($("#urlDash").val() == "laaURL_ITSO_WIRELESS_IPLOOKUP") { laaURL = laaURL_ITSO_WIRELESS_IPLOOKUP; }	
//	else if ($("#urlDash").val() == "laaURL_NISNETRECON_ALLFACTS") { laaURL = laaURL_NISNETRECON_ALLFACTS; }
//	else if ($("#urlDash").val() == "laaURL_NISNETRECON_WIRELESS") { laaURL = laaURL_NISNETRECON_WIRELESS; }
//	else if ($("#urlDash").val() == "laa_URL_NISNETRECON_IPLOCATION") { laaURL = laa_URL_NISNETRECON_IPLOCATION; }


	$("#laaURLDashboardName").html( $("#urlDash option:selected").text() );
		
	$('#laaURL').val(laaURL);
	
}

	</script>
<?php 

// EOF url_tool_javascripts.inc.php

?>